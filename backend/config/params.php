<?php
return [
	'baseUrl'=> 'http://limax.local',
	'projectName'=>'Limax',
	'rootUrl' => 'http://root.limax.local',
        'adminUrl' => 'http://admin.limax.local',
    	'adminEmail' => 'muhsin.3009@gmail.com',
    	'Tax'=>'14.5',
	'invoiceIdPrifix'=>'CWHRA',
	'currency'=>'',
	'ComapnyName'=>'RangeTech Solutions',
	'companyAddress'=>'Incubated At Startup Village , Kinfra Hi-Tech Park - Kalamassery , Cochin - 683 503 , ',
	'SalesEmail'=>'sales@rangetech.in',
	'SaleContact'=>'9539 935 888',
	'logoPath'=>'/images/logo.png',
];
