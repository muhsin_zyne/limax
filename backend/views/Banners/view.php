<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Banners */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banners-view">

  

    <p>
       <!--  <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary pull-right theme-btn']) ?> -->
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right theme-btn',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            [
                 'attribute'=>'Banner Image',
                'value'=>(yii::$app->params['rootUrl'].'/store/banners/'. $model->file),
                'format' => ['image',['width'=>'300','height'=>'150']],
            ],
           
            'created_at',
           
        ],
    ]) ?>

</div>
