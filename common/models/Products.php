<?php

namespace common\models;

use Yii;
use common\models\ProductDescription;
use common\models\ProductImages;
use common\models\ProductType;
use common\models\Categories;
use common\models\Brands;
use common\models\ProductAttributes;
use common\models\UrlKey;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $sku
 * @property string $created_at
 * @property integer $created_by
 * @property string $status
 * @property integer $case_qty
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $price;
    public $dealers_price;
    public $special_price;
    public $cost_price;
    public $from_date;
    public $to_date;
    public $meta_title;
    public $meta_description;
    public $meta_keyword;
    public $short_description;
    public $description;
    public $image;
    public $featured_image;

    public $brand_id;
    public $product_type;
    public $product_category;





    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','sku', 'status', 'case_qty','price','dealers_price','cost_price','short_description','description'], 'required'],
            [['meta_title', 'meta_description', 'meta_keyword'], 'required'],
            [['created_at','special_price','cost_price', 'created_at','image'], 'safe'],
            [['from_date','to_date','featured_image'], 'safe'],
            [['created_by', 'case_qty'], 'integer'],
            [['status'], 'string'],
            [['case_qty'],'string','max'=>2],
            [['case_qty'],'number','max'=>25,'min'=>20],
            [['sku'], 'string', 'max' => 255],
            ['sku', 'unique', 'message' => 'Art No is alreadt exist'],
            [['brand_id','product_type','product_category'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Name',
            'sku' => 'Art No',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'status' => 'Status',
            'case_qty' => 'Case Quantity',
            'from_date' => 'Valid From',
            'to_date' => 'Valid To',

        ];
    }

    public function getProductDescription(){
        return $this->hasOne(ProductDescription::className(),['product_id' => 'id']);
    }

    public function getProductAttributes(){
        return $this->hasOne(ProductAttributes::className(),['product_id'=>'id']);
    }
    public function getProductImages() {
        return $this->hasMany(ProductImages::className(),['product_id'=>'id']);
    }

    public function getUrlKey() {
        return $this->hasOne(UrlKey::className(),['product_id'=>'id']);
    }

    public function getProductType() {
        return ProductType::find()->where(['status'=>'1'])->all();
    }

    public function generateProductTypeLists() {
        $product_type = $this->getProductType();
        return ArrayHelper::map($product_type, 'id','label');

    }

    public function getProductCategory() {
        return Categories::find()->where(['status'=>'1'])->all();
    }

    public function generateProductCategoryLists() {
        $product_category = $this->getProductCategory();
        return ArrayHelper::map($product_category, 'id','title');

    }

     public function getProductBrand() {
        return Brands::find()->where(['status'=>'1'])->all();
    }

    public function generateProductBrandsLists() {
        $product_category = $this->getProductBrand();
        return ArrayHelper::map($product_category, 'id','name');

    }
}
