<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property integer $id
 * @property string $test_name
 * @property string $test_data
 * @property string $test_uni
 * @property string $status
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_uni', 'status'], 'required'],
            [['status'], 'string'],
            [['test_uni'],'unique', 'targetClass'=>'common\models\Test', 'targetAttribute'=>'test_uni', 'message'=>'you are finneshsd', 'when'=>function($model, $attribute) {
                if(!$model->isNewRecord){
                    return $model->$attribute != $model->oldAttributes[$attribute];
                }
                else {
                    return true;
                }
            }],
            [['test_name', 'test_data', 'test_uni'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_name' => 'Test Name',
            'test_data' => 'Test Data',
            'test_uni' => 'Test Uni',
            'status' => 'Status',
        ];
    }
}
