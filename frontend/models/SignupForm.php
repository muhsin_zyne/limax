<?php
namespace frontend\models;

use yii;
use yii\base\Model;
use common\models\User;
use common\models\B2bAddress;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $c_password;
    public $shop_name;
    public $address;
    public $mobile;
    public $phone;
    public $place;
    public $district;
    public $state;
    public $display_name;
    public $reCaptcha;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            [['email','mobile','password','c_password','shop_name','address','place','district','display_name'],'required'],
            [['mobile'],'string','min'=>10,'max'=>10,'message'=>'Not a valid Mobile Number.'],
            [['mobile'],'integer'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'string', 'min' => 6],
            ['c_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match"],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LeEqjQUAAAAACCFJJKImq1SoA9Tebm2pVvJaVRb', 'uncheckedMessage' => 'Please confirm that you are not a bot.'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->user_type='user';
        $user->level='5';
        $user->is_approved = 'pending';
        $user->display_name = $this->display_name;
        if($user->save()) {
            $b2bAddressModel = new B2bAddress();
            $b2bAddressModel->user_id = $user->id;
            $b2bAddressModel->mobile = $this->mobile;
            $b2bAddressModel->phone = $this->phone;
            $b2bAddressModel->shop_name = $this->shop_name;
            $b2bAddressModel->address = $this->address;
            $b2bAddressModel->place = $this->place;
            $b2bAddressModel->district = $this->district;
            if($b2bAddressModel->save()) {
                $this->signupEmailSend($user->id);
                return $user;
            }
            else {
                return null;
            }
        }else {
                return null;
        }
    
    }

    public function attributeLabels()
    {
        return [
            'c_password' => 'Confirm Password',
            'display_name' => 'Contact Person',
        ];
    }

    public function signupEmailSend($userId) {
        $userData = User::findOne($userId);
        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'signupToAdmin-html', 'text' => 'signupToAdmin-html'],
                ['model' => $userData]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'Limax Group'])
            ->setTo([Yii::$app->params['adminEmail']])
            ->setSubject('New Join Request - '. yii::$app->params['websiteName'])
            ->send();

        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'signupToUser-html', 'text' => 'signupToUser-html'],
                ['model' => $userData]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'Limax Group'])
            ->setTo([$userData->email])
            ->setSubject('Joining Request - '. yii::$app->params['websiteName'])
            ->send();
    }


}
