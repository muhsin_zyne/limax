<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\components\widgets\ListTable;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model->productDescription->detail_attributes = json_decode($model->productDescription->detail_attributes,true);
$productImages = [];

foreach ($model->productImages as $images) {  
    if ($images == $model->productImages[0]) {  
    
    } 
    else {
        $productImages[] =  $images;
    }

}

?>
<div class="container">
    <div class="products-view">
      <div id="content-block">
        <div class="content-push">
          <div class="information-blocks">
            <div class="row">
              <div class="col-sm-6 information-entry">
                    <div class="product-preview-box">
                        <div class="swiper-container product-preview-swiper" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1" style="">
                            <div class="swiper-wrapper">

                                  <?php
                                    foreach ($productImages as $productImage) {
                                        echo ' <div class="swiper-slide"> <div class="product-zoom-image"> <img src="'.yii::$app->params['rootUrl'].'/store/products/images/base/'.$productImage->image. '" alt="" data-zoom="img/product-limax1-zoom.jpg" /></div>
                                        </div> ';
                                    }

                                  ?>                                         
                                
                            </div>
                            <div class="pagination"></div>
                            <div class="product-zoom-container">
                                <div class="move-box">
                                    <img class="default-image" src="" alt="" />
                                    <img class="zoomed-image" src="" alt="" />
                                </div>
                                <div class="zoom-area"></div>
                            </div>
                        </div>
                        <div class="swiper-hidden-edges">
                            <div class="swiper-container product-thumbnails-swiper" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="3" data-int-slides="3" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4">
                                <div class="swiper-wrapper">
                                   
                                    <?php
                                       foreach ($productImages as $productImage) {
                                        echo ' <div class="swiper-slide selected"> <div class="paddings-container"> <img src="'.yii::$app->params['rootUrl'].'/store/products/images/thumbnail/'.$productImage->image. '" alt="" data-zoom="img/product-limax1-zoom.jpg" /></div>
                                        </div> ';
                                       }

                                    ?> 

                                </div>
                                <div class="pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>


                  <div class="col-sm-6 information-entry">
                            <div class="product-detail-box">
                                <h1 class="product-title"> <?= $model->title ?> </h1>
                                <h3 class="product-subtitle">ART NO : <?= $model->sku ?></h3>
                                <h3 class="product-subtitle"> <?= $model->productDescription->short_description ?> </h3>
                               <!--  <div class="rating-box">
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star-o"></i></div>
                                    <!-- <div class="rating-number">25 Reviews</div> -->
                               
                               <!--  <div class="product-description detail-info-entry">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div> -->
                               <!--  <div class="price detail-info-entry">
                                    <div class="prev"><i class="fa fa-inr" aria-hidden="true"></i> 600</div>
                                    <div class="current"><i class="fa fa-inr" aria-hidden="true"></i> 450</div>
                                </div> -->
                                <div class="size-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Size Available</div>
                                    <div class="entry active">6</div>
                                    <div class="entry active">7</div>
                                    <div class="entry active">8</div>
                                    <div class="entry active">9</div>
                                    <div class="entry active">10</div>
                                    
                                </div>

                                <?php 



?>                              

                                <?= ListTable::widget(['model'=>$model->productDescription->detail_attributes]) ?>


                               <!--  <div class="color-selector detail-info-entry"> -->
                                    <!-- <div class="detail-info-entry-title">Color</div> -->
                                   <!--  <div class="entry active" style="background-color: #d23118;">&nbsp;</div> -->
                                  <!--   <div class="entry" style="background-color: #2a84c9;">&nbsp;</div> -->
                                  <!--   <div class="entry" style="background-color: #000;">&nbsp;</div>
                                    <div class="entry" style="background-color: #d1d1d1;">&nbsp;</div> -->
                                   <!--  <div class="spacer"></div> -->
                              <!--   </div> -->






                                <!-- <div class="quantity-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Quantity</div>
                                    <div class="entry number-minus">&nbsp;</div>
                                    <div class="entry number">10</div>
                                    <div class="entry number-plus">&nbsp;</div>
                                </div>
                                <div class="detail-info-entry">
                                    <a class="button style-10">Add to cart</a>
                                    <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                    <div class="clear"></div>
                                </div> -->
                          <!--       <div class="tags-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Tags:</div>
                                    <a href="#">bootstrap</a>/
                                    <a href="#">collections</a>/
                                    <a href="#">color/</a>
                                    <a href="#">responsive</a>
                                </div> -->
                              <!--   <div class="share-box detail-info-entry">
                                    <div class="title">Share in social media</div>
                                    <div class="socials-box">
                                        <a href="#"><i class="fa fa-facebook"></i></a>                                       
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-youtube"></i></a>                                    
                                    <div class="clear"></div>
                                </div>
                            </div> -->
                          </div>

                         

                        </div>

                   
            </div>
          </div>
        </div>
      </div>
    </div>
</div>