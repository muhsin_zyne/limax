<?php

namespace backend\controllers;

use Yii;
use yii\web\UploadedFile;
use common\models\ProductImages;
use common\models\Products;
use common\models\User;
use backend\components\AdminController;
use yii\helpers\Json;
use yii\helpers\FileHelper;


class ProductImagesController extends AdminController
{	
	public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
	}

    public $uploadThumbnailPath;
    public $uploadSmallPath;
    public $uploadBasePath;
    public $directory;
    public $thumbWidth;
    public $thumbHeight;
    public $smallWidth;
    public $smallHeight;
    public $baseWidth;
    public $baseHeight;


	public function init(){
        $this->uploadThumbnailPath = \Yii::$app->basePath."/../store/products/images/thumbnail/";
        $this->uploadSmallPath = \Yii::$app->basePath."/../store/products/images/small/";
        $this->uploadBasePath = \Yii::$app->basePath."/../store/products/images/base/";
        $this->directory = \Yii::$app->basePath."/..//store/products/images/upload/";
        $this->thumbWidth = 200;
		$this->thumbHeight = 200;
		$this->smallWidth = 350;
		$this->smallHeight = 350;
		$this->baseWidth = 600;
		$this->baseHeight = 600;

        parent::init();
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUpload()
    {
        //die('hai');
     	$random = $_GET['rand'];	

     	
      	if(isset($_FILES)){  

      					
      	  //die('hai');
	      	//$file = $_FILES['Products']['tmp_name']['images'];
	      	$file = $_FILES['Products']['name']['image'];
	      	$ext = pathinfo($file, PATHINFO_EXTENSION);
	      	if($ext == "jpg")
	      		$source_image = imagecreatefromjpeg($_FILES['Products']['tmp_name']['image']);
	      	if($ext == "png")
	      		$source_image = imagecreatefrompng($_FILES['Products']['tmp_name']['image']);
			$width = imagesx($source_image);
			$height = imagesy($source_image);

			$fileName = $random.'_'. basename($_FILES['Products']['name']['image']);
			
			$uploadThumbFile = $this->uploadThumbnailPath . $fileName;
			$uploadSmallFile = $this->uploadSmallPath . $fileName;
			$uploadBaseFile = $this->uploadBasePath . $fileName;
			
			//var_dump($uploadThumbFile);die();

			/*Thubnail image resizing*/

			$uploadfile = $this->uploadThumbnailPath . basename($_FILES['Products']['name']['image']);
			$thumb_width = 200;
			$thumb_height = 200;
			$virtual_image = imagecreatetruecolor($thumb_width, $thumb_height);
			
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
			$im = imagejpeg($virtual_image, $uploadThumbFile);

			//var_dump($im);die();

			/* Small Image*/

			$small_width = 350;
			$small_height = 350;
			$virtual_image = imagecreatetruecolor($small_width, $small_height);
			
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $small_width, $small_height, $width, $height);
			$im = imagejpeg($virtual_image, $uploadSmallFile); 

			/* Base Image*/

			$base_width = 600;
			$base_height = 600;
			$virtual_image = imagecreatetruecolor($base_width, $base_height);
			
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $base_width, $base_height, $width, $height);
			$im = imagejpeg($virtual_image, $uploadBaseFile); 
      	
      }
        
    }


    public function actionDelete($id) {
    	$productImageModel = ProductImages::findOne($id);
    	if(!empty($productImageModel)) {

    		if (is_file($this->directory . $productImageModel->image)) {
		       	$productImageModelCopy = $productImageModel;
		       	$productImageModel->delete();
		       	$path = $this->uploadThumbnailPath . $productImageModelCopy->image;
		       	$output['files'][] = [
		            'name' => $productImageModelCopy->image,
		            'size' => '',
		            'url' => $path,
		            'thumbnailUrl' => $path,
		            'deleteUrl' => 'product-images/delete?id=' . $productImageModelCopy->id,
		            'deleteType' => 'POST',
		        ];

		        unlink($this->directory . $productImageModelCopy->image);
		        unlink($this->uploadThumbnailPath . $productImageModelCopy->image);
		        unlink($this->uploadBasePath . $productImageModelCopy->image);
		        unlink($this->uploadSmallPath . $productImageModelCopy->image);

		        return json_encode($output,true);
		    }

		    
    	}
    }



    public function actionUploader($product_id) {

	    $model = new Products();
	    $imageFile = UploadedFile::getInstance($model,'image');
		
		/*$directory = Yii::getAlias('@frontend/web/img/temp') . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
	    if (!is_dir($directory)) {
	        FileHelper::createDirectory($directory);
	    }*/

	    if ($imageFile) {
	    	
	        $uid = uniqid(time(), true);
	        $uid = str_replace('.', '_', $uid);
	        $fileName = $uid . '.' . $imageFile->extension;
	  		$filePath = $this->directory . $fileName;
	        if($imageFile->saveAs($filePath)) {
	            $path = yii::$app->params['rootUrl'].'/store/products/images/upload/'.$fileName;
	            if($imageFile->extension=='jpg') {
	            	$sourceImage = imagecreatefromjpeg($path);
	            }
	            if($imageFile->extension=='png') {
	            	$sourceImage = imagecreatefrompng($path);
	            }
				$width = imagesx($sourceImage);
				$height = imagesy($sourceImage);
				$uploadThumbFile = $this->uploadThumbnailPath . $fileName;
				$uploadSmallFile = $this->uploadSmallPath . $fileName;
				$uploadBaseFile = $this->uploadBasePath . $fileName;

				/*Thubnail image resizing*/
				$uploadfile = $this->uploadThumbnailPath . basename($fileName);
				$virtualImage = imagecreatetruecolor($this->thumbWidth, $this->thumbHeight);
				
				imagecopyresampled($virtualImage, $sourceImage, 0, 0, 0, 0, $this->thumbWidth, $this->thumbHeight, $width, $height);
				$im = imagejpeg($virtualImage, $uploadThumbFile);
				/* Small Image*/
				$virtualImage = imagecreatetruecolor($this->smallWidth, $this->smallHeight);
				imagecopyresampled($virtualImage, $sourceImage, 0, 0, 0, 0, $this->smallWidth, $this->smallHeight, $width, $height);
				$im = imagejpeg($virtualImage, $uploadSmallFile); 
				/* Base Image*/
				$virtualImage = imagecreatetruecolor($this->baseWidth, $this->baseHeight);
				imagecopyresampled($virtualImage, $sourceImage, 0, 0, 0, 0, $this->baseWidth, $this->baseHeight, $width, $height);
				$im = imagejpeg($virtualImage, $uploadBaseFile); 

				$productImageModel = new ProductImages();
				$productImageModel->product_id = $product_id;
				$productImageModel->image = $fileName;
				$productImageModel->is_thumb ='0';
				$productImageModel->is_small ='0';
				$productImageModel->is_base = '0';
				$productImageModel->is_featured = '1';
				if($productImageModel->save()) {
					$responce = [
		                'files' => [
		                    [
		                        'name' => $fileName,
		                        'size' => $imageFile->size,
		                        'url' => $path,
		                        'thumbnailUrl' => yii::$app->params['rootUrl'].'/store/products/images/thumbnail/'.$fileName,
		                        'deleteUrl' => 'product-images/delete?id=' . $productImageModel->id,
		                        'deleteType' => 'POST',
		                    ],
		                ],
		            ];

		            return json_encode($responce,true);
					}
	            
	        }
	    }

    return null;

} 

}
