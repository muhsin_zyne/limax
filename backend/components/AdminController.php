<?php
namespace backend\components;

use yii;
use backend\models\User;

class AdminController extends \yii\web\Controller

{	

	public $title;
    public $activeUser;
    
    public $globalAdminPermission = [
        'Index' => ['site/index', 'site/error', 'site/logout','site/login'],
        'Menus'=>['menus/index','menus/create','menus/delete','menus/update','menus/update-menu'],
        'Categories'=>['categories/index','categories/create','categories/view','categories/create-child','categories/delete','categories/update','categories/view-icon'],
        'Catalogue' =>[],
        'B2B Users'=> ['b2b-users/index','b2b-users/view','b2b-users/update','b2b-users/delete'],
        'Test'=>['test/index','test/create','test/view','test/update','test/delete'],
        'Attributes'=> ['attributes/index','attributes/create','attributes/update','attributes/delete','attributes/view','attributes/delete-attribute-option'],
        'Products'=>['products/image-uplaod','products/index','products/create','products/update','products/delete','products/view'],
        'Products Images'=>['product-images/upload','product-images/uploader','product-images/delete'],
        'Banners'=>['banners/index','banners/create','banners/update','banners/delete','banners/view'],
        'Product Type'=>['product-type/index','product-type/create','product-type/update','product-type/delete','product-type/view'],
        'Brands'=>['brands/index','brands/create','brands/update','brands/delete','brands/view'],
        'Cms Block'=>['cms-block/index','cms-block/create','cms-block/update','cms-block/delete','cms-block/view'],
        'Cms Pages'=> ['cms-pages/index','cms-pages/create','cms-pages/update','cms-pages/delete','cms-pages/view'],

    ];
    
    public $masterAdminPermission = [
       
     ];
    
    public $adminPermission = [

	];

    public $subAdminPermission = [
        
    ];

    public $userAdminPermission = [
        
         
    ];


    public $endUserLevels = [
        'admins' => [1,2],
        'endUserAdmin' =>[5],

    ];

    
    public function beforeAction($event) {
        
        if(!yii::$app->user->isGuest) {
            if(!$this->hasPermission(Yii::$app->controller->id."/".Yii::$app->controller->action->id)){
                die('403: Not allowed');
            }
        }
        else if (yii::$app->user->isGuest) {   
            if(Yii::$app->controller->id."/".Yii::$app->controller->action->id=='site/login') {
                $this->layout='loginLayout';
            }
            else {
                $this->layout='loginLayout';
                return $this->redirect(['/site/login']);
            }
        }
        //$this->mainMenu= require('../config/Menu.php');
        return parent::beforeAction($event);
    }


    public function isVisible($key,$type=null) {
        if(isset(Yii::$app->user->id)) {
            $activeUser = $this->getActiveUser();
                switch ($activeUser->level) {
                    case '1': // global admin permission gose here
                        if(array_key_exists($key,$this->globalAdminPermission)) {
                            return true;
                        }
                        else {
                                return false;
                        }
                        
                        break;
                    case '2': // master admin permission gose here
                        if(array_key_exists($key,$this->masterAdminPermission)) {
                            return true;
                        }
                        else {
                                return false;
                        }
                        
                        break;
                                       default:
                        return false;
                        break;
                }

        }
        else {
            return false;
        }
    }


    public function hasPermission($event) {
        


        if(isset(Yii::$app->user->id)) {
            $activeUser = $this->getActiveUser();
            
            if($activeUser->level=='1') {
                foreach ($this->globalAdminPermission as  $permission) {
                    if(in_array($event,$permission)) {
                        return true;
                    }
                }
                return false;
            }
            else if($activeUser->level=='2') {
                foreach ($this->masterAdminPermission as  $permission) {
                    if(in_array($event,$permission)) {
                        return true;
                    }
                }
                return false;
            }
                
        }
    } //.has permission ending

    public function getNowTime() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('Y-m-d H:i:s');
        return $dateTime;
    }

    public function getNowTimeStamp() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('Y-m-d H:i:s');
        return strtotime($dateTime);
    }

    protected function getActiveUser() {
        
        if(isset(yii::$app->user->id)) {

            $activeUser = User::find()->where(['id' =>yii::$app->user->id])->one();
            if($activeUser->image==""){
                $activeUser->image = 'avatar-01.png';
            }
            return $activeUser;
        }
        
    }

    protected function getNowDigit() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('YmdHis');
        return $dateTime;
    }

    protected function valid_formats($type) {
        switch ($type) {
            case 'image':
                $valid_formats = array("jpg", "png", "gif", "jpeg","JPG");
                break;
            
            default:
                $valid_formats = null;
                break;
        }

        return $valid_formats;
    }

} // main class ending
