<?php

namespace common\models;
use yii\helpers\Html;

use Yii;

/**
 * This is the model class for table "menus".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property integer $parent
 * @property integer $position
 * @property integer $unAssigned
 * @property string $type
 */
class Menus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','path','target'], 'required'],
            [['path', 'type'], 'string'],
            [['target','menu_type','type','icon'],'safe'],
            [['parent', 'position', 'un_assigned'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'path' => 'Path',
            'parent' => 'Parent',
            'position' => 'Position',
            'unAssigned' => 'Un Assigned',
            'icon' => 'Special Icon',
            'type' => 'Type',
        ];
    }


    public function getMenuPositionChange($ass_menu_array,$status,$parent=NULL) {
        $parent=(isset($parent))? $parent : 0;
        $position = 0;
        foreach ($ass_menu_array as $key => $ass_menu) {
            $menu = Menus::findOne($ass_menu['id']);
            $menu->parent = $parent;
            $menu->position = $position;
            $menu->un_assigned= $status;
            $menu->save(false);
            $position++;
            if(isset($ass_menu['children'])) {
                $this->getMenuPositionChange($ass_menu['children'],$status,$menu['id']);
            }
        }
        return true;

    }


    public function getMenuRecrusive($data,$child=FALSE){
        $html='<ol class="dd-list">';  
            foreach($data as $item){ //var_dump($item);
                if(isset($item['items'])) { //var_dump($item['categoriesId']);
                    $html.= '<li class="dd-item dd3-item" data-id="'.$item['id'].'">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content">'.$item['label'].'';
                        $html.='
                                <u class="menu-icon-set">'.Html::a('<i class="fa fa-pencil"></i> Edit', ['/menus/update','id'=>$item['id'],'type'=>$item['type']],['class'=>'bootstrap-modal-new hover-black','data-title' => 'Update Menu', 'data-width'=>'30%', ]).'</u>

                                <u class="menu-icon-set">'.Html::a('<i class="fa fa-trash"></i> Delete', ['/menus/delete', 'id' => $item['id'],'type'=>$item['type']], ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],'class'=>'danger']).'</u>

                                ';
                    $html.='</div>';
                    $html.= $this->getMenuRecrusive($item['items'],TRUE); 
                    $html.= '</li>';  
                }  
            }
        $html.='</ol>';                           
    return $html;
    }


    public function getItems($items_new,$parent){
        $newelements = array();
        foreach ($items_new as $newitem) {
            if($newitem['id'] != $parent && $newitem['parent'] == $parent) {
                $newelements[] =
                    [   'label' => $newitem['title'],
                        'url' => $newitem['path'],
                        'id' => $newitem['id'],
                        'type'=>$newitem['type'],
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ];
            }
        }
        return $newelements; 
    } 


    public function getMenuList($items) {

        $elements = [];
        foreach ($items as $item) {
            if($item['parent']==0) {
                $elements [] = [
                    'label' => $item['title'],
                    'url' => $item['path'],
                    'id' =>$item['id'],
                    'type' =>$item['type'],
                    'items' =>$this->getItems($items,$item['id']),

                ];
            } 
        }

        $result = $this->getMenuRecrusive($elements,FALSE);
        return $result;

        
    }


    

    
}
