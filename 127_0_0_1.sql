-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2017 at 08:33 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopping_yii2`
--
CREATE DATABASE IF NOT EXISTS `shopping_yii2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `shopping_yii2`;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options`
--

CREATE TABLE `attribute_options` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `value` mediumtext NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `required` tinyint(2) DEFAULT NULL,
  `apply_to_simple` tinyint(2) DEFAULT '0',
  `apply_to_grouped` tinyint(2) DEFAULT '0',
  `apply_to_configurable` tinyint(2) DEFAULT '0',
  `apply_to_virtual` tinyint(2) NOT NULL DEFAULT '0',
  `apply_to_bundle` tinyint(2) NOT NULL DEFAULT '0',
  `apply_to_downloadable` tinyint(2) NOT NULL DEFAULT '0',
  `system` tinyint(2) NOT NULL DEFAULT '0',
  `code` varchar(255) DEFAULT NULL,
  `field` enum('text','textarea','date','boolean','multiselect','dropdown','price') NOT NULL DEFAULT 'text',
  `validation` varchar(255) DEFAULT NULL,
  `configurable` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `title`, `required`, `apply_to_simple`, `apply_to_grouped`, `apply_to_configurable`, `apply_to_virtual`, `apply_to_bundle`, `apply_to_downloadable`, `system`, `code`, `field`, `validation`, `configurable`) VALUES
(1, 'name', 1, 1, 1, 1, 1, 1, 1, 0, 'name', 'text', 'letters', 0),
(2, 'price', 1, 1, 1, 1, 1, 1, 1, 0, 'price', 'price', 'decimal', 0);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `path` text,
  `parent` int(10) NOT NULL DEFAULT '0',
  `position` int(10) NOT NULL DEFAULT '0',
  `un_assigned` enum('0','1') DEFAULT '0',
  `type` enum('main','header','footer','category') NOT NULL,
  `menu_type` enum('cms','other') NOT NULL DEFAULT 'other',
  `target` enum('_blank','_self') NOT NULL,
  `icon` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `path`, `parent`, `position`, `un_assigned`, `type`, `menu_type`, `target`, `icon`) VALUES
(9, 'Header 2 Menu', 'ddsdsd', 0, 0, '1', 'header', 'other', '_self', NULL),
(15, 'Footer Mian', 'sss', 0, 0, '1', 'footer', 'other', '_self', NULL),
(16, 'Test Footer', 'testfooter', 0, 1, '1', 'footer', 'other', '_self', NULL),
(17, 'New', 'site/contact-su', 3, 1, '1', 'main', 'other', '_self', NULL),
(18, 'New 2', 'site/data', 3, 0, '1', 'main', 'other', '_self', NULL),
(19, 'Home', 'site/index', 0, 1, '1', 'main', 'other', '_self', 'fa fa-home'),
(20, 'testadmin3', 'testadmin3', 0, 1, '1', 'header', 'other', '_self', NULL),
(21, 'Test', 'test sss', 0, 0, '0', 'main', 'other', '_self', NULL),
(22, 'About Us', 'siteddd/about', 0, 0, '1', 'main', 'other', '_self', 'fa fa-home'),
(23, 'Contact Us', 'site/contact', 0, 2, '1', 'main', 'other', '_self', NULL),
(24, 'Advise', '#', 0, 3, '1', 'main', 'other', '_self', NULL),
(25, 'Terms & Conditions', 'terms-conditions.html', 24, 0, '1', 'main', 'other', '_self', NULL),
(26, 'Privacy', 'privacy.html', 24, 1, '1', 'main', 'other', '_self', NULL),
(27, 'Payment', 'payment.html', 24, 2, '1', 'main', 'other', '_self', NULL),
(28, 'Mobiles', 'mobiles.html', 0, 0, '1', 'category', 'other', '_blank', 'mobile'),
(29, 'data', 'data', 0, 2, '1', 'category', 'other', '_blank', 'new'),
(30, 'Laptops', '#', 0, 1, '1', 'category', 'other', '_blank', 'laptop'),
(31, 'Samsung', 'samsung.html', 0, 3, '1', 'category', 'other', '_self', ''),
(32, 'Lenovo', 'lenovo.html', 0, 4, '1', 'category', 'other', '_self', ''),
(33, 'SONY', 'sony.html', 28, 0, '1', 'category', 'other', '_self', ''),
(34, 'MI', 'mi.html', 28, 1, '1', 'category', 'other', '_self', ''),
(35, 'Motrola', 'motrola.html', 28, 2, '1', 'category', 'other', '_self', '');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495384146),
('m130524_201442_init', 1495384148);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user') COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `level` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `display_name`, `user_type`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `image`, `status`, `created_at`, `updated_at`, `level`, `created_by`) VALUES
(1, 'admin', 'Muhammed Muhsin', 'admin', '4gy_jvkWiDIQmT79sk_D-rEy4LBQu1sR', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', NULL, 'muhsin.zyne@gmail.com', '', 10, 1487064707, 1487064707, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attribute_options`
--
ALTER TABLE `attribute_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
