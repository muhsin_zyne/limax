<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductType */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-type-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          
            'label',
            'status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
