<?php 

namespace backend\components\widgets;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\CmsBlock;
use yii;

Class CmsBlocks extends Widget
{	
	public $code;
	public function init() {
		if($this->code===null) {
			$this->code=null;
		}
	}

	public function run() {
		
		$result = $this->generateCmsBlock($this->code);
		echo $result;
	
		//print_r($result);
	}

	public function generateCmsBlock($id) {
		$blockData = $this->findModel($id);
		return $blockData;
	}

	public function findModel($id) {
		$result = CmsBlock::find()->where(['id'=>$id])->one();
		if($result!=null) {
			return $result->content;
		}
		else {
			return null;
		}
	}

}

?>



 
                   
                    
                    
                



















