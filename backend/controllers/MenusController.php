<?php

namespace backend\controllers;

use Yii;
use common\models\Menus;
use common\models\search\MenusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;

/**
 * MenusController implements the CRUD actions for Menus model.
 */
class MenusController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menus models.
     * @return mixed
     */
    public function actionIndex($type)
    {   
        if($type==null) {
            return $this->redirect(['index']);
        }

        $assigned_menu = Menus::find()->where(['un_assigned'=>'1','type'=>$type]) ->orderBy('position')->all();
        $un_assigned_menu = Menus::find()->where(['un_assigned'=>'0','type'=>$type]) ->orderBy('position')->all();


        return $this->render('index', [
            'un_assigned_menu' => $un_assigned_menu,
            'assigned_menu' => $assigned_menu,
            'type' =>$type,
        ]);
    }


    public function actionUpdateMenu() {

            

        if (Yii::$app->request->post()){
            
            $model = new Menus();
          
            $assigned_menu=$_POST['signedMenu'];
            $un_assigned_menu=$_POST['unsignedMenu'];
            $ass_status=1;
            $unass_status=0;
            $ass_menu_array = json_decode($assigned_menu, true);  
            $un_ass_menu_array = json_decode($un_assigned_menu, true);  
            $model->getMenuPositionChange($ass_menu_array,$ass_status);   // Assigned Menu List
            $model->getMenuPositionChange($un_ass_menu_array,$unass_status);   // UnAssigned Menu List
            Yii::$app->getSession()->setFlash('success', 'The Menu has been Updated.');
            //return $this->redirect(['index']);
            

        }

                 
    

   

       
    }

    /**
     * Displays a single Menus model.
     * @param integer $id
     * @return mixed
     */





    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type)
    {
        $model = new Menus();

        if ($model->load(Yii::$app->request->post())) {

            $model->parent = 0;
            $model->position =0;
            $model->un_assigned=0;
            $model->type = $type;

            if($model->save(false)) {
                Yii::$app->getSession()->setFlash('success', 'Menu has been created');
                return $this->redirect(['index', 'type'=>$type]);
            }
            
        } else {

            if(Yii::$app->request->isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
            
        }
    }

    /**
     * Updates an existing Menus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($type,$id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Menu has been updated');
                return $this->redirect(['index', 'type'=>$type]);
            }

        } else {
            if(Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Menus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($type,$id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('error', 'Menu has been Deleted');
        return $this->redirect(['index','type'=>$type]);
    }

    /**
     * Finds the Menus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
