<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property integer $id
 * @property string $file
 * @property string $created_at
 * @property string $updated_at
 * @property string $url_to
 * @property string $valid_from
 * @property string $valid_to
 */
class Banners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file', 'created_at', 'updated_at', 'url_to', 'valid_from', 'valid_to'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
         
            'file' => 'Upload Image',
           
        ];
    }

    public function findAllBanners($length) {
        return Banners::find()->limit($length)->all();
    }
}
