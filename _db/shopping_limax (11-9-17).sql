-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2017 at 04:42 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopping_limax`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `required` tinyint(2) DEFAULT NULL,
  `apply_to_simple` tinyint(2) DEFAULT '0',
  `apply_to_grouped` tinyint(2) DEFAULT '0',
  `apply_to_configurable` tinyint(2) DEFAULT '0',
  `apply_to_virtual` tinyint(2) NOT NULL DEFAULT '0',
  `apply_to_bundle` tinyint(2) NOT NULL DEFAULT '0',
  `apply_to_downloadable` tinyint(2) NOT NULL DEFAULT '0',
  `system` tinyint(2) NOT NULL DEFAULT '0',
  `code` varchar(255) DEFAULT NULL,
  `field` enum('text','textarea','date','boolean','multiselect','dropdown','price') NOT NULL DEFAULT 'text',
  `validation` varchar(255) DEFAULT NULL,
  `configurable` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `title`, `required`, `apply_to_simple`, `apply_to_grouped`, `apply_to_configurable`, `apply_to_virtual`, `apply_to_bundle`, `apply_to_downloadable`, `system`, `code`, `field`, `validation`, `configurable`) VALUES
(1, 'name', 1, 1, 1, 1, 1, 1, 1, 0, 'name', 'text', 'letters', 0),
(2, 'price', 1, 1, 1, 1, 1, 1, 1, 0, 'price', 'price', 'decimal', 0),
(4, '', 0, 1, 1, 1, 1, 1, 1, 0, '', 'text', '', 0),
(5, '', 0, 1, 1, 1, 1, 1, 1, 0, '', 'text', '', 0),
(6, 'ss', 0, 1, 1, 1, 1, 1, 1, 0, '', 'text', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options`
--

CREATE TABLE `attribute_options` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `value` mediumtext NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `icon` varchar(255) DEFAULT NULL,
  `image` mediumtext,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `parent` int(11) DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `readonly` enum('0','1') NOT NULL DEFAULT '1',
  `url_key` mediumtext,
  `meta_description` text,
  `meta_keywords` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `description`, `icon`, `image`, `status`, `parent`, `position`, `readonly`, `url_key`, `meta_description`, `meta_keywords`) VALUES
(2, 'manu', 'dsd', 'birthday-cake', NULL, '1', 0, 0, '1', 'test', 'dsd', 'dsdsd'),
(5, 'test', 'djf', 'external-link', NULL, '1', 0, 0, '1', 'external-link', 'sdsd', 'dsdsd');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `path` text,
  `parent` int(10) NOT NULL DEFAULT '0',
  `position` int(10) NOT NULL DEFAULT '0',
  `un_assigned` enum('0','1') DEFAULT '0',
  `type` enum('main','header','footer','category') NOT NULL,
  `menu_type` enum('cms','other') NOT NULL DEFAULT 'other',
  `target` enum('_blank','_self') NOT NULL,
  `icon` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `path`, `parent`, `position`, `un_assigned`, `type`, `menu_type`, `target`, `icon`) VALUES
(9, 'Header 2 Menu', 'ddsdsd', 0, 0, '1', 'header', 'other', '_self', NULL),
(15, 'Footer Mian', 'sss', 0, 0, '1', 'footer', 'other', '_self', NULL),
(16, 'Test Footer', 'testfooter', 0, 1, '1', 'footer', 'other', '_self', NULL),
(17, 'New', 'site/contact-su', 3, 1, '1', 'main', 'other', '_self', NULL),
(18, 'New 2', 'site/data', 3, 0, '1', 'main', 'other', '_self', NULL),
(19, 'ABOUT LIMAX', 'site/about', 0, 1, '1', 'main', 'other', '_self', 'fa fa-home'),
(20, 'testadmin3', 'testadmin3', 0, 1, '1', 'header', 'other', '_self', NULL),
(21, 'STORE LOCATOR', '/site/stores', 0, 5, '1', 'main', 'other', '_self', ''),
(22, 'NEW ARRAIVALS', '/site/product', 0, 3, '1', 'main', 'other', '_self', 'fa fa-home'),
(23, 'PRODUCTS', '/site/product', 0, 2, '1', 'main', 'other', '_self', ''),
(24, 'CONTACT US', 'site/contact', 0, 4, '1', 'main', 'other', '_self', ''),
(25, 'DEALER''S LOGIN', '/site/dealers_login', 0, 6, '1', 'main', 'other', '_self', ''),
(26, 'WOMEN', 'privacy.html', 23, 1, '1', 'main', 'other', '_self', ''),
(27, 'MEN', 'site/product', 23, 0, '1', 'main', 'other', '_self', ''),
(28, 'Mobiles', 'mobiles.html', 0, 4, '1', 'category', 'other', '_self', 'mobile'),
(29, 'data', 'data', 0, 5, '1', 'category', 'other', '_blank', 'new'),
(30, 'Laptops', '#', 0, 2, '1', 'category', 'other', '_blank', 'fa fa-lemon-o'),
(31, 'Samsung', 'samsung.html', 0, 1, '1', 'category', 'other', '_self', ''),
(32, 'Lenovo', 'lenovo.html', 0, 3, '1', 'category', 'other', '_self', ''),
(33, 'SONY', 'sony.html', 0, 0, '1', 'category', 'other', '_self', ''),
(34, 'MI', 'mi.html', 0, 6, '1', 'category', 'other', '_self', ''),
(35, 'Motrola', 'motrola.html', 0, 7, '1', 'category', 'other', '_self', ''),
(36, 'dsd', 'dsd', 0, 0, '0', 'main', 'other', '_blank', 'dsd'),
(37, 'HOME', '/site/index', 0, 0, '1', 'main', 'other', '_self', '');

-- --------------------------------------------------------

--
-- Table structure for table `meta`
--

CREATE TABLE `meta` (
  `id` int(11) NOT NULL,
  `type` enum('products','cms') NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `cms_id` int(11) DEFAULT NULL,
  `meta_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meta`
--

INSERT INTO `meta` (`id`, `type`, `product_id`, `cms_id`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', 18, NULL, 'eeeeeeeeeeeee', 'eeeeeeeeeeeeeeee', 'eeeeeeeeeeeeeeeeee'),
(2, 'products', 19, NULL, 'htrgjtthhyf', 'fv6ythf54ertdt53', 'cgtgrechytt'),
(3, 'products', 20, NULL, 'htrgjtthhyf', 'fv6ythf54ertdt53', 'cgtgrechytt'),
(4, 'products', 21, NULL, 'htrgjtthhyf', 'fv6ythf54ertdt53', 'cgtgrechytt'),
(5, 'products', 22, NULL, 'htrgjtthhyf', 'fv6ythf54ertdt53', 'cgtgrechytt'),
(6, 'products', 23, NULL, 'rrrrrrrrrrr', 'rrrrrrrrrrrr', 'rrrrrrrrrrrrrrr');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495384146),
('m130524_201442_init', 1495384148);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `case_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `sku`, `created_at`, `created_by`, `status`, `case_qty`) VALUES
(1, 'dff', 'fdf', '2017-09-06 04:54:41', 1, '0', 33),
(2, 'dff', 'fdfdsds', '2017-09-06 04:55:29', 1, '0', 33),
(3, 'ans', '4545455', '2017-09-06 05:07:15', 1, '0', 2232),
(12, 'anaaa', 'azzz', '2017-09-06 23:57:37', 1, '0', 22),
(13, 'anaaa', 'azzzaa', '2017-09-06 23:59:25', 1, '0', 22),
(14, 'anaaa', 'azzzaaeee', '2017-09-07 00:00:50', 1, '0', 22),
(15, 'anaaa', 'azzzcc', '2017-09-07 00:03:07', 1, '0', 22),
(16, 'anaaa', 'azzzccadxas', '2017-09-07 00:03:36', 1, '0', 22),
(17, 'anaaadsgvfd', 'azzzccsgfrdsg', '2017-09-07 00:05:04', 1, '0', 22),
(18, 'www', 'ww', '2017-09-07 00:09:14', 1, '0', 22),
(19, 'uuuuuuuu', 'uuuu', '2017-09-07 00:11:22', 1, '0', 22),
(20, 'uuuuuuuu', 'uuuuuuuu', '2017-09-07 00:12:59', 1, '0', 22),
(21, 'uuuuuuuu', 'uuuuuuuuuuh', '2017-09-07 00:17:16', 1, '0', 22),
(22, 'uuuuuuuu', 'uuuuuuuuuuhaa', '2017-09-07 00:18:08', 1, '0', 22),
(23, 'zzzzz', 'zzzzz', '2017-09-07 00:19:46', 1, '0', 22);

-- --------------------------------------------------------

--
-- Table structure for table `product_description`
--

CREATE TABLE `product_description` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `detail_attributes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `is_thumb` enum('0','1') NOT NULL,
  `is_small` enum('0','1') NOT NULL,
  `is_base` enum('0','1') NOT NULL,
  `is_featured` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float(8,2) NOT NULL,
  `special_price` float(8,2) DEFAULT NULL,
  `cost_price` float(8,2) DEFAULT NULL,
  `dealers_price` float(8,2) NOT NULL,
  `type` enum('simple') NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_price`
--

INSERT INTO `product_price` (`id`, `product_id`, `price`, `special_price`, `cost_price`, `dealers_price`, `type`, `from_date`, `to_date`) VALUES
(1, 3, 4560.00, 0.00, 0.00, 0.00, 'simple', '0000-00-00', '0000-00-00'),
(2, 4, 5645.00, 0.00, 0.00, 0.00, 'simple', '0000-00-00', '0000-00-00'),
(3, 5, 123.00, 12.00, 32.00, 232.00, 'simple', '0000-00-00', '0000-00-00'),
(4, 6, 33.00, 33.00, 33.00, 33.00, 'simple', '0000-00-00', '0000-00-00'),
(5, 9, 334.00, NULL, 444.00, 343.00, 'simple', '0000-00-00', '0000-00-00'),
(6, 10, 334.00, NULL, 444.00, 343.00, 'simple', '0000-00-00', '0000-00-00'),
(7, 22, 333.33, NULL, 333.33, 333.33, 'simple', '1970-01-01', '1970-01-01'),
(8, 23, 55.55, 55.55, 55.55, 55.55, 'simple', '2017-09-19', '2017-09-30');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `test_name` varchar(255) DEFAULT NULL,
  `test_data` varchar(255) DEFAULT NULL,
  `test_uni` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `test_name`, `test_data`, `test_uni`, `status`) VALUES
(1, 'test', 'test data', 'test-1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `url_key`
--

CREATE TABLE `url_key` (
  `id` int(11) NOT NULL,
  `url_key` varchar(255) NOT NULL,
  `type` enum('product') NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user') COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `level` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `display_name`, `user_type`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `image`, `status`, `created_at`, `updated_at`, `level`, `created_by`) VALUES
(1, 'admin', 'Muhammed Muhsin', 'admin', '4gy_jvkWiDIQmT79sk_D-rEy4LBQu1sR', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', NULL, 'muhsin.zyne@gmail.com', '', 10, 1487064707, 1487064707, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indexes for table `product_description`
--
ALTER TABLE `product_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `url_key`
--
ALTER TABLE `url_key`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `attribute_options`
--
ALTER TABLE `attribute_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `meta`
--
ALTER TABLE `meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `product_description`
--
ALTER TABLE `product_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_price`
--
ALTER TABLE `product_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `url_key`
--
ALTER TABLE `url_key`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
