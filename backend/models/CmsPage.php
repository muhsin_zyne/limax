<?php

namespace backend\models;

use Yii;
use common\models\UrlKey;

/**
 * This is the model class for table "cms_page".
 *
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $content
 * @property string $status
 */
class CmsPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'meta_title', 'meta_description', 'content', 'status'], 'required'],
            [['content', 'status'], 'string'],
            [['title', 'meta_title', 'meta_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'content' => 'Content',
            'status' => 'Status',
        ];
    }
    
    public function getUrlKey() {
        return $this->hasOne(UrlKey::className(),['page_id'=>'id']);
    }
}
