<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>





<!DOCTYPE html>
<head>
<style>a{color:#3a3138;}</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="background:#eeeeee;margin:8px 0 0;">



<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
        	<td align="center"><a href=""><img  alt="banner" src="http://cdn.limaxgroup.co.in/store/assets/password-reset.jpg"></a></td>
        </tr>
        
        <tr>
        	<td style="padding:30px 20px 0;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                	<tr>
                    	<td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Hello <?= Html::encode($user->username) ?>,</td>
                    </tr>
                    <tr>
                    	<td style="font-size:16px;">We received a request to reset the password associated with this email address. 
Please click on the link below </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                           <?= Html::a(Html::encode($resetLink), $resetLink) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                            If you did not request to have your password reset you can safely ignore this email. Rest assured your customer account is safe.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                            If you repeatedly receive these password reset notifications or feel that your account may be subject to attempted fraudulent activity, please notify us as soon as posible using the <span style="text-decoration:underline;"><a href="http://limaxgroup.co.in/site/contact" target="_blank"> Contact Us<a> </span> form on our website.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                            Thank you!
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
    </tbody>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#fff;">
    <tbody>
        <!-- <tr>
          <td bgcolor="#1d1d1d" width="100%" style="">
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="width:600px!important;padding:18px 0;font-family:Arial, Helvetica, sans-serif; font-size:11px;text-align:center;color:#211e21;">
                <tr>
                    <td style="text-align:center;">
                    	<a href=""><img  alt="" src="http://everybuyrewards.com.au/Design/leadingappliances/email-template/html/foot-logo.jpg" alt=""></a>
                    </td> 
                </tr>
            </table>
          </td>
        </tr> -->
        
        
        <tr>
          <td style="padding:12px 0;">
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="width:600px!important;font-family:Arial, Helvetica, sans-serif; font-size:12px;text-align:center;color:#211e21;">
                <tr>
                    <td style="padding:0 0 2px;">Copyright � Limax Group <?=date('Y')?> Powerd By RangeTech Solutions | This email was sent to 
                    <a href="" style="color:#211e21;text-decoration:none;"><?=$user->email?></a></td>
                </tr>
            </table>
          </td>
        </tr>
    </tbody>
</table>
</body>