<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Our Products';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="containerproduct">
    <div class="raw margin-top-min-75">
        <div class="site-product">          
               <div id="content-block">
        <div class="content-center fixed-header-margin">
            <img class="project-thumbnail" src="/themes/shopping/img/banner.jpg" alt="" />
            <div class="content-push">
               
                <div class="information-blocks">
                    <div class="row">
                        
                        <div class="col-md-12 information-entry">
                            <div class="product-heading">
                                <div class="raw">
                                     <h2>OUR PRODUCTS</h2>
                                </div>                                                               
                            </div>
                            <div class="blog-landing-box type-4 columns-3">
                                <div class="blog-entry">
                                    <a class="image hover-class-1" href="/site/product_view"><img src="/themes/shopping/img/p1.jpg" alt="" /><span class="hover-label">ART NO: AGO2<br>SIZE: 6 - 10</span></a>
                                    
                                </div>
                                <div class="blog-entry">
                                    <a class="image hover-class-1" href="/site/product_view"><img src="/themes/shopping/img/p2.jpg" alt="" /><span class="hover-label">ART NO: AGO2<br>SIZE: 6 - 10</span></a>
                                  
                                </div>
                                <div class="blog-entry">
                                    <a class="image hover-class-1" href="/site/product_view"><img src="/themes/shopping/img/p3.jpg" alt="" /><span class="hover-label">ART NO: AGO2<br>SIZE: 6 - 10</span></a>
                                   
                                </div>
                                <div class="blog-entry">
                                    <a class="image hover-class-1" href="/site/product_view"><img src="/themes/shopping/img/p4-2.jpg" alt="" /><span class="hover-label">ART NO: AGO2<br>SIZE: 6 - 10</span></a>
                                   
                                </div>
                                <div class="blog-entry">
                                    <a class="image hover-class-1" href="/site/product_view"><img src="/themes/shopping/img/p6.jpg" alt="" /><span class="hover-label">ART NO: AGO2<br>SIZE: 6 - 10</span></a>                                   
                                </div>
                                <div class="blog-entry">
                                    <a class="image hover-class-1" href="/site/product_view"><img src="/themes/shopping/img/p2.jpg" alt="" /><span class="hover-label">ART NO: AGO2<br>SIZE: 6 - 10</span></a>                                    
                                </div>
                            </div>
                           
                        </div>

                    </div>
                </div>            
            </div>
        </div>
        <div class="clear"></div>
    </div>

        </div>
    </div>  
</div>
