<?php

namespace backend\controllers;

use Yii;
use common\models\Products;
use common\models\Meta;
use common\models\UrlKey;
use common\models\ProductPrice;
use common\models\ProductImages;
use common\models\ProductDescription;
use common\models\ProductAttributes;
use common\models\search\ProductSearch;
use backend\controllers\SiteController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends SiteController
{
    /**
     * @inheritdoc
     */
    public $uploadThumbnailPath;
    public $uploadSmallPath;
    public $uploadBasePath;
    public function init(){
        $this->uploadThumbnailPath = \Yii::$app->basePath."/../store/products/images/thumbnail/";
        $this->uploadSmallPath = \Yii::$app->basePath."/../store/products/images/small/";
        $this->uploadBasePath = \Yii::$app->basePath."/../store/products/images/base/";
        parent::init();
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {   

        //$extension = pathinfo('branding/googlelogo/1x/googlelogo_color_150x54dp.ng', PATHINFO_EXTENSION);
           //     echo'<pre>';
          //      print_r($extension); die();
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImageUplaod($type) {

        if($type==0) {
            /*
            if(isset($_FILES)) {
                foreach ($_FILES as $FILE) {
                            echo'<pre>';
                            print_r($FILE['name'][0]); die();
                }
            }*/



            if(isset($_FILES)){

                
                        
                $file = $_FILES['Products']['name'][0];

                $ext = pathinfo($file, PATHINFO_EXTENSION);
                if($ext == "jpg")
                    $source_image = imagecreatefromjpeg($_FILES['Products']['tmp_name'][0]);
                 if($ext == "jpeg")
                    $source_image = imagecreatefromjpeg($_FILES['Products']['tmp_name'][0]);
                if($ext == "png")
                    $source_image = imagecreatefrompng($_FILES['Products']['tmp_name'][0]);
                $width = imagesx($source_image);
                $height = imagesy($source_image);

                $fileName =  'sdsdddd'.basename($_FILES['Products']['name'][0]);
                
                $uploadThumbFile = $this->uploadThumbnailPath . $fileName;
                $uploadSmallFile = $this->uploadSmallPath . $fileName;
                $uploadBaseFile = $this->uploadBasePath . $fileName;
                
                //var_dump($uploadThumbFile);die();

                /*Thubnail image resizing*/

                $uploadfile = $this->uploadThumbnailPath.basename($_FILES['Products']['name'][0]);
                $thumb_width = 200;
                $thumb_height = 200;
                $virtual_image = imagecreatetruecolor($thumb_width, $thumb_height);
                
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
                $im = imagejpeg($virtual_image, $uploadThumbFile);

                //var_dump($im);die();

                /* Small Image*/

                $small_width = 350;
                $small_height = 350;
                $virtual_image = imagecreatetruecolor($small_width, $small_height);
                
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $small_width, $small_height, $width, $height);
                $im = imagejpeg($virtual_image, $uploadSmallFile); 

                /* Base Image*/

                $base_width = 600;
                $base_height = 600;
                $virtual_image = imagecreatetruecolor($base_width, $base_height);
                
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $base_width, $base_height, $width, $height);
                $im = imagejpeg($virtual_image, $uploadBaseFile); 


                $responce = [
                    'status'=>'true',
                ];

                return json_encode($responce,true);
            
          }

        }
               
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $activeUser = $this->getActiveUser();
        if ($model->load(Yii::$app->request->post()) && $model->validate() ) {
            
            

            $model->created_at = $this->getNowTime();
            $model->created_by = $activeUser->id;
          
            if($model->save()) {        
                $prductPriceModel = new ProductPrice();                
                $prductPriceModel->product_id = $model->id;
                $prductPriceModel->price = $model->price;
                $prductPriceModel->dealers_price = $model->dealers_price;
                $prductPriceModel->special_price = $model->special_price;
                $prductPriceModel->cost_price = $model->cost_price;
                $prductPriceModel->from_date = date('Y-m-d',strtotime($model->from_date));
                $prductPriceModel->to_date = date('Y-m-d',strtotime($model->to_date));
                $prductPriceModel->save();

                $ProductAttributes = new ProductAttributes();
                $ProductAttributes->product_id = $model->id; 
                $ProductAttributes->brand_id = $model->brand_id;
                $tempProductType = implode(', ',$model->product_type);            
                $ProductAttributes->product_type = $tempProductType;
                $tempProductCategory = implode(', ', $model->product_category);
                $ProductAttributes->product_category = $tempProductCategory;              
                $ProductAttributes->save();

                $prductMetaModel = new Meta(); 
                $prductMetaModel ->type = 'products';
                $prductMetaModel ->product_id = $model->id;
                $prductMetaModel ->meta_title = $model->meta_title;
                $prductMetaModel ->meta_description = $model->meta_description;
                $prductMetaModel ->meta_keyword = $model->meta_keyword;
                $prductMetaModel ->save();

                $productDescriptionModel = new ProductDescription();
                $detail_attributes = $_POST['detail_attributes'];
                $generatedAttributes = [];

                foreach ($detail_attributes[0] as $key => $value) {
                    $generatedAttributes[] = [
                        'attribute'=>$value,
                        'value' =>$detail_attributes[1][$key],
                    ];
                }
                
                $productDescriptionModel->detail_attributes = json_encode($generatedAttributes,true);
            
                $productDescriptionModel->product_id = $model->id;
                $productDescriptionModel->short_description = $model->short_description;
                $productDescriptionModel->description = $model->description;
                $productDescriptionModel->save();


                $urlKeyModel = new UrlKey();
                $urlKeyModel->type ='product';
                $urlKeyModel->url_key = $this->generateUrlKey($model->title);
                $urlKeyModel->product_id = $model->id;
                $urlKeyModel->save();


                        
                return $this->redirect(['/products/update','id'=>$model->id]);
            }
            else {
                        echo'<pre>';
                        print_r($model->getErrors()); die();
            }
        } 
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        

        $prductPriceModel = ProductPrice::find()->where(['product_id'=>$model->id])->one();
        $prductMetaModel = Meta::find()->where(['product_id'=>$model->id])->one();
        $productDescriptionModel = ProductDescription::find()->where(['product_id'=>$model->id])->one();
        $productImageModel  = ProductImages::find()->where(['product_id'=>$model->id])->all();

        $model->price = $prductPriceModel->price;
        $model->dealers_price = $prductPriceModel->dealers_price;
        $model->cost_price = $prductPriceModel->cost_price;
        $model->special_price = $prductPriceModel->special_price;
        $model->from_date = $prductPriceModel->from_date;
        $model->to_date = $prductPriceModel->to_date;

        $model->meta_title = $prductMetaModel->meta_title;
        $model->meta_description = $prductMetaModel->meta_description;
        $model->meta_keyword = $prductMetaModel->meta_keyword;

        $model->description = $productDescriptionModel->description;
        $model->short_description = $productDescriptionModel->short_description;
        if(!empty($model->productAttributes)) {
            $model->brand_id = $model->productAttributes->brand_id;
            $model->product_type = explode(', ', $model->productAttributes->product_type);
            $model->product_category = explode(', ', $model->productAttributes->product_category);
            
        }
        
        if ($model->load(Yii::$app->request->post())) {
               
                $detail_attributes = $_POST['detail_attributes'];
                $generatedAttributes = [];

                foreach ($detail_attributes[0] as $key => $value) {
                    $generatedAttributes[] = [
                        'attribute'=>$value,
                        'value' =>$detail_attributes[1][$key],
                    ];
                }
                if($model->title!=$model->oldAttributes['title']) {
                    $model->urlKey->url_key = $this->generateUrlKey($model->title);
                    $model->urlKey->save();
                }
                $productDescriptionModel->detail_attributes = json_encode($generatedAttributes,true);
            
                $prductPriceModel->price = $model->price;
                $prductPriceModel->dealers_price = $model->dealers_price;
                $prductPriceModel->special_price = $model->special_price;
                $prductPriceModel->cost_price = $model->cost_price;
                $prductPriceModel->from_date = date('Y-m-d',strtotime($model->from_date));
                $prductPriceModel->to_date = date('Y-m-d',strtotime($model->to_date));
                $prductPriceModel->save();

                $prductMetaModel ->meta_title = $model->meta_title;
                $prductMetaModel ->meta_description = $model->meta_description;
                $prductMetaModel ->meta_keyword = $model->meta_keyword;
                $prductMetaModel ->save();



                $model->productAttributes->brand_id = $model->brand_id;         
                $model->productAttributes->product_type = implode(', ',$model->product_type);
                $model->productAttributes->product_category = implode(', ', $model->product_category);            
                $model->productAttributes->save();

                
                $productDescriptionModel->short_description = $model->short_description;
                $productDescriptionModel->description = $model->description;
                $productDescriptionModel->save();
                


            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'productImageModel'=>$productImageModel,
                'productAttributes'=> $model->productAttributes,
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    
     */


    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function generateUrlKey($title) {
        
        $title = strtolower($title);
        $title = $this->replacekey([',','/','_',' ','@','$','!','<','>','?','__','--'],$title);
        
        $title = $this->validateUrlKey($title);
        
        return $title;

    }
    public function replacekey($replceOb,$stiring) {
        foreach ($replceOb as $value) {
            $stiring = str_replace($replceOb, '-', $stiring);
        }

        return $stiring;
    }

    public function validateUrlKey($url_key) {
        $urlKeyModel = new UrlKey();
        $responce = $urlKeyModel->validateUrl($url_key);
        return $responce;
    }

}
