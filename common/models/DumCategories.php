<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $icon
 * @property string $image
 * @property string $status
 * @property integer $parent
 * @property integer $position
 * @property string $readonly
 * @property string $url_key
 * @property string $meta_description
 * @property string $meta_keywords
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'image', 'status', 'readonly', 'url_key', 'meta_description', 'meta_keywords'], 'string'],
            [['title'],'required'],
            [['title','description','icon','url_key','readonly','meta_description','meta_keywords','status'],'required'],
            [['parent', 'position'], 'integer'],
            [['title', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Category Title',
            'description' => 'Description',
            'icon' => 'Fetured Icon',
            'image' => 'Image',
            'status' => 'Status',
            'parent' => 'Parent',
            'position' => 'Position',
            'readonly' => 'Accecibilty Read only',
            'url_key' => 'Category slug',   
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }


    public function getList($items) {
        $elements = [];
        foreach ($items as $item) {
            if($item['parent']==0) {
                $elements [] = [
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'url_key' => $item['url_key'],
                    'items' => $this->getListChild($items,$item['id']),
                ];
            }
        }
        
        $result = $this->getCategoriesList($elements,FALSE);   
        return $result;
    }
    public function getListChild($new_items,$parent) {
        $new_elements  = [];
        foreach ($new_items as $new_item) {
            if($new_item['id']!= $parent && $new_item['parent']==$parent) {
                $new_elements [] = [
                    'id' => $new_item['id'],
                    'title' => $new_item['title'],
                    'url_key' => $new_item['url_key'],
                    'items' => $this->getListChild($new_items,$new_item['id']),
                ];
            }
        }
        return $new_elements;
    }

    public function getCategoriesList($items,$child=FALSE) {
            $html = '<ol class="dd-list">';
            foreach ($items as $item) {
                $html.='<li class="dd-item dd3-item" data-id="'.$item['id'].'">';
                $html.='<div class="dd-handle dd3-handle"></div>';
                $html.=' <div class="dd3-content">'.$item['title'];
                $html.='<u class="menu-icon-set">'.Html::a('<i class="fa fa-pencil"></i> Edit', ['/categories/update','id'=>$item['id']],['class'=>'bootstrap-modal-new hover-black','data-title' => 'Update Menu', 'data-width'=>'70%', ]).'</u>

                <u class="menu-icon-set">'.Html::a('<i class="fa fa-eye"></i> view', ['/categories/view','id'=>$item['id']],['class'=>'bootstrap-modal-new hover-black','data-title' => 'Update Menu', 'data-width'=>'70%', ]).'</u>

                <u class="menu-icon-set">'.Html::a('<i class="fa fa-plus"></i> new ', ['/categories/create-child','child'=>$item['id']],['class'=>'bootstrap-modal-new hover-black','data-title' => 'Child category for - '.$item['title'], 'data-width'=>'70%', ]).'</u>


                ';

               
                $html.='</div>';
                $html.= $this->getCategoriesList($item['items'],$item['id']);
                $html.='</li>';
            }
            $html.='</ol>';



        return $html;
    }
}
