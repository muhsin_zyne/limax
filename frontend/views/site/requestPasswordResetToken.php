<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="container">   
    <div class="row">
        <div class="site-request-password-reset" style="padding: 15px; padding-bottom: 20px">
            <div clsass="col-xs-12">
                <div class="article-container style-1">
                    <h3> <?= Html::encode($this->title) ?> </h3>
                    <p>Please fill out your email. A link to reset password will be sent there.</p>
                </div>
                 <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary pull-right']) ?>
                    </div>
                    <div class="celarfix"> </div>
                <?php ActiveForm::end(); ?>

            </div>               
        </div>
    </div>
</div>


