<?php 

namespace backend\components\widgets;
use yii\base\Widget;
use yii\helpers\Html;

class MaterialCheckbox extends Widget
{
    public $label;
    public $id;
    public $value;
    
    public function init()
    {   

        if($this->label===null) {
            $this->label ='Widget Label';
        }
        if($this->id===null) {
            $this->id = 'WidgetId';
        }
        if($this->value===null) {
            $this->value = '';
        }

        $results = $this->getCheckbox();
        echo $results;
        
    }

    public function run()
    {   
        
    }

    protected function getCheckbox() {
        $html = '<div class="conf-box">';
        $html.= '<div class="conf-label">';
        $html.=' <label> '.$this->label.' </label>';
        $html.=' </div>';
        $html.=' <div class="conf-action">';
        $html.=' <label class="switch switch--material">';
        $html.=' <input type="checkbox" class="switch__input switch--material__input" id="'.$this->id.'" value="'.$this->value.'">';
        $html.='<div class="switch__toggle switch--material__toggle">';
        $html.= '<div class="switch__handle switch--material__handle">';
        $html.='</div>';
        $html.='</div>';
        $html.='</label>';
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="clearfix"></div>';
        return $html;

    }
}


?>