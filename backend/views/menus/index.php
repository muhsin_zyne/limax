<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Menus;
use backend\components\widgets\CustomModal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MenusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$menuModal = new Menus;

$this->title = 'Menus';
$this->params['breadcrumbs'][] = $this->title;


?>




<div class="menus-index">


    
    <div class="row">
        <div class="col-xs-12">
            <div class="btn-group pull-right" id="nestable-menu">
                <?= Html::a('Create Menu', ['create','type'=>$type], ['class' => 'btn btn-success theme-btn bootstrap-modal-new', 'data-title' => 'Create Menu', 'data-width'=>'30%']) ?>
                <?= Html::button('Expand All', ['class' => 'btn btn-primary theme-btn ', 'type'=>'button', 'data-action'=>'expand-all']) ?>
                <?= Html::button('Collapse All', ['class' => 'btn btn-danger theme-btn ', 'type'=>'button', 'data-action'=>'collapse-all']) ?>
                <?= Html::button('Update Changes', ['class' => 'btn btn-success theme-btn ', 'type'=>'button','id'=>'applay-change']) ?>
               
            </div>
        </div>
    </div>

    

    <div class="row" id="SELECTOR">
        <div class="col-xs-6">

            <div class="dd drag-box" id="nestable">
                <span> Assigned Menu List</span>
                <?php 
                    if(count($assigned_menu)!=0) {
                        echo $menuModal->getMenuList($assigned_menu);
                    }
                    else {
                        echo " <pre> NO Data </pre>";
                    }
                ?>
                <textarea id="nestable-output" style="display:none"></textarea>
            </div>

        </div>
        <div class="col-xs-6">
            <div class="dd drag-box" id="nestable2">
                <span> Unassigned Menu List</span>
                <?php
                    if(count($un_assigned_menu)!=0) {
                        echo $menuModal->getMenuList($un_assigned_menu);
                    }
                    else {
                       echo " <pre> NO Data </pre>";
                    }

                ?>
                <textarea id="nestable2-output" style="display:none"></textarea>
            </div>


        </div>
    </div>     
    
</div>














<script>

$(document).ready(function()
{

    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // activate Nestable for list 2
    $('#nestable2').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data

    updateOutput($('#nestable').data('output', $('#nestable-output')));
    updateOutput($('#nestable2').data('output', $('#nestable2-output')));

    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });

    $('#nestable3').nestable();

});
</script>




<script> 
$(document).ready(function(){



    $("#applay-change").click(function() {
        run_waitMe();
        var assignedMenu = $("#nestable-output").val();
        var un_assigned_menu = $("#nestable2-output").val(); 
        jQuery.ajax({
            method: 'POST',
            url: '/menus/update-menu',
            crossDomain: false,
            data:{signedMenu:assignedMenu,unsignedMenu:un_assigned_menu},
            success: function(responseData, textStatus, jqXHR) {
                $(".waitMe").hide();
                location.reload();
            },
            error: function (responseData, textStatus, errorThrown) {
                alert(textStatus);
            },
        }); 


    });






function run_waitMe(effect){
$('#SELECTOR').waitMe({

//none, rotateplane, stretch, orbit, roundBounce, win8, 
//win8_linear, ios, facebook, rotation, timer, pulse, 
//progressBar, bouncePulse or img
effect: 'timer',

//place text under the effect (string).
text: 'Please wait...',

//background for container (string).
bg: 'rgba(255,255,255,0.7)',

//color for background animation and text (string).
color: '#000',

//change width for elem animation (string).
sizeW: '',

//change height for elem animation (string).
sizeH: '',

// url to image
source: '',

// callback
onClose: function() {}

});
}
    



});
</script>























    <style type="text/css">

    .dd,.dd-list{display:block;padding:0;list-style:none}.dd-handle,.dd3-content,a:hover{text-decoration:none}.dd-handle,.dd-item>button{font-weight:700;margin:5px 0}.dd-item>button,.dd3-handle{white-space:nowrap;overflow:hidden}#nestable2 .dd-item>button:before{color:#337ab7!important}.cf:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0}* html .cf{zoom:1}html{margin:0;padding:0}body{margin:0;font-family:'Helvetica Neue',Arial,sans-serif}h1{font-size:1.75em;margin:0 0 .6em}a{color:#2996cc}p{line-height:1.5em}.small{color:#666;font-size:.875em}.large{font-size:1.25em}.dd,.dd-empty,.dd-item,.dd-placeholder{margin:0;font-size:13px;line-height:20px;position:relative}.dd{max-width:600px}.dd-list{position:relative;margin:0}.dd-list .dd-list{padding-left:30px}.dd-collapsed .dd-list{display:none}.dd-empty,.dd-item,.dd-placeholder{display:block;padding:0;min-height:20px}.dd-handle{display:block;height:38px;padding:8px 10px;color:#333;border:1px solid #ccc;-webkit-border-radius:3px;border-radius:3px;box-sizing:border-box;-moz-box-sizing:border-box}.dd-handle:hover{color:#2ea8e5}.dd-item>button{display:block;position:relative;cursor:pointer;float:left;width:28px;height:20px;padding:0;text-indent:100%;border:0;background:0 0;font-size:12px;line-height:1;text-align:center;color:#3c8dbc}.dd-item>button:before{content:'\f067';display:block;position:absolute;width:100%;text-align:center;text-indent:0;font-family:FontAwesome}.dd-item>button[data-action=collapse]:before{content:'\f068'}.dd-empty,.dd-placeholder{margin:5px 0;padding:0;min-height:30px;background:#f2fbff;border:1px dashed #b6bcbf;box-sizing:border-box;-moz-box-sizing:border-box}.dd-empty{border:1px dashed #bbb;min-height:100px;background-color:#e5e5e5;background-image:-webkit-linear-gradient(45deg,#fff 25%,transparent 25%,transparent 75%,#fff 75%,#fff),-webkit-linear-gradient(45deg,#fff 25%,transparent 25%,transparent 75%,#fff 75%,#fff);background-image:-moz-linear-gradient(45deg,#fff 25%,transparent 25%,transparent 75%,#fff 75%,#fff),-moz-linear-gradient(45deg,#fff 25%,transparent 25%,transparent 75%,#fff 75%,#fff);background-image:linear-gradient(45deg,#fff 25%,transparent 25%,transparent 75%,#fff 75%,#fff),linear-gradient(45deg,#fff 25%,transparent 25%,transparent 75%,#fff 75%,#fff);background-size:60px 60px;background-position:0 0,30px 30px}.dd-dragel{position:absolute;pointer-events:none;z-index:9999}.dd-dragel>.dd-item .dd-handle{margin-top:0}.dd-dragel .dd-handle{-webkit-box-shadow:2px 4px 6px 0 rgba(0,0,0,.1);box-shadow:2px 4px 6px 0 rgba(0,0,0,.1)}.nestable-lists{display:block;clear:both;padding:30px 0;width:100%;border:0;border-top:2px solid #ddd;border-bottom:2px solid #ddd}#nestable-menu{padding:0;margin:20px 0}#nestable-output,#nestable2-output{width:100%;height:7em;font-size:.75em;line-height:1.333333em;font-family:Consolas,monospace;padding:5px;box-sizing:border-box;-moz-box-sizing:border-box}#nestable2 .dd-handle{color:#fff;border:1px solid #367fa9;background:-webkit-linear-gradient(top ,#3c8dbc 0,#367fa9 100%);background:-moz-linear-gradient(top ,#3c8dbc 0,#367fa9 100%);background:linear-gradient(top ,#3c8dbc 0,#367fa9 100%);cursor:move}#nestable2 .dd-handle:hover{background:#3c8dbc}@media only screen and (min-width:700px){.dd{float:left;width:100%}.dd+.dd{margin-left:2%}}.dd-hover>.dd-handle{background:#2ea8e5!important}.dd3-content{display:block;margin:5px 0;padding:8px 10px 8px 40px;color:#333;font-weight:700;border:1px solid #ccc;background:#fafafa;background:-webkit-linear-gradient(top,#fafafa 0,#eee 100%);background:-moz-linear-gradient(top,#fafafa 0,#eee 100%);background:linear-gradient(top,#fafafa 0,#eee 100%);-webkit-border-radius:3px;border-radius:3px;box-sizing:border-box;-moz-box-sizing:border-box;transition:all ease-in-out}.dd3-content:hover{color:#3c8dbc;background:#fff}.dd-dragel>.dd3-item>.dd3-content{margin:0}.dd3-item>button{margin:10px 0 10px 35px}.dd3-handle{position:absolute;margin:0;left:0;top:0;cursor:move;width:35px;text-indent:100%;border:1px solid #367fa9;background:#3c8dbc;background:-webkit-linear-gradient(top,#3c8dbc 0,#367fa9 100%);background:-moz-linear-gradient(top,#3c8dbc 0,#367fa9 100%);background:linear-gradient(top,#3c8dbc 0,#367fa9 100%);border-top-right-radius:0;border-bottom-right-radius:0}.dd3-handle:before{content:'≡';display:block;position:absolute;left:0;top:8px;width:100%;text-align:center;text-indent:0;color:#fff;font-size:23px;font-weight:400}.dd3-handle:hover{background:#367fa9}.drag-box{box-shadow:0 4px 18px #bdbdbd;padding:20px 15px 15px}.drag-box span{margin-left:-5px;font-size:16px;padding-bottom:10px}u.menu-icon-set{float:right;padding:0 7px}u.menu-icon-set a.danger:hover{color:#f00;transition:color .2s ease} a.hover-black:hover{color:#000;transition:color .2s ease}
    

    </style>