<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "url_key".
 *
 * @property integer $id
 * @property string $url_key
 * @property string $type
 * @property string $status
 */
class UrlKey extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'url_key';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url_key', 'type'], 'required'],
            [['type'], 'string'],
            [['product_id','page_id'],'safe'],
            [['url_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url_key' => 'Url Key',
            'type' => 'Type',
        ];
    }
    public function validateUrl($url_key) {
        $data = UrlKey::find()->where(['url_key'=>$url_key])->one();
        $i=1;
        while (!empty($data)) {
            $i++;
            $genarate_status = $this->generateUrl($url_key,$i);
            if($genarate_status==true) {
                return $url_key.'-v-'.$i;
            } 
        }
        if(empty($data)&& $i>1) {
            return $url_key.'-v-'.$i;
        }
        else if($i==1) {
            return $url_key;
        }
        
        
    }

    public function generateUrl($url_key,$i) {
        $data = UrlKey::find()->where(['url_key'=>$url_key.'-v-'.$i])->one();
        if(empty($data)) {
            return true;
        }
        else {
            return false;
        }
    }
}
