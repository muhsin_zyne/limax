<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\FontAwesomeIcon;

$font = new FontAwesomeIcon;
$fontAwesomeIcons = $font->getIcons();


/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="categories-form">
<?php $form = ActiveForm::begin([]); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'icon')->hiddenInput(['value'=>''])->label(false); ?>

            <div class="form-group">
                <label class="control-label" for="categories-description">Icon</label>
                <a class="btn btn-app" id="changeIcon" data-width="30%" data-title="Icons">
                    <?php if(!$model->isNewRecord && $model->icon !=null) {
                            echo '<i class="fa fa-'.$model->icon.'"></i>'.$model->icon.'';
                        } 
                        else {
                            echo '<i class="fa fa-edit"></i> Icon';
                        }
                        ?>
                    
                </a>
            </div>

            <?php if($model->isNewRecord) {
                  echo $form->field($model, 'url_key')->textInput(['placeholder'=>'url_key']); 
                }
                else {
                   echo $form->field($model, 'url_key')->textInput(['placeholder'=>'url_key','disabled'=>true]);
                }
            ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'meta_description')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'status')->dropDownList([ '1'=>'Active', '0'=>'Inactive'], ['prompt' => 'status']) ?>
            
            <div class="form-group"><?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']) ?> 
            </div>
            
            
        </div>
    </div>
    <?php 
    if(isset($child)) {
       echo  $form->field($model, 'parent')->hiddenInput(['value'=> $child])->label(false);
    }
    ?>

    
    <div class="clearfix"> </div>

    <?php ActiveForm::end(); ?>

</div>


<script> 
$( document).ready(function() {

   $("#changeIcon").click(function(event){
        event.preventDefault();
        $("#modal-pop-select").modal('show')
        .find("#modalContentpop")
        .load('/categories/view-icon?data=');
        $('h4#ModalHeaderID').text($(this).data('title'));
        $("div#modal-pop-select .modal-dialog").css("width", $(this).data('width'));
        
   });

    
}); 
</script>