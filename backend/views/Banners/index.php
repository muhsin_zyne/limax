<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\components\widgets\CustomModal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BannersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banners-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>        
        
        <?= Html::a('Upload New Banner', ['create'], ['class' => 'btn btn-success bootstrap-modal-new pull-right theme-btn','data-title' => 'Upload New Banner',]) ?>

    </p>


     <?= CustomModal::widget([
            'header' => 'Update Banner',
            'id' => 'modal-update',
            'size' => 'modal-xs',
            'body' =>'<div id="modalContentUpdate"> <img src="/images/loading_01.gif" width="10%"> </div>',

        ]); 
    ?> 

    <?= CustomModal::widget([
            'header' => 'View Banner',
            'id' => 'modal-view',
            'size' => 'modal-xs',
            'body' =>'<div id="modalContentView"> <img src="/images/loading_01.gif" width="10%"> </div>',

        ]); 
    ?> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            [
                'header' => 'Banner',
                'format' => 'html',
                'value' => function($model) {
                    return Html::img(yii::$app->params['rootUrl'].'/store/banners/'.$model->file,['width' => '200px']);
                },
            ],
            [
                'header' => 'Uploaded At',
                'value' => function($model) {
                    return date('M d, Y h:i A',strtotime($model->created_at));
                },
            ],
           /* 'updated_at',
            'url_to:url',*/
            // 'valid_from',
            // 'valid_to',

             ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{view}{update}{delete}',
                            'buttons'=>[
                              'update' => function ($url, $model) {    
                                    return  Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                            'data-toggle'=>'tooltip',
                                            'data-placement'=>'top',
                                            'title'=>'Edit',
                                            'value'=>$url,
                                            'class'=>'edit_item',
                                        ]); 
                                    },
                                'view' => function($url,$model) {
                                    return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                                            'data-toggle'=>'tooltip',
                                            'data-placement'=>'top',
                                            'title'=>'View',
                                            'value'=>$url,
                                            'class'=>'view_item',
                                        ]); 

                                },


                          ]                            
                            ],
        ],
    ]); ?>
</div>


<script>

$( document ).ready(function() {
  

   $(".edit_item").click(function(){
        $("#modal-update").modal('show')
        .find("#modalContentUpdate")
        .load($(this).attr('value'));
    });

   $(".view_item").click(function(){
        $("#modal-view").modal('show')
        .find("#modalContentView")
        .load($(this).attr('value'));
    });



});
</script>

