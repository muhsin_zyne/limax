<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$filterCategories = $model->getCategoriesArray();
$filterTypes = $model->getProductTypeArray();
$brands = $model->getBrandsArray();


function findIsChecked($baseArrayName,$location) {
    if($baseArrayName!=null) {
        if(in_array($location, $baseArrayName)) {
            return 'checked';
        }
        else {
            return null;
        }

    }
    else {
        return null;
    }
    
}
/* @var $this yii\web\View */
/* @var $model common\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search search-box computerfilter">

    <?php $form = ActiveForm::begin([
        'action'    => ['index'],
        'method'    => 'get',
        'id'        => 'productSearchForm',
    ]); ?>


 <h1 class="filter-label"> Brands </h1>

<ul>

    <?php
    foreach ($brands as $key => $brand) {
        echo ' <li> <label class="checkbox checkbox--material">
        <input type="checkbox" class="checkbox__input checkbox--material__input" name="ProductSearch[brands]['.$key.']" value="'.$brand->id.'"'.findIsChecked($model['brands'],$brand->id).'>
        <div class="checkbox__checkmark checkbox--material__checkmark"></div>
                 '.$brand->name.'
        </label> </li>';
      
    }


     ?>
 </ul>




 <h1 class="filter-label"> Product Category </h1>
<ul>

    <?php
    foreach ($filterCategories as $key => $cat) {
        echo ' <li> <label class="checkbox checkbox--material">
        <input type="checkbox" class="checkbox__input checkbox--material__input" name="ProductSearch[categories]['.$key.']" value="'.$cat->id.'"'.findIsChecked($model['categories'],$cat->id).'>
        <div class="checkbox__checkmark checkbox--material__checkmark"></div>
                 '.$cat->title.'
        </label> </li>';
      
    }


     ?>
 </ul>


  <h1 class="filter-label">Product Type </h1>

<ul>

    <?php

        
    foreach ($filterTypes as $key => $type) {
        echo ' <li> <label class="checkbox checkbox--material">
        <input type="checkbox" class="checkbox__input checkbox--material__input" name="ProductSearch[product_type]['.$key.']" value="'.$type->id.'"'.findIsChecked($model['product_type'],$type->id).'>
        <div class="checkbox__checkmark checkbox--material__checkmark"></div>
                 '.$type->label.'
        </label> </li>';
      
    }


     ?>


 </ul>

  
   

    <?php ActiveForm::end(); ?> 

</div>
