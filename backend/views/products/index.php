<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">
    <div class="row">
        <div class="col-xs-12">    
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <p>
                <?= Html::a('Create Products', ['create'], ['class' => 'btn btn-success theme-btn pull-right']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'sku',
                    'created_at',
                    'created_by',
                    'status',
                    // 'case_qty',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>            
        </div>        
    </div>
</div>
