<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="raw margin-top-min-75">
        <div class="site-contact">          
            <div id="content-block">
                <div class="content-center fixed-header-margin">
                    <img class="project-thumbnail" src="/themes/shopping/img/banner.jpg" alt="" />
                    <div class="content-push">
                        <div class="site-contact">
                          
                           

                    <div class="information-blocks">
                    <center><h2 class="block-title inline-product-column-title">CONTACT DETAILS</h2></center>
                    <div class="map-box type-2">
                        <div id="map-canvas" data-lat="48.8579353" data-lng="2.2953856" data-zoom="17"></div>
                        <div class="addresses-block">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15715.504719263783!2d76.3080901!3d10.0270753!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5951f316ba13a37e!2sLuLu+Mall!5e0!3m2!1sen!2sin!4v1502941156023" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            <a data-lat="48.8583694" data-lng="2.2960132" data-string="2. Here is some address or email or phone or something else..."></a>
                        </div>
                    </div>
                    <div class="map-overlay-info">
                        <div class="article-container style-1">
                            <div class="cell-view">
                                <h5>Company address</h5>
                                <p>8808 Ave Dermentum, Onsectetur Adipiscing<br>
                                Tortor Sagittis, CA 880986,<br>
                                United States<br>
                                CA 90896<br> 
                                United States<br></p>
                                <h5>Contact Informations</h5>
                                <p><span><i class="fa fa-envelope"></i>&nbsp info@limaxgroup.co.in</span><br>
                                <span><i class="fa fa-phone-square"></i>&nbsp +91 9048 569 874</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="row">
                        <div class="col-md-8 information-entry">
                            <h3 class="block-title main-heading"><span><i class="fa fa-paper-plane"></i> Drop Us A Line</span></h3>
                            <form>
                             <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                                <div class="row">
                                    <div class="col-sm-6">                                        
                                         <?= $form->field($model, 'name')->textInput(['autofocus' => false]) ?>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'email') ?>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-sm-12">
                                        <?= $form->field($model, 'email') ?>

                                        <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
                                        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                                        ]) ?>
                                         <div class="form-group">
                                            <?= Html::submitButton('Submit', ['class' => 'button style-10', 'name' => 'contact-button']) ?>
                                        </div>
                                      
                                    </div>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </form>
                        </div>
                        <div class="col-md-4 information-entry">
                            <h3 class="block-title main-heading"><span><i class="fa fa-clock-o"></i> Trading Hours</span></h3>
                            <div class="article-container style-5">
                                    MON     8.30am  – 05.00pm<br>
                                    TUE     8.30am  – 05.00pm<br>
                                    WED     8.30am  – 05.00pm<br>
                                    THU     8.30am  – 05.30pm<br>
                                    FRI     8.30am  – 05.00pm<br>
                                    SAT     9.00am  – 12.30pm<br>
                                    SUNDAY -      Closed 
                            </div>
                            <div class="share-box detail-info-entry">
                                <div class="title">Connect us in social media</div>
                                <div class="socials-box">
                                    <a href="https://www.facebook.com/limaxgroup/"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>

                        </div>              
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>  
</div>

