<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$filterCategories = $model->getCategoriesArray();
$filterTypes = $model->getProductTypeArray();
$brands = $model->getBrandsArray();



/* @var $this yii\web\View */
/* @var $model common\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search search-box mob">

    <?php $form = ActiveForm::begin([
        'action'    => ['index'],
        'method'    => 'get',
        'id'        => 'productSearchFormMob',
    ]); ?>



<ul id="brandsList">

    <?php
    foreach ($brands as $key => $brand) {
        echo ' <li> <label class="checkbox checkbox--material">
        <input type="checkbox" class="checkbox__input checkbox--material__input" name="ProductSearch[brands]['.$key.']" value="'.$brand->id.'"'.findIsChecked($model['brands'],$brand->id).'>
        <div class="checkbox__checkmark checkbox--material__checkmark"></div>
                 '.$brand->name.'
        </label> </li>';
      
    }


     ?>
 </ul>


<ul id="categoriesList" style="display: none;">

    <?php
    foreach ($filterCategories as $key => $cat) {
        echo ' <li> <label class="checkbox checkbox--material">
        <input type="checkbox" class="checkbox__input checkbox--material__input" name="ProductSearch[categories]['.$key.']" value="'.$cat->id.'"'.findIsChecked($model['categories'],$cat->id).'>
        <div class="checkbox__checkmark checkbox--material__checkmark"></div>
                 '.$cat->title.'
        </label> </li>';
      
    }


     ?>
 </ul>


  
<ul id="filterTypesList" style="display: none;">

    <?php

        
    foreach ($filterTypes as $key => $type) {
        echo ' <li> <label class="checkbox checkbox--material">
        <input type="checkbox" class="checkbox__input checkbox--material__input" name="ProductSearch[product_type]['.$key.']" value="'.$type->id.'"'.findIsChecked($model['product_type'],$type->id).'>
        <div class="checkbox__checkmark checkbox--material__checkmark"></div>
                 '.$type->label.'
        </label> </li>';
      
    }


     ?>


 </ul>

  
   

    <?php ActiveForm::end(); ?> 

</div>
