<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>





<!DOCTYPE html>
<head>
<style>a{color:#3a3138;}</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="background:#eeeeee;margin:8px 0 0;">

<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
        	<td align="center" style="text-align:cente;padding:10px 0px;"><a href=""><img  alt="" src="http://cdn.limaxgroup.co.in/store/assets/logo.png" style="height: 100px"></a></td>
        </tr>
    </tbody>    
</table>

<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
        	<td align="center"><a href=""><img  alt="banner" src="http://cdn.limaxgroup.co.in/store/assets/user-activation.jpg"></a></td>
        </tr>
        
        <tr>
        	<td style="padding:30px 20px 0;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                	<tr>
                    	<td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Hello Admin,</td>
                    </tr>
                    <tr>
                    	<td style="font-size:16px;">You have a new request for Dealer Signup 
                        Please click on the link below </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                           <a href="http://admin.limaxgroup.co.in/b2b-users/index?type=pending"> Click here  </a>
                        </td>
                    </tr>
                    <tr>
                        <table class="table">
                          <tbody>
                            <tr>
                              <th> Full Name </th>
                              <td> <?= $model->display_name ?></td>
                            </tr>
                            <tr>
                              <th> Email </th>
                              <td><?= $model->email ?></td>
                            </tr>
                            <tr>
                              <th> Mobile </th>
                              <td><?= $model->b2bAddressModel->mobile ?></td>
                            </tr>
                           <tr>
                              <th> Shop Name & Address </th>
                              <td><?= $model->b2bAddressModel->shop_name ?> <br> <?= $model->b2bAddressModel->address ?></td>
                            </tr>
                          </tbody>
                        </table>
                    </tr>
                    <tr>
                        <td style="padding:16px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                            Thank you!
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
    </tbody>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#fff;">
    <tbody>
        <!-- <tr>
          <td bgcolor="#1d1d1d" width="100%" style="">
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="width:600px!important;padding:18px 0;font-family:Arial, Helvetica, sans-serif; font-size:11px;text-align:center;color:#211e21;">
                <tr>
                    <td style="text-align:center;">
                    	<a href=""><img  alt="" src="http://everybuyrewards.com.au/Design/leadingappliances/email-template/html/foot-logo.jpg" alt=""></a>
                    </td> 
                </tr>
            </table>
          </td>
        </tr> -->
        
        
        <tr>
          <td style="padding:12px 0;">
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="width:600px!important;font-family:Arial, Helvetica, sans-serif; font-size:12px;text-align:center;color:#211e21;">
                <tr>
                    <td style="padding:0 0 2px;">Copyright � Limax Group <?=date('Y')?> Powerd By RangeTech Solutions | This email was sent to 
                    <a href="" style="color:#211e21;text-decoration:none;"><?=yii::$app->params['adminEmail']?></a></td>
                </tr>
            </table>
          </td>
        </tr>
    </tbody>
</table>
</body>