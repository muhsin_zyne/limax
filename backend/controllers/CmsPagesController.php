<?php

namespace backend\controllers;

use Yii;
use backend\models\CmsPage;
use backend\models\search\CmsPages;
use common\models\UrlKey;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;

/**
 * CmsPagesController implements the CRUD actions for CmsPage model.
 */
class CmsPagesController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmsPage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CmsPages();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CmsPage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CmsPage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CmsPage();

        if ($model->load(Yii::$app->request->post())) {

                $model->save();
                $urlKeyModel = new UrlKey();
                $urlKeyModel->type ='cms';
                $urlKeyModel->url_key = $this->generateUrlKey($model->title);
                $urlKeyModel->page_id = $model->id;
                $urlKeyModel->save();


            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CmsPage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->oldAttributes['title']!=$model->title) {
                
                $model->urlKey->url_key = $this->generateUrlKey($model->title);
                $model->urlKey->save();
               
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CmsPage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {   
        $model = $this->findModel($id);
        $urlKey = urlKey::find()->where(['page_id'=>$model->id])->one();
        $urlKey->delete();
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the CmsPage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsPage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function generateUrlKey($title) {
        
        $title = strtolower($title);
        $title = $this->replacekey([',','/','_',' ','@','$','!','<','>','?','__','--'],$title);
        
        $title = $this->validateUrlKey($title);
        
        return $title;

    }
    public function replacekey($replceOb,$stiring) {
        foreach ($replceOb as $value) {
            $stiring = str_replace($replceOb, '-', $stiring);
        }

        return $stiring;
    }

    public function validateUrlKey($url_key) {
        $urlKeyModel = new UrlKey();
        $responce = $urlKeyModel->validateUrl($url_key);
        return $responce;
    }
}
