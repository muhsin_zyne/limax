<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\money\MaskMoney;
use yii\helpers\Url;
use kartik\file\FileInput;
use kartik\widgets\DatePicker;
use dosamigos\fileupload\FileUploadUI;
use kartik\widgets\Select2;



?>

<?php $form = ActiveForm::begin([
  'options'=>['encytype'=>'multipart/form-data']
]); ?>
    <div class="nav-tabs-custom form-new products-form">
        <ul class="nav nav-tabs">
            <li class="active" id="tab-h-1"><a href="#tab_1" data-toggle="tab" aria-expanded="false">General </a></li>
            <li class="" id="tab-h-2"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Prices</a></li>
            <li class="" id="tab-h-3"><a href="#tab_3" data-toggle="tab" aria-expanded="false">Meta Information</a></li>
            <li class="" id="tab-h-4"><a href="#tab_4" data-toggle="tab" aria-expanded="false">Description</a></li>
            <?php if(!$model->isNewRecord) { ?>
            <li class="" id="tab-h-5"><a href="#tab_5" data-toggle="tab" aria-expanded="false">Images</a></li>
            <?php } ?>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                   <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?> 
                   <?= $form->field($model, 'brand_id')->widget(Select2::classname(), [
                          'data' => $model->generateProductBrandsLists(),
                          'options' => ['placeholder' => 'Select Brands'],
                          'pluginOptions' => [
                              
                              'multiple' => false
                          ],
                      ]);                     
                   ?>
                   <?= $form->field($model, 'status')->dropDownList([ '0'=>'Enable', '1'=>'Disable', ], ['prompt' => 'Status']) ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                   
                   <?= $form->field($model, 'case_qty')->textInput(['maxlength' => true]) ?>
            

                   <?= $form->field($model, 'product_type')->widget(Select2::classname(), [
                          'data' => $model->generateProductTypeLists(),
                          'options' => ['placeholder' => 'Select  Type'],
                          
                          'pluginOptions' => [
                              'allowClear' => true,
                              'multiple' => true,
                          ],
                      ]);                     
                   ?>
                   <?= $form->field($model, 'product_category')->widget(Select2::classname(), [
                          'data' => $model->generateProductCategoryLists(),
                          'options' => ['placeholder' => 'Select  Category'],
                          'pluginOptions' => [
                              'allowClear' => true,
                              'multiple' => true
                          ],
                      ]);                     
                   ?>
                
                    <div class="clearfix"> </div>
                      
                </div>
            </div>

            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_2'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 1']);?>
        
                </div>
              </div>
            </div>

          </div><!-- /.tab-pane -->
          <div class="tab-pane " id="tab_2">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <?= $form->field($model, 'price')->widget(MaskMoney::classname(), [ ]); ?>
                  <?= $form->field($model, 'dealers_price')->widget(MaskMoney::classname(), [ ]); ?>
                  <?= $form->field($model, 'cost_price')->widget(MaskMoney::classname(), [ ]); ?> 
                  
                </div>
                <div class="col-xs-12 col-sm-6">    
                   <?= $form->field($model, 'special_price')->widget(MaskMoney::classname(), [ ]); ?>
                   <?= $form->field($model, 'from_date')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Special price valid from ...'],
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                         'format' => 'yyyy/mm/dd',
                                                          'startDate' => date('Y-m-d')
                                                    ]
                                                  ]); ?>
                   <?= $form->field($model, 'to_date')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Special price valid to ...'],
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                        'format' => 'yyyy/mm/dd',
                                                        'startDate' => date('Y-m-d')
                                                    ]
                                                  ]); ?>
                       
                       
                </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_1'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 2']); ?>
                  <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_3'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 2']);?>
        
                </div>
              </div>
            </div>
          </div><!-- /.tab-pane -->

          <div class="tab-pane " id="tab_3">
            <div class="row">
                <div class="col-xs-12">
                  
                   <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>                
                   <?= $form->field($model, 'meta_description')->textArea(['maxlength' => true,'rows'=>4]) ?>
                   <?= $form->field($model, 'meta_keyword')->textArea(['maxlength' => true,'rows'=>3]) ?>           
                </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_2'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 3']);?>
                  <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_4'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 3']);?>
                 
                </div>
              </div>
            </div>
          </div><!-- /.tab-pane -->

          <div class="tab-pane " id="tab_4">
            <div class="row">
                <div class="col-xs-12">               
                   <?= $form->field($model, 'description')->textArea(['maxlength' => true,'rows'=>2]) ?>
                   <?= $form->field($model, 'short_description')->textArea(['maxlength' => true,'rows'=>4]) ?>  
                </div>
                <div class="col-xs-12">               
                
                  <table id="attribute-Table" class="table table-striped table-bordered detail-view">
                      <tr> 
                        <th style="font-weight: bold;"> Description </th>
                        <th style="font-weight: bold;"> Value </th> 
                      </tr>
                      <?php
                      if(!empty($model->productDescription->detail_attributes)) {
                          $productAttributeJson = $model->productDescription->detail_attributes;
                          $procutAttributeArray = json_decode($productAttributeJson,true);
                      } else {
                        $procutAttributeArray = [];
                      }
                        foreach ($procutAttributeArray as $key => $productAttribute) {
                          
                          if($key==0) {
                              echo 
                              '<tr id="table-source"> 
                              <td> 
                                <div class="form-group">
                                  <input type="text" class="form-control" name="detail_attributes[0][]" placeholder="Attribute" value="'.$productAttribute['attribute'].'">
                                </div>
                              </td>

                              <td>
                                <div class="box-attr" style="float: left; width: 100%">
                                   <div class="form-group" style="float: left; width: 70%;">
                                      <input type="text" class="form-control" name="detail_attributes[1][]" placeholder="Value" value="'.$productAttribute['value'].'">
                                   </div>
                                   <div class="btn-group" style="float: left;width: 21%; margin-top: 14px; margin-left: 6px;"> <a class="btn btn-primary theme-btn" onclick="addAttribute()"> Add </a>   <a class="btn btn-danger theme-btn" onclick="remAttribute($(this))"> Remove </a>  </div>
                                   <div class="clearfix"> </div>
                                </div>
                              </td>


                              ';
                          }
                          else {
                            echo 
                              '<tr> 
                                <td> 
                                  <div class="form-group">
                                    <input type="text" class="form-control" name="detail_attributes[0][]" placeholder="Attribute" value="'.$productAttribute['attribute'].'">
                                  </div>
                                </td>

                                <td>
                                  <div class="box-attr" style="float: left; width: 100%">
                                     <div class="form-group" style="float: left; width: 70%;">
                                        <input type="text" class="form-control" name="detail_attributes[1][]" placeholder="Value" value="'.$productAttribute['value'].'">
                                     </div>
                                     <div class="btn-group" style="float: left;width: 21%; margin-top: 14px; margin-left: 6px;"> <a class="btn btn-primary theme-btn" onclick="addAttribute()"> Add </a>   <a class="btn btn-danger theme-btn" onclick="remAttribute($(this))"> Remove </a>  </div>
                                     <div class="clearfix"> </div>
                                  </div>
                                </td>


                              ';
                          }

                        }

                      ?>
                      <tr id="table-source">  
                         <td> 
                           <div class="form-group">
                              <input type="text" class="form-control" name="detail_attributes[0][]" placeholder="Attribute">
                           </div>
                          </td>
                          <td>
                            <div class="box-attr" style="float: left; width: 100%">
                               <div class="form-group" style="float: left; width: 70%;">
                                  <input type="text" class="form-control" name="detail_attributes[1][]" placeholder="Value">
                               </div>
                               <div class="btn-group" style="float: left;width: 21%; margin-top: 14px; margin-left: 6px;"> <a class="btn btn-primary theme-btn" onclick="addAttribute()"> Add </a>   <a class="btn btn-danger theme-btn" onclick="remAttribute($(this))"> Remove </a>  </div>
                               <div class="clearfix"> </div>
                            </div>
                        </td>
                      </tr>

                 </table>
                </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_3'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 4']);?>
                  <?php if($model->isNewRecord) { ?>
                  <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary ']) ?>
                  <?php } else { ?>
                  <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_5'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 4']);?>
                  <?php } ?>
                 
                </div>
              </div>
            </div>
          </div><!-- /.tab-pane -->

          <?php if(!$model->isNewRecord)  { ?>

          <div class="tab-pane " id="tab_5">
            <div class="row">
                <div class="col-xs-12">

                           <!-- <?=Html::activeInput('file', $model, 'image',$options = ['class'=>'file','id'=>'file-1','multiple'=>'true','data-overwrite-initial'=>'false','data-min-file-count'=>'1']) ?> -->
<label> Product Images </label>
<div class="product-image-block" >

<?= FileUploadUI::widget([
    'model' => $model,
    'attribute' => 'image',
    'url' => ['product-images/uploader', 'product_id' => $model->id],
    'gallery' => true,
    'fieldOptions' => [
        'accept' => 'image/*'
    ],
    'clientOptions' => [
        'maxFileSize' => 2000000
    ],
    // ...
    'clientEvents' => [
        'fileuploaddone' => 'function(e,data) {
                                console.log(data);
                                //e.preventDefault();
                                //console.log(e);
                                console.log(data);
                            }',
        'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
    ],
]); ?>

</div>




<?php

if(isset($productImageModel)) {
  foreach ($productImageModel as $key => $image) {
        if($image->is_featured=='1') {
        ?>

        <script type="text/javascript">
          $(".products-update table tbody.files").append('<tr class="template-download fade in"><td><span class="preview"><a href="<?=yii::$app->params['rootUrl']?>/store/products/images/thumbnail/<?=$image->image?>" title="<?=$image->image?>" download="<?=$image->image?>" data-gallery="<?=yii::$app->params['rootUrl']?>/store/products/images/thumbnail/<?=$image->image?>"> <img style="width:65px;" src="<?=yii::$app->params['rootUrl']?>/store/products/images/thumbnail/<?=$image->image?>"></a> </span>  </td> <td> <p class="name"> <a href="<?=yii::$app->params['rootUrl']?>/store/products/images/thumbnail/<?=$image->image?>" title="<?=$image->image?>" download="<?=$image->image?>" data-gallery="<?=yii::$app->params['rootUrl']?>/store/products/images/thumbnail/<?=$image->image?>"><?=$image->image?></a> </p> </td> <td>  <span class="size"> </span> </td> <td> <button class="btn btn-danger delete" data-type="POST" data-url="<?=yii::$app->params['adminUrl']?>/product-images/delete?id=<?=$image->id?>"> <i class="glyphicon glyphicon-trash"></i> <span>Delete</span>  </button> <input type="checkbox" name="delete" value="1" class="toggle">  </td> </tr>');
        </script>

        <?php
        }
  }
}

 ?>

          </div>
          <div class="clearfix" style="margin-top: 20px"> </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right" style="padding-top:10px;">
                  <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_4'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 5']);?>
                
                  <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn ']) ?>
                </div>
              </div>
            </div>
          </div><!-- /.tab-pane -->

          <?php } ?>

          
        </div><!-- /.tab-content -->
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">

  function addAttribute() {
     var rowAttribute = $("#table-source").html();
     $("#attribute-Table").append('<tr>'+ rowAttribute +'</tr>'),500;
  }

  function remAttribute(row) {
    rowCount = $('#attribute-Table tr').length;
    if(rowCount>2) {
    row.closest('tr').remove();
    }
    else {
      alert("sorry need atleast one");
    }
  }

$( document ).ready(function() {



   $(".next-pane.1").click(function() {
       $("#tab-h-1").removeClass("active");
       $("#tab-h-2").addClass("active"); 
   });
   $(".next-pane.2").click(function() {
       $("#tab-h-2").removeClass("active");
       $("#tab-h-3").addClass("active"); 
   });


   
   $(".previous-pane.2").click(function() {
       $("#tab-h-2").removeClass("active");
       $("#tab-h-1").addClass("active"); 
   });
   $(".previous-pane.3").click(function() {
       $("#tab-h-3").removeClass("active");
       $("#tab-h-2").addClass("active"); 
   });
   $(".previous-pane.4").click(function() {
       $("#tab-h-4").removeClass("active");
       $("#tab-h-3").addClass("active"); 
   });
   $(".previous-pane.5").click(function() {
       $("#tab-h-5").removeClass("active");
       $("#tab-h-4").addClass("active"); 
   });


   function run_waitMe(effect){
    $('#loader-block').waitMe({

    //none, rotateplane, stretch, orbit, roundBounce, win8, 
    //win8_linear, ios, facebook, rotation, timer, pulse, 
    //progressBar, bouncePulse or img
    effect: 'roundBounce',

    //place text under the effect (string).
    text: 'Offer Details...',

    //background for container (string).
    bg: 'rgba(255,255,255,0.7)',

    //color for background animation and text (string).
    color: '#00aff0',

    //change width for elem animation (string).
    sizeW: '',

    //change height for elem animation (string).
    sizeH: '',

    // url to image
    source: '',

    // callback
    onClose: function() {}

    });
  }
  
});
</script>

<style type="text/css">
  table.image_table {
    width: 100%;
  }
  .preview img{
    width: 65px;
  }
  span.size {
    display: none;
  }
  .product-image-block {
    padding: 10px;
    border: 2px solid #afafaf;
  }
</style>