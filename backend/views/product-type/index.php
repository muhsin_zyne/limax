<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\components\widgets\CustomModal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-type-index">

 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product Type', ['create'], ['class' => 'btn btn-success pull-right theme-btn bootstrap-modal-new','data-title' => 'Create New Product Type',]) ?>
    </p>


    <?= CustomModal::widget([
            'header' => 'Update Product Type',
            'id' => 'modal-update',
            'size' => 'modal-xs',
            'body' =>'<div id="modalContentUpdate"> <img src="/images/loading_01.gif" width="10%"> </div>',

        ]); 
    ?> 

    <?= CustomModal::widget([
            'header' => 'View Product Type',
            'id' => 'modal-view',
            'size' => 'modal-xs',
            'body' =>'<div id="modalContentView"> <img src="/images/loading_01.gif" width="10%"> </div>',

        ]); 
    ?> 



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'label',
            
            'created_at',
        

             ['class' => 'yii\grid\ActionColumn',
                          'template'=>'{view}{update}{delete}',
                            'buttons'=>[
                              'update' => function ($url, $model) {    
                                    return  Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                            'data-toggle'=>'tooltip',
                                            'data-placement'=>'top',
                                            'title'=>'Edit',
                                            'value'=>$url,
                                            'class'=>'edit_item',
                                        ]); 
                                    },
                                'view' => function($url,$model) {
                                    return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                                            'data-toggle'=>'tooltip',
                                            'data-placement'=>'top',
                                            'title'=>'View',
                                            'value'=>$url,
                                            'class'=>'view_item',
                                        ]); 

                                },


                          ]                            
                            ],
        ],
    ]); ?>
</div>


<script>

$( document ).ready(function() {
  

   $(".edit_item").click(function(){
        $("#modal-update").modal('show')
        .find("#modalContentUpdate")
        .load($(this).attr('value'));
    });

   $(".view_item").click(function(){
        $("#modal-view").modal('show')
        .find("#modalContentView")
        .load($(this).attr('value'));
    });



});
</script>