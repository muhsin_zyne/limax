<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use kartik\alert\Alert;

$this->title = 'Dealers Accounts';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="raw margin-top-min-75">
        <div class="site-contact">          
            <div id="content-block">
                <div class="content-center fixed-header-margin">
                    <div class="content-push">
                        <div class="information-blocks">
                            <div class="row">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-12 information-entry">
                                    <div class="login-box">
                                        <?php if(isset($user)) {?>
                                        <center>
                                           <div id="animated-block" style="display: none">
                                                <img src="/img/checkmark-png-28.png" width="120px">
                                                <h3 class="success-message-content"> Hello <?=$user->display_name?>, <br> Thank you for being interested to associate with Limax Group Our team will contact you soon</h3>
                                                <h4 style="padding-bottom: 20px">Our concern team reviewing your application it may take upto 3 working days</h4>
                                            </div>
                                            <?=Html::a(' <i class="fa fa-arrow-left" aria-hidden="true"></i> Back to Home',['/site/index'],['class'=>'button style-10'])?>
                                        </center>
                                        <?php } ?>
                                    </div>
                                </div>
                                
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="clear"></div>
            </div>  

        </div>
    </div>  
</div>

<style type="text/css">
    h3.success-message-content {
        font-size: 26px;
        padding: 20px;
        color: #0ebe01;
    }
</style>
<script type="text/javascript">
    $( document ).ready(function() {
        setTimeout(
            function() 
            {
                $("#animated-block").show(400);
            },400);
    });
</script>