<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        /*EXTERNAL LINKS */

        'themes/adminlte/plugins/font-awesome/css/font-awesome.min.css',
        //'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',

        /* ==EXTERNAL LINKS */
        

        'css/site.css',
        'themes/adminlte/bootstrap/css/bootstrap.min.css',
        //'themes/adminlte/dist/css/AdminLTE.min.css',
        'themes/adminlte/dist/css/AdminLTE.css',
        'themes/adminlte/dist/css/skins/_all-skins.min.css',
        'themes/adminlte/plugins/iCheck/flat/blue.css',
        'themes/adminlte/plugins/morris/morris.css',
        'themes/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'themes/adminlte/plugins/datepicker/datepicker3.css',
        'themes/adminlte/plugins/daterangepicker/daterangepicker-bs3.css',
        'themes/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'themes/adminlte/dist/font/flaticon/flaticon.css',
        'themes/adminlte/dist/css/onsen_style.css',
        'themes/adminlte/plugins/waitme/waitMe.css',
        'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.css',
        '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
        //'themes/adminlte/',
    ];
    public $js = [

        'themes/adminlte/dist/js/application_main.js',
        
        //'themes/adminlte/plugins/jQuery/jQuery-2.1.4.min.js',
        'themes/adminlte/bootstrap/js/bootstrap.min.js',
        'themes/adminlte/plugins/morris/morris.min.js',
        'themes/adminlte/plugins/sparkline/jquery.sparkline.min.js',
        'themes/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'themes/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'themes/adminlte/plugins/knob/jquery.knob.js',
        'themes/adminlte/plugins/daterangepicker/daterangepicker.js',
        'themes/adminlte/plugins/datepicker/bootstrap-datepicker.js',
        'themes/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'themes/adminlte/plugins/slimScroll/jquery.slimscroll.min.js',
        'themes/adminlte/plugins/fastclick/fastclick.min.js',
        'themes/adminlte/dist/js/app.min.js',
        //'themes/adminlte/dist/js/pages/dashboard.js', // error check after imporing theme layout
        'themes/adminlte/dist/js/demo.js',
        'js/jquery.form.js',

        /* EXTERNAL JS */
        'js/jquery-ui.min.js',
        'js/raphael-min.js',
        'js/moment.min.js',
        'themes/adminlte/plugins/waitme/waitMe.js',
        'themes/adminlte/plugins/nestable/jquery.nestable.js',
        'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js',
        /* ==EXTERNAL JS */
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
