<?php

namespace backend\controllers;

use Yii;
use backend\models\Banners;
use backend\models\search\BannersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;

/**
 * BannersController implements the CRUD actions for Banners model.
 */
class BannersController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banners model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->request->isAjax) {
                return $this->renderAjax('view', [
                'model' => $this->findModel($id),
                ]);
            }            
        else {
            return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Banners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banners();

        if ($model->load(Yii::$app->request->post())) {

            $fileName = NULL;

            if(isset($_FILES)) {

                $bannerImagefile = $_FILES['Banners']['name']['file'];
                $bannerImageext = pathinfo($bannerImagefile,PATHINFO_EXTENSION);
                if($bannerImageext == "jpg")
                $source_image = imagecreatefromjpeg($_FILES['Banners']['tmp_name']['file']);
                if($bannerImageext == "png")
                $source_image = imagecreatefrompng($_FILES['Banners']['tmp_name']['file']);
               

                $width = imagesx($source_image);
                $height = imagesy($source_image);

                $uid = uniqid(time(), true);
                $fileName = $uid.'_'. basename($_FILES['Banners']['name']['file']);

                $uploadBanner = \Yii::$app->basePath."/../store/banners/" . $fileName;

                /*$uploadfile = $this->uploadThumbnailPath . basename($_FILES['Products']['name']['image']);*/
                $banner_width = 1920;
                $banner_height = 1080;
                $virtual_image = imagecreatetruecolor($banner_width, $banner_height);
                
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $banner_width, $banner_height, $width, $height);
                $im = imagejpeg($virtual_image, $uploadBanner);

                }




            $model->file = $fileName;
            $model->created_at = $this->getNowTime();
            $model->updated_at = $this->getNowTime();

           
             
           if($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'New banner has been created');
                return $this->redirect(['index','id' => $model->id]); 
            }
        } else {

            if(Yii::$app->request->isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            
        }
    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

         if ($model->load(Yii::$app->request->post())) {

            $fileName = NULL;

            if(isset($_FILES)) {

                $bannerImagefile = $_FILES['Banners']['name']['file'];
                $bannerImageext = pathinfo($bannerImagefile,PATHINFO_EXTENSION);
                if($bannerImageext == "jpg")
                $source_image = imagecreatefromjpeg($_FILES['Banners']['tmp_name']['file']);
                if($bannerImageext == "png")
                $source_image = imagecreatefrompng($_FILES['Banners']['tmp_name']['file']);
               

                $width = imagesx($source_image);
                $height = imagesy($source_image);

                $uid = uniqid(time(), true);
                $fileName = $uid.'_'. basename($_FILES['Banners']['name']['file']);

                $uploadBanner = \Yii::$app->basePath."/../store/banners/" . $fileName;

                /*$uploadfile = $this->uploadThumbnailPath . basename($_FILES['Products']['name']['image']);*/
                $banner_width = 1920;
                $banner_height = 1080;
                $virtual_image = imagecreatetruecolor($banner_width, $banner_height);
                
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $banner_width, $banner_height, $width, $height);
                $im = imagejpeg($virtual_image, $uploadBanner);

                }




            $model->file = $fileName;
            $model->created_at = $this->getNowTime();
            $model->updated_at = $this->getNowTime();

           
             
           if($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Banner has been updated');
                return $this->redirect(['index','id' => $model->id]); 
            }
        } else {

            if(Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            
        }
    }

    /**
     * Deletes an existing Banners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
