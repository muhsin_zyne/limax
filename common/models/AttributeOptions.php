<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attribute_options".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property string $value
 * @property integer $position
 */
class AttributeOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attribute_options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id','position'], 'required'],
            [['attribute_id', 'position'], 'integer'],
            ['value', 'safe'],
            [['value'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attribute_id' => 'Attribute ID',
            'value' => 'Value',
            'position' => 'Position',
        ];
    }
}
