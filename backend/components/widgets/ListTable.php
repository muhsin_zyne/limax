<?php 

namespace backend\components\widgets;
use yii\base\Widget;
use yii\helpers\Html;

class ListTable extends Widget
{
    public $model;
    
    public function init()
    {   
        $empty = false;
        if (empty($this->model)) {
            $empty = true;
        }
        
        if($empty==false) {
            $result = $this->generateListTable($this->model);
            echo $result;
        }
        
        
    }

    public function run()
    {   
        
    }

    protected function generateListTable($model) {

        $resultStruture = $this->generateStructure($model);
        $html = '';
        $html.='<table id="w0" class="table table-striped table-bordered detail-view">';
        $html.='<thead> <tr> ';
        foreach ($resultStruture as  $value) {
            $html.= '<th style="font-weight: bold;"> '.$value.' </th>';
        }
        $html.=' </tr> </thead>';
        $html.='<tbody>';
        foreach ($model as $value) {
            $html.= $this->generateRow($value);

          }  
        $html.='</tbody></table>';

        return $html;

    }

    protected function generateRow($item) {
        $html='';
        $html.='<tr>';
        foreach ($item as $value) {
            $html.='<td>'.$value.' </td>';
        }
        $html.='</tr>';

        return $html;
    }

    protected function generateStructure($model) {
        $header = $model[0];
        $headerAtributes = [];
        foreach ($header as $key => $value) {
            $headerAtributes [] = ucfirst($key);
        }
        return $headerAtributes;
    }

    /*protected function getModal() {
        $html = '<div id="'.$this->id.'"class="fade modal" role="dialog" tabindex="-1">';
        $html.= '<div class="modal-dialog" id="ModalDialog">';
        $html.=' <div class="modal-content">';
        $html.=' <div class="modal-header">';
        $html.=' <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        $html.=' <h4 id="ModalHeaderID"> '.$this->header.'</h4>';
        $html.=' </div> ';
        $html.=' <div class="modal-body"> ';
        $html.= $this->body;
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        return $html;

    }
*/}


?>