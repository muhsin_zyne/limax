<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
use kartik\alert\Alert;

$this->title = 'Dealers Accounts';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="raw margin-top-min-75" >
        <div class="site-contact">          
            <div id="content-block">
                <div class="content-center fixed-header-margin">
                    <div class="content-push">
                        <div class="information-blocks">
                            <div class="row">
                                <div class="col-sm-12 information-entry" style="padding-top: 10px;">
                                    <div class="login-box">
                                        <div class="article-container style-1">
                                            <h3>Dealers Registration </h3>
                                        </div>
                                    
                                        <div class="row">
                                            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                                            
                                            <div class="col-xs-12 col-sm-6">
                                                <?= $form->field($model, 'display_name')->textInput(['maxlength'=>true])->label('Contact Person') ?>
                                                <?= $form->field($model,'shop_name')->textInput(['maxlength'=>true,]) ?>
                                                <?= $form->field($model,'address')->textArea(['rows'=>3]) ?>
                                                <?= $form->field($model, 'place')->textInput(['maxlength'=>true]) ?>
                                                <?= $form->field($model, 'district')->textInput(['maxlength'=>true]) ?>

                                               
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'maxlength'=>true]) ?>
                                                <?= $form->field($model, 'email') ?>
                                                <?= $form->field($model, 'mobile')->textInput(['maxlength'=>true]) ?>
                                                <?= $form->field($model, 'password')->passwordInput(['maxlength'=>true]) ?>
                                                <?= $form->field($model, 'c_password')->passwordInput(['maxlength'=>true]) ?>
                                                <?= $form->field($model, 'reCaptcha')->widget(
                                                    \himiklab\yii2\recaptcha\ReCaptcha::className(),
                                                    ['siteKey' => '6LeEqjQUAAAAAKWcY0LBQ7xfvDhBG-bP8KP1fPvo']
                                                ) ?>

                                                 <div class="form-group">
                                                    <?= Html::submitButton('Signup', ['class' => 'button style-12 pull-right', 'name' => 'signup-button']) ?>
                                                </div>

                                            </div>
                                            <?php ActiveForm::end(); ?>
                                        </div>

                                       

                                            

                                            

                                            

                                        

                                     </div>
                                </div>
                                
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="clear"></div>
            </div>  

        </div>
    </div>  
</div>

