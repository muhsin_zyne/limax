<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
         'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'id' => 'ajax'
        ]); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true,'readOnly'=>true]) ?>
            <?= $form->field($model, 'display_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->b2bAddressModel, 'mobile')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->b2bAddressModel, 'phone')->textInput(['maxlength' => true]) ?>
            
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model->b2bAddressModel, 'shop_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->b2bAddressModel, 'place')->textInput([]) ?>
            <?= $form->field($model->b2bAddressModel, 'district')->textInput([]) ?>
            <?= $form->field($model->b2bAddressModel, 'address')->textArea(['rows'=>2]) ?>
            <?= $form->field($model, 'is_approved')->radioList(['pending'=>'Pending','success'=>'Activate','disable'=>'Disable','terminated'=>'Terminate'],[])->label('account status') ?>
            
            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary theme-btn pull-right']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

   

    

    

   

    

</div>
