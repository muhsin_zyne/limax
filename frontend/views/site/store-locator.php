<?php


use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
?>

<div class="container" style="background-image: url()>
    <div class="raw margin-top-min-75">
        <div class="site-contact">          
            <div id="content-block">
              <!--   <div class="content-center fixed-header-margin"> -->
                    <img class="project-thumbnail" src="/themes/shopping/img/banner.jpg" alt="" />
                    <div class="content-push">
                        <div class="information-blocks">
                            <div class="row">                               
                                <div class="col-sm-12 information-entry">
                                    <div class="login-box" style="margin-top: 0px !important;margin-bottom: -23px !important;">
                                        <?= ListView::widget([
                                        'dataProvider' => $dataProvider,
                                        'layout' => "\n{items}\n <div class='clearfix'> </div> <div class='pull-right'>{pager} </div> <div class='clearfix'></div>",
                                        'pager' => [
                                            'firstPageLabel' => 'First',
                                            'lastPageLabel' => 'Last',
                                        ],
                                        'itemOptions' => ['class' => ''],
                                        'itemView'=>'_store-view',
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
               <!--  </div> -->
                <div class="clear"></div>
            </div>  

        </div>
    </div>  
</div>
<style type="text/css">

#content-block *:last-child {
    margin-bottom: 8px !important;
}

.article-container h5, .h5 {   
    color: #b51717 !important;   
}

.article-container {   
    color: #000000 !important;    
}

.project-thumbnail { 
    margin-bottom: 10px !important; 
}
</style>