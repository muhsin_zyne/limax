<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "brands".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $url_to
 * @property string $status
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 */
class Brands extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status', 'created_at', 'updated_at'], 'required'],
            [['description', 'status', 'image'], 'string'],
            [['created_at', 'updated_at','description', 'status', 'image','url_to'], 'safe'],
            [['name', 'url_to'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Brand Name',
            'description' => 'Description',
            'url_to' => 'Url To',
            'status' => 'Status',
            'image' => 'Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
