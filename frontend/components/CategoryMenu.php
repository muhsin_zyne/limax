<?php

namespace frontend\components;
use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Menus;

class CategoryMenu extends Widget{
    
    public $menu_type;
    
    public function init(){

        if ($this->menu_type === null) {
            $this->menu_type = '';
        }

        parent::init();
        
    }
    
    public function run(){
       
       $result = $this->getMenus($this->menu_type);
       echo $result; 
    }

    public function getMenus($type) {
    	$menu = Menus::find()->where(['un_assigned'=>'1','type'=>$type]) ->orderBy('position')->all();
    	$menu_array = $this->getMenuList($menu);
    	$generated_menu = $this->generateMenu($menu_array);
    	
    	return $generated_menu;
    }

    public function getMenuList($items) {
    	$elements = [];

    	foreach ($items as $item) {
    		if($item['parent']==0) {
    			if(preg_match('/http/', $item['path'])) {
    				$elements [] = [
	    				'id' => $item['id'],
	    				'label'=>$item['title'],
	    				'url' =>$item['path'],
	    				'target' =>$item['target'],
	    				'icon' =>'fa fa-'.$item['icon'],
	    				'items' => $this->getItems($items,$item['id']),
    				];

    			}
    			else {

    				$elements [] = [
	    				'id' => $item['id'],
	    				'label'=>$item['title'],
	    				'url' =>yii::$app->params['baseUrl'].'/'.$item['path'],
	    				'target' =>$item['target'],
	    				'icon' =>'fa fa-'.$item['icon'],
	    				'items' => $this->getItems($items,$item['id']),
    				];

    			}
    			
    		}
    	}

    	return $elements;

    }

    public function getItems($new_items,$parent) {
    	$new_elements = [];
    	foreach ($new_items as $new_item) {
    		if($new_item['id'] != $parent && $new_item['parent']==$parent) {
    			if(preg_match('/http/', $new_item['path'])) {
    				
    				$new_elements [] = [
	    				'id' => $new_item['id'],
	    				'label' => $new_item['title'],
	    				'url' => $new_item['path'],
	    				'target' => $new_item['target'],
	    				'icon' =>'fa fa-'.$new_item['icon'],
	    				'items' => $this->getItems($new_items,$new_item['id']),
	    			];

    			}
    			else {

    				$new_elements [] = [
	    				'id' => $new_item['id'],
	    				'label' => $new_item['title'],
	    				'url' => yii::$app->params['baseUrl'].'/'.$new_item['path'],
	    				'target' => $new_item['target'],
	    				'icon' =>'fa fa-'.$new_item['icon'],
	    				'items' => $this->getItems($new_items,$new_item['id']),
	    			];

    			}
    			
    		}
    	}

    	return $new_elements;

    }

    public function generateMenu($items,$child=FALSE) {
    	$html='<div class="full-width-menu-items-full">';
    	$html.='<div class="row">';

    	foreach ($items as $item) {
    		$html.='<div class="col-lg-2">';
    		$html.='<div class="submenu-list-title"> '.Html::a('<i class="'.$item['icon'].'"></i> '.$item['label'],$item['url'],['class'=>'','target'=>$item['target']]).' <span class="toggle-list-button"></span></div>';
    		if(count($item['items']>0)) {
    			$html.=$this->generateMenuSub($item['items'],TRUE);
    			
    		}
    
    		$html.='</div>';


    	}
    	$html.='</div>';
    	$html.='</div>';

    	return $html;
    }

    public function generateMenuSub($items,$child=FALSE) {
		$html='<ul class="list-type-1 toggle-list-container">';
		foreach ($items as $item) {
			$html.='<li>'.Html::a('<i class="fa fa-angle-right"></i>'.$item['label'],$item['url'],['class'=>'','target'=>$item['target']]).'</li>';
		}
		$html.='</ul>';

		return $html;
    }

    

}


?>