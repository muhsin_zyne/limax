<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\CmsPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

    
   <?= $form->field($model, 'content')->widget(CKEditor::className(), 
                    [
                        'id'=>'cmscontent',
                        'preset' => 'full', 
                        'clientOptions' =>[
                            'language' => 'en', 
                            'allowedContent' => true,
                            'filebrowserUploadUrl' =>Yii::$app->urlManager->createUrl(['cms-blocks/image-upload']),
                            'filebrowserBrowseUrl'=>Yii::$app->urlManager->createUrl(['cms-blocks/image-browse']),
                            ]
                    ]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 1 => '1', 0 => '0', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
