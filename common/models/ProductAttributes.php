<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_attributes".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $brand_id
 * @property string $product_type
 * @property string $product_category
 */
class ProductAttributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'brand_id', 'product_type', 'product_category'], 'required'],
            [['product_id', 'brand_id'], 'integer'],
            [['product_type', 'product_category'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'brand_id' => 'Brand ID',
            'product_type' => 'Product Type',
            'product_category' => 'Product Category',
        ];
    }
}
