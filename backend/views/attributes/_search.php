<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AttributesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attributes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'required') ?>

    <?= $form->field($model, 'apply_to_simple') ?>

    <?= $form->field($model, 'apply_to_grouped') ?>

    <?php // echo $form->field($model, 'apply_to_configurable') ?>

    <?php // echo $form->field($model, 'apply_to_virtual') ?>

    <?php // echo $form->field($model, 'apply_to_bundle') ?>

    <?php // echo $form->field($model, 'apply_to_downloadable') ?>

    <?php // echo $form->field($model, 'system') ?>

    <?php // echo $form->field($model, 'code') ?>

    <?php // echo $form->field($model, 'field') ?>

    <?php // echo $form->field($model, 'validation') ?>

    <?php // echo $form->field($model, 'configurable') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
