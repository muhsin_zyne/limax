<?php

namespace frontend\components;
use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Menus;

class MainMenu extends Widget{
    public $menu_type;
    
    public function init(){

        if ($this->menu_type === null) {
            $this->menu_type = '';
        }

        parent::init();
        
    }
    
    public function run(){
        $result = $this->getMenus($this->menu_type);
        echo $result;
        

    }

    protected function getMenus($type) {
        $menu_array = Menus::find()->where(['un_assigned'=>'1','type'=>$type]) ->orderBy('position')->all();
        $menus_array = $this->getMenuList($menu_array);
        $data = $this->getMenuRecrusive($menus_array,FALSE);
        return $data;
    }

    public function getMenuRecrusive($data, $child=FALSE) {
            $html = '';

            foreach ($data as $item) {
                    
                $html.='<li class="simple-list">';
                $html.= Html::a($item['label'],$item['url'],['class'=>'','target'=>$item['target']]);
                if(count($item['items'])>0) {
                    $html.='<i class="fa fa-chevron-down"></i>';
                    $html.='<div class="submenu">';
                    $html.='<ul class="simple-menu-list-column">';
                    $html.=$this->getMenuRecrusiveSub($item['items'],TRUE);
                    $html.='</ul>
                        </div>';
                }
                
                $html.='</li>';

                                        
            }
           
           return $html;
    }

    public function getMenuRecrusiveSub($data, $child=FALSE) {
            $html = '';

            foreach ($data as $item) {

                
                    
                $html.='<li>'. Html::a('<i class="fa fa-angle-right"></i>'.$item['label'],$item['url'],['class'=>'','target'=>$item['target']]).'</li>';


                                        
            }
           
           return $html;
    }

    protected function getItems($items_new,$parent) {
        $newelements = [];
        foreach ($items_new as $newitem) {
            if($newitem['id'] != $parent && $newitem['parent']==$parent) {
                if(preg_match('/http/', $newitem['path'])) {
                    
                    $newelements [] = [
                        'id' => $newitem['id'],
                        'label' => $newitem['title'],
                        'url' => $newitem['path'],
                        'target' => $newitem['target'],
                        'icon' => $newitem['icon'],
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ];
                   
                }
                else {

                    $newelements [] = [
                        'id' => $newitem['id'],
                        'label' => $newitem['title'],
                        'url' => Yii::$app->params['baseUrl'].'/'.$newitem['path'],
                        'target' => $newitem['target'],
                        'icon' => $newitem['icon'],
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ];

                }

                
            }
        }

        return $newelements;

    }

    protected function getMenuList($items) {
        $elements = [];
        
        foreach ($items as $item) {
            if($item['parent']==0) {

                if(preg_match('/http/', $item['path'])) {
                    $elements [] = [
                        'id' => $item['id'],
                        'label'=>$item['title'],
                        'url' => $item['path'],
                        'target' => $item['target'],
                        'icon' => $item['icon'],
                        'items' => $this->getItems($items, $item['id']),

                    ];

                }
                else {
                    $elements [] = [
                        'id' => $item['id'],
                        'label'=>$item['title'],
                        'url' =>Yii::$app->params['baseUrl'].'/'.$item['path'],
                        'target' => $item['target'],
                        'icon' => $item['icon'],
                        'items' => $this->getItems($items, $item['id']),

                    ];

                }
                
            }
        }

        return $elements;

    }

    

}


?>