<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_description".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $short_description
 * @property string $description
 * @property string $detail_attributes
 */
class ProductDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'short_description', 'description', 'detail_attributes'], 'required'],
            [['id', 'product_id'], 'integer'],
            [['short_description', 'description', 'detail_attributes'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'detail_attributes' => 'Detail Attributes',
        ];
    }
}
