<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Attributes;
use common\models\AttributeOptions;

/* @var $this yii\web\View */
/* @var $model common\models\Attributes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attributes-form">
<?php $form = ActiveForm::begin(); ?>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Product Attribute</a></li>
            <li id="m_title" class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Manage Titles (Size, Color, etc.)</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'code')->textInput() ?>
                        <?= $form->field($model, 'field')->dropDownList(['text' => 'Text Field', 'textarea' => 'Text Area','date' => 'Date', 'boolean' => 'Boolean','multiselect' => 'Multiple Select','dropdown' => 'Dropdown', 'price' => 'Price']); ?>
                        <?php
                            if ($this->context->action->id == 'update') {
                                $att= Attributes::find($model->id)->where(['id' => $model->id])->one();
                                if($att['apply_to_simple']=='0' || $att['apply_to_grouped']=='0' || $att['apply_to_configurable']=='0' || $att['apply_to_virtual']=='0' || $att['apply_to_bundle']=='0' || $att['apply_to_downloadable']=='0')
                                    {
                                        $model->attributeGroupId='2';
                                    }
                                }
                        ?>
                        <?= $form->field($model, 'attributeGroupId')->dropDownList(['1' => 'All Product Types', '2' => 'Selected Product Types']); ?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div id="spt">
                            <?php
                            $attr_selected[]=0;
                            if ($this->context->action->id == 'update') {
                                $att= Attributes::find($model->id)->where(['id' => $model->id])->one();                        
                                //print_r($att);
                                if($att['apply_to_simple']=='1') { $attr_selected[]='Simple';}
                                if($att['apply_to_grouped']=='1') { $attr_selected[]='Grouped';}
                                if($att['apply_to_configurable']=='1') { $attr_selected[]='Configurable';}
                                if($att['apply_to_virtual']=='1') { $attr_selected[]='Virtual';}
                                if($att['apply_to_bundle']=='1') { $attr_selected[]='Bundle';}
                                if($att['apply_to_downloadable']=='1') { $attr_selected[]='Downloadable';}
                                
                                $data = array('Simple' => 'Simple','Grouped' => 'Grouped','Configurable' => 'Configurable','Virtual' => 'Virtual','Bundle' => 'Bundle','Downloadable' => 'Downloadable',); 
                                $htmlOptions = array('multiple' => 'true');
                                echo $form->field($model,'productTypeApplicableUpdate')->listBox($data, $htmlOptions);
                            }?> 

                            <?php 
                            if ($this->context->action->id == 'create') {
                                $data = array('Simple' => 'Simple','Grouped' => 'Grouped','Configurable' => 'Configurable','Virtual' => 'Virtual','Bundle' => 'Bundle','Downloadable' => 'Downloadable',); 
                                $htmlOptions = array('multiple' => 'true');
                                echo $form->field($model,'productTypeApplicable')->listBox($data, $htmlOptions);
                            }
                            ?>                  
                        </div>

                         
                        <?= $form->field($model, 'required')->dropDownList(['0' => 'No', '1' => 'Yes']); ?>
                        <?= $form->field($model, 'validation')->dropDownList([ 'decimal' => 'Decimal Number','integer' => 'Integer Number','email' => 'Email','url' => 'URL','letters' => 'Letters(a-z)(A-Z) or Numbers(0-9)'],['prompt'=>'Select']); ?>
                        <div id="drop_conf">
                            <?= $form->field($model, 'configurable')->dropDownList(['0' => 'No', '1' => 'yes']); ?>
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']) ?>
                        </div> 
                    </div>
                </div>
        
            </div><!-- /.tab-pane -->
            <div class="tab-pane " id="tab_2">

            <div class="btn-group pull-right">
                <a class="btn btn-primary theme-btn" onclick="addRow('dataTable')"> <i class="fa fa-plus"> </i> Add </a>
                <a class="btn btn-danger theme-btn" id="remove_last_row"> <i class="fa fa-minus"> </i> Remove </a>
            </div>
            
                            <div class="slimScroll">
                                <?php $model1= new AttributeOptions();?>
                                <?php if ($this->context->action->id == 'update') { ?>
                                    <?php
                                    $att_options = AttributeOptions::find()->where(['attribute_id' => $model->id])->all();
                                    foreach ($att_options as $index => $att_options_id) {?>
                                        <table id="dataTable1" width="50%">
                                            <tr id="<?=$att_options_id['id']?>"><td width="90%">                    
                                            <?= $form->field($model1, 'value['.$att_options_id['id'].']')->textInput(['class'=>'disabled form-control','readonly' => !$model->isNewRecord,'value'=>$att_options_id['value']])->label(false) ?></td>
                                            <td width="10%"><span class="man-title cursor-link" id="<?=$att_options_id['id']?>"><i class="fa fa-close"></i></span>                            
                                            </td></tr>
                                        </table>
                                    <?php }
                                } ?>
                                <table id="dataTable" width="50%">
                                    <tr>
                                        <td width="90%"><?= $form->field($model1, 'value[]')->textInput(['maxlength' => 45])->label(false) ?></td>
                                        <td width="10%"></td>
                                    </tr>
                                </table>
                            </div> 

                                <div class="form-group">
                                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']) ?>
                                </div>

                                <div class="clearfix"> </div>
                                
                            
       
    
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_3">
            
        
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div>

    




    


   


<?php ActiveForm::end(); ?>
</div>


<script type="text/javascript">

    function addRow(tableID){
        var table=document.getElementById(tableID);
        var rowCount=table.rows.length;
        var row=table.insertRow(rowCount);
        var colCount=table.rows[0].cells.length;
        for(var i=0;i<colCount;i++){
            var newcell=row.insertCell(i);
            newcell.innerHTML=table.rows[0].cells[i].innerHTML;
            switch(newcell.childNodes[0].type){
                case"text":newcell.childNodes[0].value="";break;
                case"checkbox":newcell.childNodes[0].checked=false;break;
                case"select-one":newcell.childNodes[0].selectedIndex=0;break;
            }
        }
    }


    $(document).ready(function(){       
        $("#remove_last_row").click(function() {
            var count = $('#dataTable tr').length;
            if(count>1) {
                $('.attributes-form #dataTable tr:last').remove();
            }
            else {
                swal(
                  'Sorry!',
                  'You have removed maximum!',
                  'error'
                );
            }
            
        });


        $("#attributes-producttypeapplicableupdate option").each(function(){            
            <?php foreach ($attr_selected as $index => $attr_sel)  {?>
                if ($(this).text() == "<?=$attr_sel?>")
                $(this).attr("selected","selected");
            <?php } ?> 
        });
        if ($('#attributes-field').val() == 'dropdown' || $('#attributes-field').val() == 'multiselect'){
            $("#m_title").show();    
            $("#drop_conf").show();
            $("#manage_title").show();                    
        }
        else
            {

            $("#m_title").hide();   
            $("#drop_conf").hide();
            $("#manage_title").hide();
        }

        if ($('#attributes-attributegroupid').val() == '2'){
            $("#spt").show();
        }
        else {
            $("#spt").hide();
        }
    
        $('#attributes-attributegroupid').on('change', function() {
            if ( this.value == '2'){
                $("#spt").show();
            }
            else{
                $("#spt").hide();
            }
        });    
        $('#attributes-field').on('change', function() { 
            if ( this.value == 'dropdown' || this.value == 'multiselect'){
                $("#m_title").show();    
                $("#drop_conf").show();
                $("#manage_title").show();
            }
            else {
                $("#m_title").hide();    
                $("#drop_conf").hide();
                $("#manage_title").hide();
            }
        }); 
    });


    $(document).ready(function(){
        $(".man-title").click(function(){




            if(confirm("Are you sure you want to delete this item?..")){
                $.ajax({
                    type: "POST",
                    url: "<?=Yii::$app->urlManager->createUrl(['attributes/delete-attribute-option'])?>",             
                    data: "&id=" + this.id ,
                    dataType: "html", 
                    success: function (data) {                         
                        $("#"+data).hide(); 
                        $("#attributeoptions-value-"+data).val('');

                    }                 
                }); 
            }
            else{
                return false;
            }            
        });
    });



</script>



<script> 
$( document).ready(function() {

  var screenHeight = $(window).height();
  $ ( '.slimScroll' ).slimScroll({
        height: screenHeight-300

    });
    
}); 
</script>