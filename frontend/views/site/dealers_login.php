<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Dealers Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="raw margin-top-min-75">
        <div class="site-contact">          
           
              <div id="content-block">

        <div class="content-center fixed-header-margin">

            <div class="content-push">
<!-- 
                <div class="breadcrumb-box">
                    <a href="/site/index">Home</a>
                    <a href="#">Login Form</a>
                </div> -->

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>Registered Dealers</h3>
                                </div>
                                <form action="/site/dealers_home">
                                    <label>Limax Username</label>
                                    <input class="simple-field" type="text" placeholder="Username" value="" />
                                    <label>Limax Password</label>
                                    <input class="simple-field" type="password" placeholder="Password" value="" />
                                    <div class="button style-10">Login Here<input type="submit" value="" /></div>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>New Dealers</h3>
                                    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                                </div>
                                <a class="button style-12">Register Here</a>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <div class="clear"></div>

    </div>  

        </div>
    </div>  
</div>

