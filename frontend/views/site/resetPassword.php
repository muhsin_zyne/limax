<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="container">   
    <div class="row">
        <div class="site-request-password-reset" style="padding: 15px; padding-bottom: 20px">
            <div clsass="col-xs-12">
                <div class="article-container style-1">
                    <h3> <?= Html::encode($this->title) ?> </h3>
                    <p>Please choose your new password:</p>  
                </div>
                <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'c_password')->passwordInput([]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>

            </div>               
        </div>
    </div>
</div>


