<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\money\MaskMoney;
use yii\helpers\Url;
use kartik\file\FileInput;
use yii\web\UploadedFile;
use kartik\widgets\DatePicker;

?>

<?php $form = ActiveForm::begin([
  'options'=>['encytype'=>'multipart/form-data']
]); ?>
    <div class="nav-tabs-custom form-new products-form">
        <ul class="nav nav-tabs">
            <li class="active" id="tab-h-1"><a href="#tab_1" data-toggle="tab" aria-expanded="false">General </a></li>
            <li class="" id="tab-h-2"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Prices</a></li>
            <li class="" id="tab-h-3"><a href="#tab_3" data-toggle="tab" aria-expanded="false">Meta Information</a></li>
            <li class="" id="tab-h-4"><a href="#tab_4" data-toggle="tab" aria-expanded="false">Description</a></li>
            <li class="" id="tab-h-5"><a href="#tab_5" data-toggle="tab" aria-expanded="false">Images</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                   <?= $form->field($model, 'status')->dropDownList([ '0'=>'Enable', '1'=>'Disable', ], ['prompt' => 'Status']) ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                   <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>
                   <?= $form->field($model, 'case_qty')->textInput(['maxlength' => true]) ?>
                
                    <div class="clearfix"> </div>
                      
                </div>
            </div>

            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_2'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 1']);?>
        
                </div>
              </div>
            </div>

          </div><!-- /.tab-pane -->
          <div class="tab-pane " id="tab_2">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <?= $form->field($model, 'price')->widget(MaskMoney::classname(), [ ]); ?>
                  <?= $form->field($model, 'dealers_price')->widget(MaskMoney::classname(), [ ]); ?>
                  <?= $form->field($model, 'cost_price')->widget(MaskMoney::classname(), [ ]); ?> 
                  
                </div>
                <div class="col-xs-12 col-sm-6">    
                   <?= $form->field($model, 'special_price')->widget(MaskMoney::classname(), [ ]); ?>
                   <?= $form->field($model, 'from_date')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Special price valid from ...'],
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                         'format' => 'yyyy/mm/dd',
                                                          'startDate' => date('Y-m-d')
                                                    ]
                                                  ]); ?>
                   <?= $form->field($model, 'to_date')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Special price valid to ...'],
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                        'format' => 'yyyy/mm/dd',
                                                        'startDate' => date('Y-m-d')
                                                    ]
                                                  ]); ?>
                       
                       
                </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_1'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 2']); ?>
                  <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_3'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 2']);?>
        
                </div>
              </div>
            </div>
          </div><!-- /.tab-pane -->

          <div class="tab-pane " id="tab_3">
            <div class="row">
                <div class="col-xs-12">
                   <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>                
                   <?= $form->field($model, 'meta_description')->textArea(['maxlength' => true,'rows'=>4]) ?>
                   <?= $form->field($model, 'meta_keyword')->textArea(['maxlength' => true,'rows'=>3]) ?>           
                </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_2'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 3']);?>
                  <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_4'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 3']);?>
                 
                </div>
              </div>
            </div>
          </div><!-- /.tab-pane -->

          <div class="tab-pane " id="tab_4">
            <div class="row">
                <div class="col-xs-12">               
                   <?= $form->field($model, 'description')->textArea(['maxlength' => true,'rows'=>2]) ?>
                   <?= $form->field($model, 'short_description')->textArea(['maxlength' => true,'rows'=>4]) ?>  
                </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_3'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 4']);?>
                  <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_5'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 4']);?>
                 
                </div>
              </div>
            </div>
          </div><!-- /.tab-pane -->


          <div class="tab-pane " id="tab_5">
            <div class="row">
                <div class="col-xs-12">


                  <?=Html::activeInput('file', $model, 'image',$options = ['class'=>'file','id'=>'file-1','multiple'=>'true','data-overwrite-initial'=>'false','data-min-file-count'=>'1']) ?>

<script type="text/javascript">
  function randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
      var randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}
</script>

<script>

  var randomValue = randomString(10);
  var url = "<?=\yii\helpers\Url::to(['/product-images/upload'])?>";
  var key = 0;

  $("#file-1").fileinput({ 
    uploadUrl: url+'?rand='+randomValue, 
      allowedFileExtensions : ['jpg', 'png','gif'],
        overwriteInitial: false,
        uploadAsync: true,
        maxFileSize: 20000,
        maxFilesNum: 3,
        'showPreview' : true,
        allowedFileTypes: ['image'],
        slugCallback: function(filename) { 
          filename = randomValue+'_'+filename;
          

          <?php if(!$model->isNewRecord) {  ?>
            len = $('.file-preview-frame').length;
            if(len != 1 && len != 0){
              key = $('.base_image:last').val();
              $('.file-preview-frame').attr('data-fileindex',(parseInt(key)+1));
            }
            else{
              $('.file-preview-frame').attr('data-fileindex',len);
            } 
        <?php } ?>
            return filename.replace(')', ')').replace(']', '_');
        }
  });


  $('#tr').click(function(){
    $(this).remove();
    return false;
});

</script> 



             
                </div>
               
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="form-group pull-right">
                  <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_4'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 5']);?>
                
                  <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary ']) ?>
                </div>
              </div>
            </div>
          </div><!-- /.tab-pane -->


          
        </div><!-- /.tab-content -->
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
$( document ).ready(function() {
  
   

 

  


   $(".next-pane.1").click(function() {
       $("#tab-h-1").removeClass("active");
       $("#tab-h-2").addClass("active"); 
   });
   $(".next-pane.2").click(function() {
       $("#tab-h-2").removeClass("active");
       $("#tab-h-3").addClass("active"); 
   });


   
   $(".previous-pane.2").click(function() {
       $("#tab-h-2").removeClass("active");
       $("#tab-h-1").addClass("active"); 
   });
   $(".previous-pane.3").click(function() {
       $("#tab-h-3").removeClass("active");
       $("#tab-h-2").addClass("active"); 
   });
   $(".previous-pane.4").click(function() {
       $("#tab-h-4").removeClass("active");
       $("#tab-h-3").addClass("active"); 
   });
   $(".previous-pane.5").click(function() {
       $("#tab-h-5").removeClass("active");
       $("#tab-h-4").addClass("active"); 
   });


   function run_waitMe(effect){
    $('#loader-block').waitMe({

    //none, rotateplane, stretch, orbit, roundBounce, win8, 
    //win8_linear, ios, facebook, rotation, timer, pulse, 
    //progressBar, bouncePulse or img
    effect: 'roundBounce',

    //place text under the effect (string).
    text: 'Offer Details...',

    //background for container (string).
    bg: 'rgba(255,255,255,0.7)',

    //color for background animation and text (string).
    color: '#00aff0',

    //change width for elem animation (string).
    sizeW: '',

    //change height for elem animation (string).
    sizeH: '',

    // url to image
    source: '',

    // callback
    onClose: function() {}

    });
  }
  
});
</script>

<style type="text/css">
  table.image_table {
    width: 100%;
  }
  div.kv-fileinput-error.file-error-message {
    display: none !important;
  }
</style>