<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\Categories;
use common\models\Brands;
use common\models\ProductType;
use yii\db\query;

/**
 * ProductSearch represents the model behind the search form about `common\models\Products`.
 */
class ProductSearch extends Products
{
    /**
     * @inheritdoc
     */
    public $categories;
    public $product_type;
    public $brands;
    public $page;

    public function rules()
    {
        return [
            [['id', 'created_by', 'case_qty'], 'integer'],
            [['sku', 'created_at', 'status' ,'title','categories','product_type','brands','page'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);


        if(empty($this->categories)) {
            $this->categories = [];
        }
        if(empty($this->product_type)) {
            $this->product_type = [];
        }
        if(empty($this->brands)) {
            $this->brands = [];
        }

        $query = Products::find()->where(['status'=>'1']);
        $query->joinWith('productAttributes');

        if(!empty($this->brands)) {
            $query->andWhere('brand_id in ('.implode(", ", $this->brands).')');
        }
        if(!empty($this->categories)) {
            $query->andWhere('product_category in ('.implode(", ", $this->categories).')');
        }

        if(!empty($this->product_type)) {
            $query->andWhere('product_type in ('.implode(", ", $this->product_type).')');
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> [
                'pageSize' => 50,
            ],
        ]);

        
        if (!$this->validate()) {

            return $dataProvider;
        }
        
        return $dataProvider;
    }




    public function getCategoriesArray() {
        $categoriesArray = Categories::find()->where(['status'=>'1','parent'=>'0'])->all();
        return $categoriesArray;
    }

    public function getProductTypeArray() {
        $productTypeArray = ProductType::find()->where(['status'=>'1'])->all();
        return $productTypeArray;
    }

    public function getBrandsArray() {
        $brandsArray = Brands::find()->where(['status'=>'1'])->all();
        return $brandsArray;
    }



}
