<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
$this->title = 'Dealers Accounts';
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
section.page-view {
   background: rgba(194, 163, 37, 0.48);
}

#content-block *:last-child {
    margin-bottom: 8px !important;
}

.article-container h5, .h5 {   
    color: #b51717 !important;   
}

.article-container {   
    color: #000000 !important;    
}

.project-thumbnail { 
    margin-bottom: 10px !important; 
}
</style>

<div class="container" style="background-image: url()>
    <div class="raw margin-top-min-75">
        <div class="site-contact">          
            <div id="content-block">
              <!--   <div class="content-center fixed-header-margin"> -->
                    <img class="project-thumbnail" src="/themes/shopping/img/banner.jpg" alt="" />
                    <div class="content-push">
                        <div class="information-blocks">
                            <div class="row">
                               
                                <div class="col-sm-6 information-entry">
                                    <div class="login-box" style="margin-top: 0px !important;margin-bottom: -23px !important;">
                                        <div class="article-container style-1">
                                            <h3>LIMAX STORE ONE</h3>
                                            <h4>PERINTHALMANNA</h4> 
                                            <h5>Company address</h5>
                                            <p>8808 Ave Dermentum, Onsectetur Adipiscing<br>
                                            Tortor Sagittis, CA 880986,<br>
                                            United States<br>
                                            CA 90896<br> 
                                            United States<br></p>
                                            <h5>Contact Informations</h5>
                                            <p><span><i class="fa fa-envelope"></i>&nbsp info@limaxgroup.co.in</span><br>
                                            <span><i class="fa fa-phone-square"></i>&nbsp +91 9048 569 874</span></p>
                                        </div>
                                    </div>
                                </div>

                                 
                                <div class="col-sm-6 information-entry">
                                    <div class="login-box" style="margin-top: 0px !important;margin-bottom: -23px !important;">
                                        <div class="article-container style-1">
                                            <h3>LIMAX STORE TWO</h3>
                                            <h4>THIRUVANANTHAPURAM</h4> 
                                            <h5>Company address</h5>
                                            <p>8808 Ave Dermentum, Onsectetur Adipiscing<br>
                                            Tortor Sagittis, CA 880986,<br>
                                            United States<br>
                                            CA 90896<br> 
                                            United States<br></p>
                                            <h5>Contact Informations</h5>
                                            <p><span><i class="fa fa-envelope"></i>&nbsp info@limaxgroup.co.in</span><br>
                                            <span><i class="fa fa-phone-square"></i>&nbsp +91 9048 569 874</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
               <!--  </div> -->
                <div class="clear"></div>
            </div>  

        </div>
    </div>  
</div>

