<?php 

namespace backend\components\widgets;
use yii\base\Widget;
use yii\helpers\Html;

class CustomModal extends Widget
{
    public $header;
    public $id;
    public $size;
    public $body;
    
    public function init()
    {   

        if ($this->header === null) {
            $this->header = 'Sample Modal';
        }
        if ($this->id === null) {
            $this->id = 'modal';
        }
        if ($this->size === null) {
            $this->size = 'modal-xs';
        }
        if ($this->body === null) {
            $this->body = '<div id="modalContent"> </div>';
        }
        
        $result = $this->getModal();
        echo $result;
        
    }

    public function run()
    {   
        
    }

    protected function getModal() {
        $html = '<div id="'.$this->id.'"class="fade modal" role="dialog" tabindex="-1">';
        $html.= '<div class="modal-dialog" id="ModalDialog">';
        $html.=' <div class="modal-content">';
        $html.=' <div class="modal-header">';
        $html.=' <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        $html.=' <h4 id="ModalHeaderID"> '.$this->header.'</h4>';
        $html.=' </div> ';
        $html.=' <div class="modal-body"> ';
        $html.= $this->body;
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        return $html;

    }
}


?>