<?php

namespace backend\controllers;

use Yii;
use common\models\Categories;
use common\models\search\CategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $all_categories = Categories::find()->where(['status'=>'1',])->orderBy('position')->all();
        return $this->render('index', [
            'all_categories' => $all_categories,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewIcon($data)
    {   

        $font = new \backend\models\FontAwesomeIcon;
        
        $allIcons = $font->getIcons();
        $filter = preg_quote($data, '~');
        $fontAwesomeIcons = preg_grep('~' . $filter . '~', $allIcons);
        //echo'<script> alert('.count($fontAwesomeIcons).') </script>';

        return $this->renderAjax('_icon',[
               'fontAwesomeIcons' =>$fontAwesomeIcons,
               'searchData' => $data,
          ]);
        
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Categories();

        

        if ($model->load(Yii::$app->request->post())) {
        
            if($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Category has been created');
                return $this->redirect(['index','id' => $model->id]); 
            }
            
        } else {

            if(Yii::$app->request->isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            
        }
    }

    public function actionCreateChild($child)
    {
        $model = new Categories();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->renderAjax('create-child', [
                'model' => $model,
                'child' => $child,
            ]);
        }
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
