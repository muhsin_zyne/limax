<?php

namespace backend\controllers;

use Yii;
use common\models\Attributes;
use common\models\search\AttributesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;
use common\models\AttributeOptions;
/**
 * AttributesController implements the CRUD actions for Attributes model.
 */
class AttributesController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Attributes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttributesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Attributes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Attributes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Attributes();
        $input_type_dropdown=0;

        if(isset($_POST['Attributes'])){
            if($_POST['Attributes']['attributeGroupId']=='1'){
                $model->apply_to_simple = 1;
                $model->apply_to_bundle = 1;
                $model->apply_to_grouped = 1;
                $model->apply_to_configurable = 1;
                $model->apply_to_virtual = 1;
                $model->apply_to_downloadable = 1;
            }
            else {
                $model->apply_to_simple = 0;
                $model->apply_to_bundle = 0;
                $model->apply_to_grouped = 0;
                $model->apply_to_configurable = 0;
                $model->apply_to_virtual = 0;
                $model->apply_to_downloadable = 0;
                $sel_pro_type=$_POST['Attributes']['productTypeApplicable'];
                if($sel_pro_type!=''){
                    $sel_pro_type_count=count($sel_pro_type)-1;
                    for($i=0;$i<=$sel_pro_type_count;$i++){                    
                        if($sel_pro_type[$i]=='Simple'){$model->apply_to_simple = 1;}
                        if($sel_pro_type[$i]=='Bundle'){$model->apply_to_bundle = 1;} 
                        if($sel_pro_type[$i]=='Grouped'){$model->apply_to_grouped = 1;}
                        if($sel_pro_type[$i]=='Configurable'){ $model->apply_to_configurable = 1;}
                        if($sel_pro_type[$i]=='Virtual'){$model->apply_to_virtual = 1;}
                        if($sel_pro_type[$i]=='Downloadable'){$model->apply_to_downloadable = 1;}             
                    }
                }   
            }
            $model->required=$_POST['Attributes']['required'];
            $model->configurable=$_POST['Attributes']['configurable'];
            $model->validation=$_POST['Attributes']['validation'];            
            if($_POST['Attributes']['field']=='dropdown' || $_POST['Attributes']['field']=='multiselect'){
                $input_type_dropdown=1;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            if($input_type_dropdown==1 && isset($_POST['AttributeOptions']['value'])){
                foreach ($_POST['AttributeOptions']['value'] as $key => $attr_value) {
                    //echo $_POST['AttributeOptions']['value'][$i-1];
                    $model1= new AttributeOptions();
                    $model1->value=$attr_value;
                    $model1->attribute_id=$model->id;
                    $model1->save(false);
                }
                // exit;
            }
            return $this->redirect(['index']);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Attributes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {   

        $model = $this->findModel($id);
        
        $input_type_dropdown=0;  
        if(isset($_POST['Attributes'])){         
            if($_POST['Attributes']['attributeGroupId']=='1'){
                $model->apply_to_simple = 1;
                $model->apply_to_bundle = 1;
                $model->apply_to_grouped = 1;
                $model->apply_to_configurable = 1;
                $model->apply_to_virtual = 1;
                $model->apply_to_downloadable = 1;
            }
            else {
                $sel_pro_type=$_POST['Attributes']['productTypeApplicableUpdate'];          
                if($sel_pro_type!=''){
                    $model->apply_to_simple = 0;
                    $model->apply_to_bundle = 0;
                    $model->apply_to_grouped = 0;
                    $model->apply_to_configurable = 0;
                    $model->apply_to_virtual = 0;
                    $model->apply_to_downloadable = 0;
                    $sel_pro_type_count=count($sel_pro_type)-1;
                    for($i=0;$i<=$sel_pro_type_count;$i++){
                        if($sel_pro_type[$i]=='Simple'){$model->apply_to_simple = 1; }
                        if($sel_pro_type[$i]=='Bundle'){$model->apply_to_bundle = 1; }
                        if($sel_pro_type[$i]=='Grouped'){$model->apply_to_grouped = 1; }
                        if($sel_pro_type[$i]=='Configurable'){$model->apply_to_configurable = 1;}
                        if($sel_pro_type[$i]=='Virtual'){$model->apply_to_virtual = 1;}
                        if($sel_pro_type[$i]=='Downloadable'){$model->apply_to_downloadable = 1;}
                    }
                }   
            }
            $model->required=$_POST['Attributes']['required'];
            $model->configurable=$_POST['Attributes']['configurable'];
            $model->validation=$_POST['Attributes']['validation']; 
            if($_POST['Attributes']['field']=='dropdown' || $_POST['Attributes']['field']=='multiselect'){
                $input_type_dropdown=1;
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()){
            if($input_type_dropdown==1){ 
                $att_op_values=$_POST['AttributeOptions']['value']; 
                //var_dump($att_op_values);die;
                foreach ($att_op_values as $index => $option){ 
                    if(!$attributeOption = AttributeOptions::find()->where(['value' => $option, 'attribute_id' => $model->id])->one()){ //die($option);
                        $attributeOption = new AttributeOptions;
                    }
                    $attributeOption->value=$option;
                    if($attributeOption->value!=''){
                        $attributeOption->attribute_id=$model->id;
                        $attributeOption->save(false);
                    }
                }
            }
            return $this->redirect(['index']);
        } 
        else {
            return $this->render('update', ['model' => $model,]);                    
        }
    }

    /**
     * Deletes an existing Attributes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteAttributeOption(){    
        
        $id=$_POST['id'];
        $attibutesoption=AttributeOptions::findOne($_POST['id']);
        $attibutesoption->delete(false);
        return $id;
    }

    /**
     * Finds the Attributes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attributes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attributes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
