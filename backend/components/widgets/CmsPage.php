<?php 

namespace backend\components\widgets;
use yii\base\Widget;
use yii\helpers\Html;
use backend\models\CmsPage;
use yii;

Class CmsPages extends Widget
{	
	public $code;
	public function init() {
		if($this->code===null) {
			$this->code=null;
		}
	}

	public function run() {
		
		$result = $this->generateCmsPage($this->code);
		echo $result;
	
		//print_r($result);
	}

	public function generateCmsPage($id) {
		$blockData = $this->findModel($id);
		return $blockData;
	}

	public function findModel($id) {
		$result = CmsPage::find()->where(['id'=>$id])->one();
		return $result;
	}

}

?>
