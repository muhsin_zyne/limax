<div class="information-blocks">
                            <div class="tabs-container">
                                <div class="swiper-tabs tabs-switch">
                                    <div class="title">Products</div>
                                    <div class="list">
                                        <a class="block-title tab-switcher active">Featured Products</a>
                                        <a class="block-title tab-switcher">Popular Products</a>
                                        <a class="block-title tab-switcher">New Arrivals</a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tabs-entry" style="opacity: 1; display: block;">
                                        <div class="products-swiper">
                                            <div class="swiper-container swiper-swiper-unique-id-1 initialized pagination-hidden" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4" id="swiper-unique-id-1">
                                                <div class="swiper-wrapper" style="width: 916px; transition-duration: 0s; transform: translate3d(0px, 0px, 0px); height: 389px;">
                                                    <div class="swiper-slide swiper-slide-visible swiper-slide-active" style="width: 229px; height: 389px;"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-1.jpg" alt="">
                                                                    <a class="top-line-a left"><i class="fa fa-retweet"></i></a>
                                                                    <a class="top-line-a right"><i class="fa fa-heart"></i></a>
                                                                    <div class="bottom-line">
                                                                        <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide swiper-slide-visible" style="width: 229px; height: 389px;"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-2.jpg" alt="">
                                                                    <a class="top-line-a right open-product"><i class="fa fa-expand"></i> <span>Quick View</span></a>
                                                                    <div class="bottom-line">
                                                                        <div class="right-align">
                                                                            <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                                            <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                                        </div>
                                                                        <div class="left-align">
                                                                            <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide swiper-slide-visible" style="width: 229px; height: 389px;"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-3.jpg" alt="">
                                                                    <div class="bottom-line left-attached">
                                                                        <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                                        <a class="bottom-line-a square open-product"><i class="fa fa-expand"></i></a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide swiper-slide-visible" style="width: 229px; height: 389px;"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-4.jpg" alt="">
                                                                    <div class="bottom-line left-attached">
                                                                        <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                                        <a class="bottom-line-a square open-product"><i class="fa fa-expand"></i></a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pagination pagination-swiper-unique-id-1"><span class="swiper-pagination-switch swiper-visible-switch swiper-active-switch" style="display: inline-block;"></span><span class="swiper-pagination-switch swiper-visible-switch" style="display: none;"></span><span class="swiper-pagination-switch swiper-visible-switch" style="display: none;"></span><span class="swiper-pagination-switch swiper-visible-switch" style="display: none;"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs-entry" style="display: none; opacity: 0;">
                                        <div class="products-swiper">
                                            <div class="swiper-container swiper-swiper-unique-id-2 initialized pagination-hidden" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4" id="swiper-unique-id-2">
                                                <div class="swiper-wrapper" style="width: 916px;">
                                                    <div class="swiper-slide" style="width: 229px;"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-1.jpg" alt="">
                                                                    <a class="top-line-a left"><i class="fa fa-retweet"></i></a>
                                                                    <a class="top-line-a right"><i class="fa fa-heart"></i></a>
                                                                    <div class="bottom-line">
                                                                        <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide" style="width: 229px;"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-3.jpg" alt="">
                                                                    <div class="bottom-line left-attached">
                                                                        <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                                        <a class="bottom-line-a square open-product"><i class="fa fa-expand"></i></a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide" style="width: 229px;"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-4.jpg" alt="">
                                                                    <div class="bottom-line left-attached">
                                                                        <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                                        <a class="bottom-line-a square open-product"><i class="fa fa-expand"></i></a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide" style="width: 229px;"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-2.jpg" alt="">
                                                                    <a class="top-line-a left"><i class="fa fa-retweet"></i></a>
                                                                    <a class="top-line-a right"><i class="fa fa-heart"></i></a>
                                                                    <div class="bottom-line">
                                                                        <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pagination pagination-swiper-unique-id-2"><span class="swiper-pagination-switch swiper-active-switch" style="display: inline-block;"></span><span class="swiper-pagination-switch" style="display: none;"></span><span class="swiper-pagination-switch" style="display: none;"></span><span class="swiper-pagination-switch" style="display: none;"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs-entry">
                                        <div class="products-swiper">
                                            <div class="swiper-container swiper-swiper-unique-id-3 initialized pagination-hidden" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4" id="swiper-unique-id-3">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-2.jpg" alt="">
                                                                    <a class="top-line-a right open-product"><i class="fa fa-expand"></i> <span>Quick View</span></a>
                                                                    <div class="bottom-line">
                                                                        <div class="right-align">
                                                                            <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                                            <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                                        </div>
                                                                        <div class="left-align">
                                                                            <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-3.jpg" alt="">
                                                                    <div class="bottom-line left-attached">
                                                                        <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-heart"></i></a>
                                                                        <a class="bottom-line-a square"><i class="fa fa-retweet"></i></a>
                                                                        <a class="bottom-line-a square open-product"><i class="fa fa-expand"></i></a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-1.jpg" alt="">
                                                                    <a class="top-line-a left"><i class="fa fa-retweet"></i></a>
                                                                    <a class="top-line-a right"><i class="fa fa-heart"></i></a>
                                                                    <div class="bottom-line">
                                                                        <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="swiper-slide"> 
                                                        <div class="paddings-container">
                                                            <div class="product-slide-entry">
                                                                <div class="product-image">
                                                                    <img src="/themes/shopping/img/product-electronics-4.jpg" alt="">
                                                                    <a class="top-line-a left"><i class="fa fa-retweet"></i></a>
                                                                    <a class="top-line-a right"><i class="fa fa-heart"></i></a>
                                                                    <div class="bottom-line">
                                                                        <a class="bottom-line-a"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                    </div>
                                                                </div>
                                                                <a class="tag" href="#">Men clothing</a>
                                                                <a class="title" href="#">Blue Pullover Batwing Sleeve Zigzag</a>
                                                                <div class="rating-box">
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                    <div class="star"><i class="fa fa-star"></i></div>
                                                                </div>
                                                                <div class="price">
                                                                    <div class="prev">$199,99</div>
                                                                    <div class="current">$119,99</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pagination pagination-swiper-unique-id-3"><span class="swiper-pagination-switch" style="display: inline-block;"></span><span class="swiper-pagination-switch" style="display: none;"></span><span class="swiper-pagination-switch" style="display: none;"></span><span class="swiper-pagination-switch" style="display: none;"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>