<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\DashboardAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use backend\models\User;
use backend\components\TestMenu;
use kartik\sidenav\SideNav;
use backend\config\Menu;
use yii\bootstrap;
use backend\components\widgets\CustomModal;

DashboardAsset::register($this);

$activeuser =  User::find()->where(['id' =>yii::$app->user->id])->one();
if($activeuser->image=="") {
    $activeuser->image = 'avatar-01.png';
}
$currentPage = yii::$app->controller->id.'/'.yii::$app->controller->action->id;
$controllerId = Yii::$app->controller->id;
$actionId = Yii::$app->controller->action->id;
$requestUrl = yii::$app->request->url;
$item = $currentPage;

if(yii::$app->user->isGuest) {
  die("403 Not Allowded");
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/jquery.form.js"></script>
    <script src="/js/fileinput.js"> </script>
    


    <?php $this->head() ?>
</head>

<body class="hold-transition skin-black sidebar-mini">
<?php $this->beginBody() ?>
<div class="se-pre-con">



  <svg class="progress-circular">
    <circle class="progress-circular__primary" cx="50%" cy="50%" r="30%" fill="none" stroke-width="9%" stroke-miterlimit="10"/>
  </svg>


</div>
<div class="wrapper">

        

                  <header class="main-header">
                    <!-- Logo -->
                    
                    <!-- Header Navbar: style can be found in header.less -->
                    <nav class="navbar navbar-static-top" role="navigation">
                      <!-- Sidebar toggle button-->
                      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                      </a>
                      <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                          
                          <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                              <img src="/drive/uploads/avatar/<?=$activeuser->image;?>" class="user-image" alt="User Image">
                              <span class="hidden-xs"> <?= $activeuser->display_name; ?> </span>
                            </a>
                            <ul class="dropdown-menu">
                              <!-- User image -->
                              <li class="user-header">
                                <img src="/drive/uploads/avatar/<?=$activeuser->image;?>" class="img-circle" alt="User Image">
                                <p>
                                  <?= $activeuser->display_name.' - ('.$activeuser->user_type.')'; ?>
                                  <small><?= $activeuser->email; ?></small>
                                </p>
                              </li>
                              
                              <li class="user-footer">
                                <div class="pull-left">
                                  <a href="<?= Url::to(['/profile']);?>" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                   <a href="<?= Url::to(['/site/logout']);?>" class="btn btn-default btn-flat"> Logout </a>
                                 
                                </div>
                              </li>
                            </ul>
                          </li>
                          <!-- Control Sidebar Toggle Button -->
                          
                        </ul>
                      </div>
                    </nav>
                  </header>





                  <aside class="main-sidebar">
                    <section class="sidebar">
                      <div class="user-panel">
                        <div class="pull-left image">
                          <img src="/drive/uploads/avatar/<?=$activeuser->image;?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                          <p> <?= $activeuser->display_name; ?> </p>
                          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                      </div>
                      <!-- search form -->
                      <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input type="text" name="q" class="form-control" placeholder="Search...">
                          <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
                      </form>
                      

                        <?php 


                        $type = 'SideNav::TYPE_DEFAULT';
                        echo SideNav::widget([
                        'type' => $type,
                        'encodeLabels' => false,
                        'heading' => '',
                        'items' =>[
                            
                                      [
                                        'label' => 'Home', 
                                        'icon' => 'home', 
                                        'visible'=>yii::$app->controller->isVisible('Index'),
                                        'url' => Url::to(['/site/index']),
                                        'active' => ($currentPage == 'site/index')
                                      ],

                                      [
                                        'label' => 'Catalogue', 
                                        'icon' => 'table', 
                                        'visible'=>yii::$app->controller->isVisible('Catalogue'),
                                        'url' => Url::to(['/#']),
                                        'active' =>  in_array($controllerId, ['attributes','product-type']) ? true : false,
                                        'items' =>[    

                                                      [
                                                        'label' => 'Categories',
                                                        'url' => Url::to(['/categories']), 
                                                        'active' => ($currentPage == 'categories/index'),
                                                        
                                                      ],
                                                      [
                                                        'label' => 'Attributes',
                                                        'url' => Url::to(['#']), 
                                                        'active' => in_array($currentPage, ['attributes/index','product-type/index','brands/index']) ? true : false,
                                                        'items' => [

                                                            [
                                                              'label'=>'Manage Attributes',
                                                              'url' => Url::to(['/attributes/index']),
                                                              'active'=>($currentPage=='attributes/index')
                                                            ],
                                                            [
                                                              'label'=>'Product Types',
                                                              'url' => Url::to(['/product-type/index']),
                                                              'active'=>($currentPage=='product-type/index')
                                                            ],
                                                            [
                                                              'label'=>'Brands',
                                                              'url' => Url::to(['/brands/index']),
                                                              'active'=>($currentPage=='brands/index')
                                                            ],

                                                        ],
                                                      ],

                                                       [
                                                        'label' => 'Products',
                                                        'url' => Url::to(['#']), 
                                                        'active' => ($currentPage == 'products/index'),
                                                        'items' => [

                                                            [
                                                              'label'=>'Manage Products',
                                                              'url' => Url::to(['/products/index']),
                                                              'active'=>($currentPage=='products/index')
                                                            ],

                                                        ],
                                                      ],


                                                      [
                                                        'label' => 'Banners',
                                                        'url' => Url::to(['#']), 
                                                        'active' => ($currentPage == 'banners/index'),
                                                        'items' => [

                                                            [
                                                              'label'=>'Manage Banners',
                                                              'url' => Url::to(['/banners/index']),
                                                              'active'=>($currentPage=='banners/index')
                                                            ],

                                                        ],
                                                      ],


                                                     

                                              

                                                  ],

                                      ],

                                      [
                                        'label' => 'Menu Management', 
                                        'icon' => 'sitemap', 
                                        'visible'=>yii::$app->controller->isVisible('Menus'),
                                        'url' => Url::to(['/menus']),
                                        'active' => ($controllerId == 'menus'),
                                        'items' =>[   
                                                      [
                                                        'label' => 'Category  Menu',
                                                        'url' => Url::to(['/menus/index','type'=>'category']), 
                                                        'active' => ($requestUrl == '/menus/index?type=category')
                                                      ],

                                                      [
                                                        'label' => 'Main Menu',
                                                        'url' => Url::to(['/menus/index','type'=>'main']), 
                                                        'active' => ($requestUrl == '/menus/index?type=main')
                                                      ],
                                                      [
                                                        'label'=>'Header Menu',
                                                        'url' => Url::to(['/menus/index','type'=>'header']),
                                                        'active'=>($requestUrl=='/menus/index?type=header')
                                                      ],
                                                      [
                                                        'label'=>'Footer Menu',
                                                        'url' => Url::to(['/menus/index','type'=>'footer']),
                                                        'active'=>($requestUrl=='/menus/index?type=footer')
                                                      ],

                                                  ],

                                      ],

                                      [
                                        'label' => 'B2B Users', 
                                        'icon' => 'users', 
                                        'visible'=>yii::$app->controller->isVisible('Menus'),
                                        'url' => Url::to(['/b2b-users/index','type'=>'all']),
                                        'active' =>  in_array($controllerId, ['b2b-users']) ? true : false,
                                        'items' =>[   
                                                      [
                                                        'label' => 'All Users',
                                                        'url' => Url::to(['/b2b-users/index','type'=>'all']), 
                                                        'active' => ($requestUrl == '/b2b-users/index?type=all'),
                                                      ],

                                                      [
                                                        'label' => 'New Requests',
                                                        'url' => Url::to(['/b2b-users/index','type'=>'pending']), 
                                                        'active' => ($requestUrl == '/b2b-users/index?type=pending'),
                                                      ],
                                                      [
                                                        'label' => 'Terminated Users',
                                                        'url' => Url::to(['/b2b-users/index','type'=>'terminated']), 
                                                        'active' => ($requestUrl == '/b2b-users/index?type=terminated'),
                                                      ],

                                                       [
                                                        'label' => 'Disabled Users',
                                                        'url' => Url::to(['/b2b-users/index','type'=>'disable']), 
                                                        'active' => ($requestUrl == '/b2b-users/index?type=disable'),
                                                      ],
                                                      

                                                  ],

                                      ],

                                      
                                    /* [
                                        'label' => 'Attribute-set', 
                                        'icon' => 'bars',
                                        'visible'=>yii::$app->controller->isVisible('Attribute-set'),
                                        'url' => Url::to(['/attribute-set/index']),
                                        'active' => ($currentPage == 'attribute-set/index')
                                      ],
                                      
                                      [
                                        'label' => 'Configurations',
                                        'icon' => 'cogs',
                                        'visible'=>yii::$app->controller->isVisible('Configurations'),
                                        'active'=>($controllerId=='configuration'),
                                        'items' =>[
                                                      [
                                                        'label' => 'General Configuration',
                                                        'url' => Url::to(['/configuration/general']), 
                                                        'active' => ($currentPage == 'configuration/general')
                                                      ],

                                                  ],
                                       
                                      ],

                                      */


                                      
                                  ],
                      ]); ?>

                      



                    </section>
                  </aside>

         

        

          <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1 class="capitalise">
               <?= $controllerId ?>
                <small class="capitalise"> <?= Html::encode($this->title); ?> </small>
              </h1>
              <ol class="breadcrumb">
                <li><a  class="capitalise" href="<?=Url::to(['/'.$controllerId]); ?>"><i class="fa fa-dashboard"></i> <?php if($controllerId=='site') { echo"Home"; } else { echo $controllerId; } ?> </a></li>
                <li class="active"> <a  class="capitalise" href="#"> <?=$actionId ?> </a> </li>
              </ol>
            </section>


            <!-- Main content -->
            <section class="content">

               <div class="box-layout">
                  
               <?php  //$data = require('../config/menu.php');   var_dump($data); ?>

                  <?= skinka\widgets\gritter\AlertGritterWidget::widget() ?>
                  <?= $content ?>

               </div>
              
              
            </section>
        </div>

    <footer class="main-footer">
            <div class="pull-right hidden-xs">
              <b>Version</b> 2.3.0
            </div>
            <strong>Copyright &copy; <?= date('Y');?><a href="http://www.rangetech.in" target="_blank"> RangeTech Solutions</a>.</strong> All rights reserved.
    </footer>
</div>

 <?= CustomModal::widget([
            'header' => 'DemoMenu',
            'id' => 'modal-default',
            'body' =>'<div id="modalContentDefault"> 
                      <svg class="progress-circular">
                      <circle class="progress-circular__primary" cx="50%" cy="50%" r="25%" fill="none" stroke-width="9%" stroke-miterlimit="10"/>
                  </svg>
                  </div>',

        ]); 
  ?>


  <?= CustomModal::widget([
            'header' => 'DemoMenu2',
            'id' => 'modal-pop-select',
            'size' =>'model-xs',
            'body' =>'<div id="modalContentpop"> 
                      
                  </div>',

        ]); 
  ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

<script>
//paste this code under the head tag or in a separate js file.
  // Wait for window load
  $(window).load(function() {
    // Animate loader off screen
    
    $(".se-pre-con").fadeOut();
  });
</script>
<script>
$(function () {
    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
         $( ".picker" ).each(function() {
            $( this ).datepicker({
            dateFormat : 'dd-mm-yy',
            language : 'en',
            changeMonth: true,
            changeYear: true
          });
        });
    });
});
</script>

<script> 
$( document).ready(function() {

   $(".bootstrap-modal-new").click(function(event){
        event.preventDefault();
        $("#modal-default").modal('show')
        .find("#modalContentDefault")
        .load($(this).attr('href'));
        $('h4#ModalHeaderID').text($(this).attr('data-title'));
        $(".modal-dialog").css("width", $(this).attr('data-width'));
   });


  var screenHeight = $(window).height();
  $ ( '.custom-nav' ).slimScroll({
        height: screenHeight-100

    });
    
}); 
</script>


<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>