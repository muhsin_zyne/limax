<?php
use backend\components\widgets\CustomBanner;
/* @var $this yii\web\View */

$this->title = 'Limax Group';
?>
<section>
<div class="main-banner">
    <div class="tp-banner-container">
        <div class="tp-banner" >
            <ul>   
                <!-- SLIDE  1  -->
               
                
                <!-- SLIDE  2 -->
                <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                    <!-- MAIN IMAGE -->
                    <img src="/images/slide2a.png"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption customin"
                        data-x="474"
                        data-y="189"
                        data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="500"
                        data-start="800"
                        data-easing="Power3.easeInOut"
                        data-endspeed="300"
                        style="z-index: 2"><img src="/images/ago2-blue-2.png" alt="">
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption customin"
                        data-x="245"
                        data-y="92"
                        data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="500"
                        data-start="500"
                        data-easing="Power3.easeInOut"
                        data-endspeed="300"
                        style="z-index: 3"><img src="/images/ago2-bw-5.png" alt="">
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption customin"
                        data-x="693"
                        data-y="69"
                        data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="500"
                        data-start="1300"
                        data-easing="Power3.easeInOut"
                        data-endspeed="300"
                        style="z-index: 4"><img src="/images/ago2-simple-1.png" alt="">
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption customin"
                        data-x="100"
                        data-y="171"
                        data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="500"
                        data-start="1400"
                        data-easing="Power3.easeInOut"
                        data-endspeed="300"
                        style="z-index: 5"><img src="/images/ago2-orange-1.png" alt="">
                    </div>
                </li>
                
                 <?= CustomBanner::widget([])?>
               
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="demo loader" id="waitme">
        <p> dfdfdfdd </p>

    
    </div>

        </div>
    </div>
</section>
    



    <!-- THE SCRIPT INITIALISATION -->
    <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
    <script type="text/javascript">

        function run_waitMe(effect){
    $('#waitme').waitMe({
        effect: 'facebook',
        text: 'Please wait...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: '',
        source: '',
        onClose: function() {}
    });

}

        var revapi;

        jQuery(document).ready(function() {

               revapi = jQuery('.tp-banner').revolution(
                {
                    delay:9000,
                    startwidth:1170,
                    startheight:500,
                    hideThumbs:10,
                    fullWidth:"on",
                    forceFullWidth:"on"
                });

        }); //ready

    </script>

</div>