<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_images".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $image
 * @property string $is_thumb
 * @property string $is_small
 * @property string $is_base
 * @property string $is_featured
 */
class ProductImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'image', 'is_thumb', 'is_small', 'is_base', 'is_featured'], 'required'],
            [['product_id'], 'integer'],
            [['image', 'is_thumb', 'is_small', 'is_base', 'is_featured'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'image' => 'Image',
            'is_thumb' => 'Is Thumb',
            'is_small' => 'Is Small',
            'is_base' => 'Is Base',
            'is_featured' => 'Is Featured',
        ];
    }
}
