<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-logo">
    <a href=""><b>LIMAX GROUP</b></a>
</div><!-- /.login-logo -->

<div class="login-box-body">
     <p class="login-box-msg">Sign in to start your session</p>
     <!-- <h1><?= Html::encode($this->title) ?></h1> -->
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                

                <div class="form-group has-feedback">
                    <?= $form->field($model, 'username')->textInput(['class'=>'form-control','autofocus' => true]) ?>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                 </div>

                <div class="form-group has-feedback">

                    <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control']) ?>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>


        
            
         

</div>


