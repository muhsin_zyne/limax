<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\search\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;

/**
 * B2bUsersController implements the CRUD actions for User model.
 */
class B2bUsersController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex($type)
    {   
    
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);
        if($type=='all') {
            $dataProvider->query->andWhere(['level'=>'5']);
        }
        else if($type=='pending') {
            $dataProvider->query->andWhere(['level'=>'5','is_approved'=>'pending']);
        }
        else if($type=='terminated') {
            $dataProvider->query->andWhere(['level'=>'5','is_approved'=>'terminated']);
        }
        else if($type=='disable') {
            $dataProvider->query->andWhere(['level'=>'5','is_approved'=>'disable']);
        }
       

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (yii::$app->request->post()) {
                $model->b2bAddressModel->load(yii::$app->request->post());
                $model->load(yii::$app->request->post());
                $model->updated_at = $this->getNowTimeStamp();
                if($model->oldAttributes['is_approved']=='pending' && $model->is_approved=='success') {
                  

                    $email = Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'activationToUser-html', 'text' => 'activationToUser-html'],
                            ['model' => $model]
                        )
                        ->setFrom([Yii::$app->params['supportEmail'] => 'Limax Group'])
                        ->setTo([$model->email])
                        ->setSubject('Join Request Approved - '. yii::$app->params['websiteName'])
                        ->send();
                      
                }

                $model->save();
                
                
                $model->b2bAddressModel->save();
                Yii::$app->getSession()->setFlash('success', 'Details has been updated');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);     
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
