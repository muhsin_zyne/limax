<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "meta".
 *
 * @property integer $id
 * @property string $type
 * @property integer $product_id
 * @property integer $cms_id
 * @property string $meat-title
 * @property string $meta-description
 * @property string $meta-keyword
 */
class Meta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'product_id', 'meta_title', 'meta_description', 'meta_keyword'], 'required'],
            [['type', 'meta_title', 'meta_description', 'meta_keyword'], 'string'],
            [['product_id', 'cms_id'], 'integer'],
            [['cms_id'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'product_id' => 'Product ID',
            'cms_id' => 'Cms ID',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
        ];
    }
}
