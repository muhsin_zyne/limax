<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <div class="row">
        <div class="col-xs-12">
             <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'username',
                    'display_name',
                    'email',
        
                     ['class' => 'yii\grid\ActionColumn',   
                        'header'=>'Action',
                        'template'=>'{view}{delete}',
                        'buttons'=>[
                          
                            'view' => function($url,$model) {
                                return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', ['b2b-users/update','id'=>$model->id], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>'View Dealer Account - '.$model->username,                                           
                                        'data-width'=>'50%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'View',
                                    ]); 

                            },
                        ]                            
                    ],
                ],
            ]); ?>

        </div>
    </div>

    
   
</div>
