<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $icon
 * @property string $image
 * @property string $status
 * @property integer $parent
 * @property integer $position
 * @property string $readonly
 * @property string $url_key
 * @property string $meta_description
 * @property string $meta_keywords
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'image', 'status', 'readonly', 'url_key', 'meta_description', 'meta_keywords'], 'string'],
            [['parent', 'position'], 'integer'],
            [['url_key'], 'unique', 'targetClass' => 'common\models\Categories', 'targetAttribute' => 'url_key', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['title', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'icon' => 'Icon',
            'image' => 'Image',
            'status' => 'Status',
            'parent' => 'Parent',
            'position' => 'Position',
            'readonly' => 'Readonly',
            'url_key' => 'Url Key',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }

    public function getList($items) {
        $elements = [];
        foreach ($items as $item) {
            if($item['parent']==0) {
                $elements [] = [
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'url_key' => $item['url_key'],
                    'items' => $this->getChildList($items, $item['id']),
                ];

            }
        }

       $result = $this->getCategoriesList($elements,FALSE);   
        return $result;
    }

    public function getChildList($new_items, $parent) {
        $new_elements = [];
        foreach ($new_items as $new_item) {

            if($new_item['id']!=$parent && $new_item['parent']==$parent) {

                $new_elements [] = [
                    'id' => $new_item['id'],
                    'title' => $new_item['title'],
                    'url_key' => $new_item['url_key'],
                    'items' => $this->getChildList($new_items, $new_item['id']),
                ];
            }
            
        }
        return $new_elements;
    }


    public function getCategoriesList($items,$child=FALSE) {
            $html = '<ol class="dd-list">';
            foreach ($items as $item) {
                $html.='<li class="dd-item dd3-item" data-id="'.$item['id'].'">';
                $html.='<div class="dd-handle dd3-handle"></div>';
                $html.=' <div class="dd3-content">'.$item['title'];
                $html.='<u class="menu-icon-set">'.Html::a('<i class="fa fa-pencil"></i> Edit', ['/categories/update','id'=>$item['id']],['class'=>'bootstrap-modal-new hover-black','data-title' => 'Update Menu', 'data-width'=>'70%', ]).'</u>

                <u class="menu-icon-set">'.Html::a('<i class="fa fa-eye"></i> view', ['/categories/view','id'=>$item['id']],['class'=>'bootstrap-modal-new hover-black','data-title' => 'View category', 'data-width'=>'70%', ]).'</u>

                <u class="menu-icon-set">'.Html::a('<i class="fa fa-plus"></i> new ', ['/categories/create-child','child'=>$item['id']],['class'=>'bootstrap-modal-new hover-black','data-title' => 'Child category for - '.$item['title'], 'data-width'=>'70%', ]).'</u>

                <u class="menu-icon-set">'.Html::a('<i class="fa fa-trash"></i> Delete', ['/categories/delete', 'id' => $item['id']], ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],'class'=>'danger']).'</u>


                ';

               
                $html.='</div>';
                $html.= $this->getCategoriesList($item['items'],$item['id']);
                $html.='</li>';
            }
            $html.='</ol>';



        return $html;
    }



}
