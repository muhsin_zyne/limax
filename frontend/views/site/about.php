<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use backend\components\widgets\CmsBlocks;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
section.page-view {
   background: rgba(194, 163, 37, 0.48);
}

#content-block *:last-child {
    margin-bottom: 8px !important;
}

.article-container h5, .h5 {   
    color: #b51717 !important;   
}

.article-container {   
    color: #000000 !important;    
}

.project-thumbnail { 
    margin-bottom: 10px !important; 
}
.about-border {
    border: solid;
    margin-bottom: 12px;
    padding: 7px 15px 0px 20px;
    border-color: #b7a972;
}
</style>

<div class="container">
	<div class="raw margin-top-min-75">
		<div class="site-about">
			<div id="content-block">

		        <div class="content-center fixed-header-margin">
		           
		          <img class="project-thumbnail" src="/themes/shopping/img/banner.jpg" alt="" />

		            <div class="content-push">

		               

		                <div class="information-blocks">
		                    <div class="row">

		                    	<?= CmsBlocks::widget(['code'=>2])?>
		                 

		                        <div class="col-md-4 information-entry">
		                            <div class="accordeon">
		                                <div class="accordeon-title"> <?= CmsBlocks::widget(['code'=>3])?> </div>
		                                <div class="accordeon-entry" style="display: none;">
		                                    <div class="article-container style-1">
		                                        <?= CmsBlocks::widget(['code'=>4])?> 
		                                    </div>
		                                </div>
		                                <div class="accordeon-title"><?= CmsBlocks::widget(['code'=>5])?> </div>
		                                <div class="accordeon-entry" style="display: none;">
		                                    <div class="article-container style-1">
		                                        <?= CmsBlocks::widget(['code'=>6])?>
		                                    </div>
		                                </div>
		                               
		                            </div>
                                </div>                   
		                    </div>
		                </div>
		                
		                
		            </div>
		        </div>
		        <div class="clear"></div>
		    </div>
		</div>
	</div>	
</div>

