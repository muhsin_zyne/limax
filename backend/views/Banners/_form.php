<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        
        <div class="col-xs-12" style="padding: 25px 30px;">
            <?= $form->field($model, 'file')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [ // Plugin options of the Kartik's FileInput widget 
                'showUpload' => false,
                    /*'initialPreview'=> $model->isNewRecord ? '' : [
                        yii::$app->params['rootUrl'].'/store/banners/'.$model->file,
                        
                    ],
                    'initialPreviewAsData'=>true,*/
                
            ]

            ]);

?>
          
        </div>    
     
    </div>

        

    <div class="form-group">
        
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right theme-btn' : 'btn btn-primary pull-right theme-btn']) ?>
    </div>
    <div class="clearfix">  </div>
    <?php ActiveForm::end(); ?>

</div>
