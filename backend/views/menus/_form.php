<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Menus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'path')->textarea(['rows' => 6]) ?>


    <?= $form->field($model, 'target')->dropDownList([ '_self' => 'Current Tab', '_blank' => 'New Tab'], ['prompt' => 'Link type']) ?>

    <?= $form->field($model, 'icon')->textInput([]) ?>
    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn' : 'btn btn-primary theme-btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
