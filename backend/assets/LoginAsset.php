<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'themes/adminlte/bootstrap/css/bootstrap.min.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
        'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'themes/adminlte/dist/css/AdminLTE.min.css',
        'themes/adminlte/dist/css/AdminLTE.css',
        'themes/adminlte/plugins/iCheck/square/blue.css',
        '',
    ];
    public $js = [
        //'themes/adminlte/plugins/jQuery/jQuery-2.1.4.min.js',
        'themes/adminlte/bootstrap/js/bootstrap.min.js',
        'themes/adminlte/plugins/iCheck/icheck.min.js',

    ];
    public $depends = [
        //'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
