<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use backend\components\widgets\CmsBlocks;

$this->title = $model->meta_title;
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="container">
	<div class="row">
		<div class="Col-xs-12">
			<h2 style="font-size: 26px; text-decoration: underline; text-transform: uppercase;"> <?= $model->title?> </h2>
		</div>
		<div class="col-xs-12" style="padding: 10px; padding-top: 30px;">
			<?= $model->content ?>
		</div>
	</div>	
</div>

