<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="categories-form">
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'url_key')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'readonly')->dropDownList([ '1'=>'No', '0'=>'Yes',], ['prompt' =>'Accesiblilty']) ?>
            <?= $form->field($model, 'meta_description')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'status')->dropDownList([ '1'=>'Active', '0'=>'Inactive'], ['prompt' => 'status']) ?>
            
            <?php
                if(isset($model->readonly)) {
                    if($model->readonly!=0) {
                        echo '<div class="form-group"> '.Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']).'</div>';
                    }
                }
                else {
                    echo '<div class="form-group">'.Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']).'</div>';
                }
            ?>

        </div>
    </div>
    <?php 
    if(isset($child)) {
       echo  $form->field($model, 'parent')->hiddenInput(['value'=> $child])->label(false);
    }
    ?>

    
    <div class="clearfix"> </div>

    <?php ActiveForm::end(); ?>

</div>


