<?php 

namespace backend\components\widgets;
use yii\base\Widget;
use yii\helpers\Html;
use backend\models\Banners;
use yii;

Class CustomBanner extends Widget
{	
	public $type;
	public function init() {
		if($this->type===null) {
			$this->type='homeMain';
		}
	}

	public function run() {
		
		$result = $this->generateBanner($this->type);
		echo $result;
	
		//print_r($result);
	}

	public function generateBanner($type) {
		$banner = new Banners;
		$result = $banner->findAllBanners('4');
		$htmlData = $this->generateHtmlData($result);

		return $htmlData;
	}

	public function generateHtmlData($result) {
		$animations = [

			0 => 'cube',
			1 => '3dcurtain-vertical',
			2 => 'boxslide',
			3 => 'papercut',
			
 		];

		$html = '';

			foreach ($result as $key =>  $value) {
			
			$html.= '<li data-transition="'.$animations[$key].'" data-slotamount="7" data-masterspeed="1500" >';
		    $html.= '<img src="'.yii::$app->params['rootUrl'].'/store/banners/'.$value->file.'"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">';
		    $html.= '</li>';
		}
		
		
		

		return $html;
	}

}

?>



 
                   
                    
                    
                



















