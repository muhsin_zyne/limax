<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
$this->title = 'Dealers Accounts';
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
section.page-view {
    background-image: url('/images/dealer.jpg')!important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
}
</style>

<div class="container" style="background-image: url()>
    <div class="raw margin-top-min-75">
        <div class="site-contact">          
            <div id="content-block">
                <div class="content-center fixed-header-margin">
                    <div class="content-push">
                        <div class="information-blocks">
                            <div class="row">
                                <div class="col-sm-6 information-entry">
                                    <div class="login-box">
                                        <div class="article-container style-1">
                                            <h3>Registered Dealers</h3>
                                        </div>

                                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                                            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                                            <?= $form->field($model, 'password')->passwordInput() ?>
                                            
                                            <?= $form->field($model, 'rememberMe', [
                                                'template' => "<label class='checkbox-entry'> {input} <span class='check'></span> Remember Me </label> ",
                                            ])->checkbox([],false) ?>

                                            <div style="color:#999;margin:1em 0">
                                                If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                            </div>

                                            <div class="form-group">
                                                <?= Html::submitButton('Login', ['class' => 'button style-10', 'name' => 'login-button']) ?>
                                            </div>

                                        <?php ActiveForm::end(); ?>
                                       
                                    </div>
                                </div>
                                <div class="col-sm-6 information-entry">
                                    <div class="login-box">
                                        <div class="article-container style-1">
                                            <h3>Are you a New Dealer ?</h3>
                                            <p>If your are a bulk dealer and looking to join us please procced registration By creating an Dealer account, you will be able to redeem exclusive offers and much more !</p>
                                        </div>
                                        <a class="button style-12" href="<?=Url::to(['/site/dealer-signup'])?>">Register Here</a>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="clear"></div>
            </div>  

        </div>
    </div>  
</div>

