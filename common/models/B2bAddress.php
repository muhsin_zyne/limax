<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "b2b_address".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $mobile
 * @property string $phone
 * @property string $shop_name
 * @property string $address
 * @property string $place
 * @property string $district
 */
class B2bAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'b2b_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['shop_name', 'address', 'district'], 'required'],
            [['address'], 'string'],
            [['mobile', 'phone'], 'string', 'max' => 10],
            [['shop_name', 'place', 'district'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'mobile' => 'Mobile',
            'phone' => 'Phone',
            'shop_name' => 'Shop Name',
            'address' => 'Address',
            'place' => 'Place',
            'district' => 'District',
        ];
    }
}
