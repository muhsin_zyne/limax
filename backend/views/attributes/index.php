<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AttributesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attributes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attributes-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Attributes', ['create'], ['class' => 'btn btn-success theme-btn pull-right']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'required'=>[
                'header' =>'Required',
                'value' => function($model, $attribute){ return $model->required=="1"? "Required" : "Not Required"; },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'required', ['1' => 'Required', '0' => 'Not Required'],['class'=>'form-control','prompt' => 'All']),
            ],
            
            'apply_to_simple'=>[
                'header' =>'Simple product',
                'value' => function($model, $attribute){ return $model->apply_to_simple=="1"? "Yes" : "No"; },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'apply_to_simple', ['1' => 'Yes', '0' => 'No'],['class'=>'form-control','prompt' => 'All']),
            ],

            'configurable'=>[
                'header' =>'configurable product',
                'value' => function($model, $attribute){ return $model->configurable=="1"? "Yes" : "No"; },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'configurable', ['1' => 'Yes', '0' => 'No'],['class'=>'form-control','prompt' => 'All']),
            ],
            // 'apply_to_virtual',
            // 'apply_to_bundle',
            // 'apply_to_downloadable',
            // 'system',
            // 'code',
            // 'field',
            // 'validation',
            // 'configurable',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
