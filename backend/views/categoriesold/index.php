<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Categories;

$categoriesModel = new Categories;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
    
    <div class="row">
        <div class="col-xs-12">
                <div class="btn-group pull-left" id="nestable-menu">
                
                <?= Html::button('Expand All', ['class' => 'btn btn-primary theme-btn ', 'type'=>'button', 'data-action'=>'expand-all']) ?>
                <?= Html::button('Collapse All', ['class' => 'btn btn-danger theme-btn ', 'type'=>'button', 'data-action'=>'collapse-all']) ?>
                <?= Html::button('Update Changes', ['class' => 'btn btn-success theme-btn ', 'type'=>'button','id'=>'applay-change']) ?>
               
            </div>
        </div>
    </div>



    <div class="row" id="SELECTOR">
        <div class="col-xs-8">

            <div class="dd drag-box" id="nestable">
                <div class="block-2">
                    <span class="inner-10"> Product Categories</span>

                   <?= Html::a('<i class="fa fa-plus-square" aria-hidden="true"></i> ', ['/categories/create'], ['class'=>'btn btn-success theme-btn bootstrap-modal-new inner-2 pull-right','data-title' => 'Create Category', 'data-width'=>'70%', ]) ?>

                    
                </div>
                
                
                <div class="clearfix"> </div>
                <?php
                    if(count($all_categories)>0) {
                        $result = $categoriesModel->getList($all_categories);
                        echo $result;
                    }
                    else {
                        echo " <pre> NO Data </pre>";
                    }
                ?>
                <textarea id="nestable-output"></textarea>
            </div>

        </div>
       
    </div>    






<script>

$(document).ready(function()
{

    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // activate Nestable for list 2
    //$('#nestable2').nestable({
    //    group: 1
   // })
    //.on('change', updateOutput);

    // output initial serialised data

    updateOutput($('#nestable').data('output', $('#nestable-output')));
    //updateOutput($('#nestable2').data('output', $('#nestable2-output')));

    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });

    $('#nestable3').nestable();

});
</script>


<script type="text/javascript">
    
$(document).ready(function(){
   




function run_waitMe(effect){
    $('#viewRefresh').waitMe({
        effect: 'facebook',
        text: 'Please wait...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: '',
        source: '',
        onClose: function() {}
    });

    //none, rotateplane, stretch, orbit, roundBounce, win8, win8_linear, ios, facebook, rotation, timer, pulse,  progressBar, bouncePulse or img
}


});
</script>






    <style type="text/css">

    
    

    </style>