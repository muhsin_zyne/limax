<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Attributes;

/**
 * AttributesSearch represents the model behind the search form about `common\models\Attributes`.
 */
class AttributesSearch extends Attributes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'required', 'apply_to_simple', 'apply_to_grouped', 'apply_to_configurable', 'apply_to_virtual', 'apply_to_bundle', 'apply_to_downloadable', 'system', 'configurable'], 'integer'],
            [['title', 'code', 'field', 'validation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attributes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'required' => $this->required,
            'apply_to_simple' => $this->apply_to_simple,
            'apply_to_grouped' => $this->apply_to_grouped,
            'apply_to_configurable' => $this->apply_to_configurable,
            'apply_to_virtual' => $this->apply_to_virtual,
            'apply_to_bundle' => $this->apply_to_bundle,
            'apply_to_downloadable' => $this->apply_to_downloadable,
            'system' => $this->system,
            'configurable' => $this->configurable,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'field', $this->field])
            ->andFilterWhere(['like', 'validation', $this->validation]);

        return $dataProvider;
    }
}
