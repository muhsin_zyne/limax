<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_price".
 *
 * @property integer $id
 * @property integer $product_id
 * @property double $price
 * @property double $special_price
 * @property double $cost_price
 * @property double $dealers_price
 * @property string $type
 * @property string $from_date
 * @property string $to_date
 */
class ProductPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'price', 'cost_price', 'dealers_price', 'from_date', 'to_date'], 'required'],
            [['id', 'product_id'], 'integer'],
            [['price', 'special_price', 'cost_price', 'dealers_price'], 'number'],
            [['type'], 'string'],
            [['from_date', 'to_date', 'special_price', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'price' => 'Price',
            'special_price' => 'Special Price',
            'cost_price' => 'Cost Price',
            'dealers_price' => 'Dealers Price',
            'type' => 'Type',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
        ];
    }
}
