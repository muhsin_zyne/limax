<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attributes".
 *
 * @property integer $id
 * @property string $title
 * @property integer $required
 * @property integer $apply_to_simple
 * @property integer $apply_to_grouped
 * @property integer $apply_to_configurable
 * @property integer $apply_to_virtual
 * @property integer $apply_to_bundle
 * @property integer $apply_to_downloadable
 * @property integer $system
 * @property string $code
 * @property string $field
 * @property string $validation
 * @property integer $configurable
 */
class Attributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $attributeGroupId;
    public $productTypeApplicableUpdate;
    public $productTypeApplicable;

    public static function tableName()
    {
        return 'attributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['required', 'apply_to_simple', 'apply_to_grouped', 'apply_to_configurable', 'apply_to_virtual', 'apply_to_bundle', 'apply_to_downloadable', 'system', 'configurable'], 'integer'],
            [['field'], 'string'],
            [['title'], 'string', 'max' => 45],
            [['code', 'validation'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Attribute Title',
            'required' => 'Required',
            'apply_to_simple' => 'Apply To Simple',
            'apply_to_grouped' => 'Apply To Grouped',
            'apply_to_configurable' => 'Apply To Configurable',
            'apply_to_virtual' => 'Apply To Virtual',
            'apply_to_bundle' => 'Apply To Bundle',
            'apply_to_downloadable' => 'Apply To Downloadable',
            'system' => 'System',
            'code' => 'Attribute Code',
            'field' => 'Attribute   Field',
            'validation' => 'Validation',
            'configurable' => 'Configurable',
            'attributeGroupId' => 'Applay for ',
            'productTypeApplicableUpdate' => 'Applicable for ',
            'productTypeApplicable' => 'Applicable for ',
        ];
    }
}
