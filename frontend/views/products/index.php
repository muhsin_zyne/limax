<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container" style="background: #f4f4f4;">
    <div class="row">
      <!-- <div class="btn-group btn-group-justified">
        <a href="#" class="btn btn-primary open-filterMob">Applay Filters</a>
      </div> -->

       <a href="#" class="float open-filterMob">
        <i class="fa fa-plus my-float"></i>
      </a>
     
        <div class="col-xs-12 col-sm-3">
            <?= $this->render('_search',['model'=>$searchModel]); ?>
        </div>

        

        <div class="col-xs-12 col-sm-9 col-sm-8row-list">
                <?= ListView::widget([
                'id'=>'feturedproductLister',
                'dataProvider' => $dataProvider,
                'layout' => "
                ".$this->render('_list-product',['dataProvider'=>$dataProvider])."
                
                <div class='pull-right'>{pager}</div> <div class='clearfix'></div>",
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                ],
            ]) ?> 
        </div>
        
        <script type="text/javascript">
            $("#feturedproductLister").removeClass('list-view');
        </script>                 


    </div>
</div>


<style type="text/css">

@media only screen and (max-width: 767px) {
  .computerfilter {
    display: none !important;
  }
}

@media only screen and (min-width: 768px) {
  .open-filterMob {
    display: none !important;
  }
}
            .product-block {
                padding: 18px;
                background: #fff;
                margin-left: -10px;
                margin-bottom: 43p;
                margin-right: -10px;
                margin-top: 5px;
                margin-bottom: 5px;
            }
            .col-xs-12.col-sm-4.p-box {
                background: #f4f4f4;
            }
            .row-list {
                background: #f4f4f4;
            }
            .zoom { 
            z-index: 10;
                -webkit-transition: all .3s ease-out; 
                -moz-transition: all .3s ease-out; 
                -o-transition: all .3s ease-out; 
                transition: all .3s ease-out; 
            -webkit-backface-visibility: hidden;
            -webkit-perspective: 1000;
            }
            .zoom:hover { 
                -moz-transform: scale(1.2);
                -webkit-transform: scale(1.2);
                -o-transform: scale(1.2);
                transform: scale(1.2);
                -ms-transform: scale(1.2);
              
            }
            .product-image {
                overflow: hidden;
            }
            .product-size h6 {
                font-size: 16px;
                font-weight: bold;
                color: #839298;
            }
            .product-id h4 {
                font-size: 29px;
                font-weight: bold;
                color: #f08500;
            }
            .product-id h6 {
                font-size: 16px;
                font-size: bold;
                text-transform: uppercase;
                color: #6e6e77;
            }
            section.page-view {
                background: #f4f4f4;
            }

            .list-view .shop-grid-item {
                width: 50%;
            }
            .shop-grid-item:hover {
                box-shadow: 0 4px 16px 0 rgba(0,0,0,.11);
                
            }
            .product-slide-entry.shift-image {
                height: 277px;
            }


            .checkbox {
  position: relative;
  display: inline-block;
  vertical-align: top;
  cursor: default;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-family: -apple-system, 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-weight: 400;
  font-size: 17px;
  line-height: 24px;
}
.checkbox__checkmark {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  position: relative;
  display: inline-block;
  vertical-align: top;
  cursor: default;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-family: -apple-system, 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-weight: 400;
  font-size: 17px;
  position: relative;
  overflow: hidden;
  height: 24px;
  pointer-events: none;
}
.checkbox__input {
  position: absolute;
  overflow: hidden;
  right: 0px;
  top: 0px;
  left: 0px;
  bottom: 0px;
  padding: 0;
  border: 0;
  opacity: 0.001;
  z-index: 1;
  vertical-align: top;
  outline: none;
  width: 100%;
  height: 100%;
  margin: 0;
  -webkit-appearance: none;
  appearance: none;
}
.checkbox__input:checked {
  background: rgba(24,103,194,0.81);
}
.checkbox__input:checked + .checkbox__checkmark:before {
  background: rgba(24,103,194,0.81);
  border: 1px solid rgba(24,103,194,0.81);
}
.checkbox__input:checked + .checkbox__checkmark:after {
  opacity: 1;
}
.checkbox__checkmark:before {
  content: '';
  position: absolute;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  width: 24px;
  height: 24px;
  background: transparent;
  border: 1px solid rgba(24,103,194,0.81);
  -webkit-border-radius: 16px;
  border-radius: 16px;
  -webkit-box-shadow: none;
  box-shadow: none;
  left: 0;
}
.checkbox__checkmark {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  position: relative;
  display: inline-block;
  vertical-align: top;
  cursor: default;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-family: -apple-system, 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-weight: 400;
  font-size: 17px;
  position: relative;
  overflow: hidden;
  width: 24px;
  height: 24px;
}
.checkbox__checkmark:after {
  content: '';
  position: absolute;
  top: 6px;
  left: 5px;
  width: 12px;
  height: 6px;
  background: transparent;
  border: 3px solid #fff;
  border-width: 2px;
  border-top: none;
  border-right: none;
  -webkit-border-radius: 0px;
  border-radius: 0px;
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  opacity: 0;
}
.checkbox__input:focus + .checkbox__checkmark:before {
  -webkit-box-shadow: none;
  box-shadow: none;
}
.checkbox__input:disabled + .checkbox__checkmark {
  opacity: 0.3;
  cursor: default;
  pointer-events: none;
}
.checkbox__input:disabled:active + .checkbox__checkmark:before {
  background: transparent;
  -webkit-box-shadow: none;
  box-shadow: none;
}
.checkbox--material {
  line-height: 18px;
  font-family: 'Roboto', 'Noto', sans-serif;
  -webkit-font-smoothing: antialiased;
}
.checkbox--material__checkmark {
  width: 18px;
  height: 18px;
}
.checkbox--material__checkmark:before {
  border: 2px solid #717171;
}
.checkbox--material__input:checked + .checkbox--material__checkmark:before {
  background-color: #009688;
  border: none;
}
.checkbox--material__input + .checkbox--material__checkmark:after {
  border-color: #ffffff;
  -webkit-transition: -webkit-transform 0.3s ease;
  -moz-transition: -moz-transform 0.3s ease;
  -o-transition: -o-transform 0.3s ease;
  transition: transform 0.3s ease;
  width: 10px;
  height: 5px;
  top: 4px;
  left: 3px;
  -webkit-transform: scale(0) rotate(-45deg);
  -moz-transform: scale(0) rotate(-45deg);
  -ms-transform: scale(0) rotate(-45deg);
  -o-transform: scale(0) rotate(-45deg);
  transform: scale(0) rotate(-45deg);
}
.checkbox--material__input:checked + .checkbox--material__checkmark:after {
  top: 4px;
  left: 3px;
  -webkit-transform: scale(1) rotate(-45deg);
  -moz-transform: scale(1) rotate(-45deg);
  -ms-transform: scale(1) rotate(-45deg);
  -o-transform: scale(1) rotate(-45deg);
  transform: scale(1) rotate(-45deg);
}
.checkbox--material__input:disabled + .checkbox--material__checkmark {
  opacity: 1;
}
.checkbox--material__input:disabled + .checkbox--material__checkmark:before {
  border-color: #afafaf;
}
.checkbox--material__input:disabled:checked + .checkbox--material__checkmark:before {
  background-color: #afafaf;
}
.checkbox--material__input:disabled:checked + .checkbox--material__checkmark:after {
  border-color: #fff;
}
.checkbox--material__input:disabled:checked:active + .checkbox--material__checkmark:before {
  background-color: #afafaf;
}
.checkbox--material__checkmark:before {
  -webkit-border-radius: 2px;
  border-radius: 2px;
  width: 18px;
  height: 18px;
}
.search-box {
  background: #fff;
    padding: 30px;
    margin-top: 6px;
}
h1.filter-label {
    font-size: 20px !important;
    font-weight: bold;
    margin-left: -12px;
    padding-bottom: 10px;
}

.float{
  position:fixed;
  width:60px;
  height:60px;
  bottom:40px;
  right:40px;
  background-color:#0C9;
  color:#FFF;
  border-radius:50px;
  text-align:center;
  box-shadow: 2px 2px 3px #999;
  z-index: 999999999999;
}

.my-float{
  margin-top:22px;
}

</style>

<script type="text/javascript">
  $("input").change(function() {
    /*var form = $("form#productSearchForm").serializeArray();
    console.log(form);
*/
    /*jQuery.ajax({
        method: 'POST',
        url: '/products/index',
        crossDomain: false,
        data: form,
        dataType:'json',
        success: function(responseData, textStatus, jqXHR) {
            
            console.log(responseData);
            
        },
        error: function (responseData, textStatus, errorThrown) {
            console.log(responseData);
        },
    });
*/

    $("#productSearchForm").submit();
  });
</script>



<div id="filter-popup" class="overlay-popup" style="">
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">

                        <div class="row" style="width: 100%; height: 100%">
                          <div class="col-xs-6 filter-btn" style="background: #ff0402;">
                              <ul style="width: 100%">
                                  <li class="mob-filter active" data-view="brands"> Brands </li>
                                  <li class="mob-filter" data-view="category"> Category </li>
                                  <li class="mob-filter" data-view="types"> Types </li>

                              </ul>
                              <div class="filter-adjuster"> </div> 
                          </div>
                          <div class="col-xs-6">
                               <?= $this->render('_searchMob',['model'=>$searchModel]); ?>
                          </div>
                        </div>
                        <div class="row">
                        <div class="btn-group btn-group-justified" style="padding: 10px;">
                            <a href="#" class="btn btn-primary" id="applyMobFilter" style="height: 52px;">Applay Filter</a>
                            <a href="<?=Url::to(['/products/index'])?>" class="btn btn-danger"  style="height: 52px;">Reset</a>
                        </div>
                        </div>

                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<style type="text/css">
  li.mob-filter {
    list-style: none;
    padding: 25px;
    background: #00aff0;
    margin: 2px;
    font-size: 18px;
    font-weight: bold;
    color: #fff;
}
li.mob-filter:hover {
    background: #000 !important;
}
li.mob-filter.active {
    background: #000 !important;
}
.col-xs-6.filter-btn {
    margin-left: -26px !important;
    margin-top: -27px !important;
    padding-right: 0px;
}
.filter-adjuster {
    padding-bottom: 60px;
}
.products-search.search-box.mob {
    margin-left: 0px !important;
    margin-right: -94px;
}

</style>