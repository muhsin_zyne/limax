<?php
return [
    'websiteName' => 'Limax Group',
    'adminEmail' => 'muhsin.3009@gmail.com',
    'supportEmail' => 'no-reply@limaxgroup.co.in',
    'user.passwordResetTokenExpire' => 3600,

     'maskMoneyOptions' => [
        'prefix' => 'INR ',        
        'affixesStay' => true,
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 2, 
        'allowZero' => false,
        'allowNegative' => false,
    ]

];
